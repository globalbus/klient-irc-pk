/*
 * Copyright (C) 2002, 2003 Christoph Schwering
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License. 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details. 
 * You should have received a copy of the GNU General Public License along with 
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
 * Place, Suite 330, Boston, MA 02111-1307, USA. 
 */

package org.schwering.irc.moep;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import org.schwering.irc.lib.*;

/** 
 * The chan-center window which contains MODE information. 
 * @author Christoph Schwering &lt;ch@schwering.org&gt;
 */
public class ChanCenter extends JDialog {

  /**
   * The owning class.
   */
  private MoepIRC moep;
  
  /**
   * The name of the channel. 
   */
  private String chan;

  /**
   * The AWT List which contains the banlist. 
   */
  private java.awt.List lBanlist;

  /**
   * The Arraylist which contains the banlist. 
   */
  private ArrayList banlist;

  /**
   * The list of bans which were selected to be removed by the user.
   */
  private ArrayList banlistToRemove = new ArrayList();
    
  /**
   * The list of bans which were added by the user.
   */
  private ArrayList banlistToAdd = new ArrayList();

  /**
   * The IRCModeParser of the channel-modes. 
   */
  private IRCModeParser modeParser;
  
  /**
   * The checkbox for mode <code>i</code> (invite). 
   */
  private JCheckBox cbModei;

  /**
   * The boolean which represents which value the checkbox for mode i had when 
   * the window was loaded. 
   */
  private boolean bModei;

  /**
   * The checkbox for mode <code>k</code> (key). 
   */
  private JCheckBox cbModek;

  /**
   * The boolean which represents which value the checkbox for mode k had when 
   * the window was loaded. 
   */
  private boolean bModek;

  /**
   * The checkbox for mode <code>l</code> (limit). 
   */
  private JCheckBox cbModel;

  /**
   * The boolean which represents which value the checkbox for mode l had when 
   * the window was loaded. 
   */
  private boolean bModel;

  /**
   * The checkbox for mode <code>m</code> (moderated). 
   */
  private JCheckBox cbModem;

  /**
   * The boolean which represents which value the checkbox for mode m had when 
   * the window was loaded. 
   */
  private boolean bModem;

  /**
   * The checkbox for mode <code>n</code> (no msgs from outside). 
   */
  private JCheckBox cbModen;

  /**
   * The boolean which represents which value the checkbox for mode n had when 
   * the window was loaded. 
   */
  private boolean bModen;

  /**
   * The checkbox for mode <code>p</code> (private). 
   */
  private JCheckBox cbModep;

  /**
   * The boolean which represents which value the checkbox for mode 
   * <code>p</code> had when the window was loaded. 
   */
  private boolean bModep;

  /**
   * The checkbox for mode <code>s</code> (secret). 
   */
  private JCheckBox cbModes;

  /**
   * The boolean which represents which value the checkbox for mode 
   * <code>s</code> had when the window was loaded. 
   */
  private boolean bModes;

  /** 
   * The checkbox for mode <code>t</code> (topic). 
   */
  private JCheckBox cbModet;

  /** 
   * The boolean which represents which value the checkbox for mode 
   * <code>t</code> had when the window was loaded. 
   */
  private boolean bModet;

  /** 
   * The textfield for the key. 
   */
  private JTextField tfKey;

  /** 
   * The String represents the key which was set when the window was loaded.
   */
  private String key = "";

  /** 
   * The textfield for the limit. 
   */
  private JTextField tfLimit;

  /** 
   * The String represents the limit which was set when the window was loaded. 
   */
  private String limit = "";

// ------------------------------

  /** 
   * The constructor creates the GUI of the setup-window. 
   * @param owner The owning <code>MoepIRC</code> instance.
   * @param chan The channel.
   * @param count The count of users.
   * @param operCount The count of operators.
   * @param banlist The banlist.
   * @param modes The set modes.
   */
  public ChanCenter(MoepIRC owner, String chan, int count, int operCount, 
      ArrayList banlist, String modes) {
    super(owner, true);
    String chanInTitle = (chan.length() > 8) ? chan.substring(0,2) + "..."+ 
        chan.substring(chan.length()-3) : chan;
    setTitle(chanInTitle +" - "+ String.valueOf(operCount) 
        +" / "+ String.valueOf(count) +" opers"); 
    this.moep = owner;
    this.chan = chan;
    this.banlist = banlist;
    this.modeParser = new IRCModeParser(modes);
    JButton okay = new JButton("OK");
    JButton cancel = new JButton("Cancel");
    okay.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) {
        generateModes();
        dispose();
      }
    } );
    cancel.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    } );
    JPanel bottom = new JPanel(); // child of the main panel
    bottom.add(okay);
    bottom.add(cancel);
    // centered panel
    JPanel center = new JPanel();
    center.setLayout(new GridLayout(2,1));
    center.add(makeBanlistPanel());
    center.add(makeModesPanel());
    // main panel
    JPanel main = new JPanel();
    main.setLayout(new BorderLayout());
    main.add(center, BorderLayout.CENTER);
    main.add(bottom, BorderLayout.SOUTH);
    getContentPane().add(main);
    setSize(280,300); 
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension mySize = getSize();
    if (mySize.height > screenSize.height)
      mySize.height = screenSize.height;
    if (mySize.width > screenSize.width) 
      mySize.width = screenSize.width;
    int x = (screenSize.width - mySize.width)/2;
    int y = (screenSize.height - mySize.height)/2;
    setLocation(x, y);
    setVisible(true);
  }

// ------------------------------

  /** 
   * Returns a complete panel for the banlist area. 
   * @return A complete panel for the banlist area.
   */
  private JPanel makeBanlistPanel() {
    lBanlist = new java.awt.List(7, false);
    int banCount = banlist.size();
    for (int i = 0; i < banCount; i++)
      lBanlist.add((String)banlist.get(i));
    JPanel list = new JPanel(); 
    list.setLayout(new GridLayout(1,1));
    list.add(lBanlist);

    JButton add = new JButton("Add");
    add.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) {
        String input = JOptionPane.showInputDialog(null, "Banmask?");
        if (input != null)
          banlistToAdd.add(input);
      } 
    } );
    JButton remove = new JButton("Remove");
    remove.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) {
        if (lBanlist.getSelectedIndex() != -1)
          banlistToRemove.add(lBanlist.getSelectedItem());
      } 
    } );
    JPanel buttons = new JPanel(new GridLayout(4, 1));
    buttons.add(add);
    buttons.add(remove);
      
    JPanel p = new JPanel();
    p.setLayout(new BorderLayout());
    p.add(list, BorderLayout.CENTER);
    p.add(buttons, BorderLayout.EAST);
    return p;
  }

// ------------------------------

  /** 
   * Returns a complete panel for the modes' area. 
   * @return A complete panel for the modes' area. 
   */
  private JPanel makeModesPanel() {
    cbModei = new JCheckBox();
    cbModek = new JCheckBox();
    cbModel = new JCheckBox();
    cbModem = new JCheckBox();
    cbModen = new JCheckBox();
    cbModep = new JCheckBox();
    cbModes = new JCheckBox();
    cbModet = new JCheckBox();
    tfKey = new JTextField(4);
    tfLimit = new JTextField(3);

    analyzeModes(); 

    JPanel leftboxes = new JPanel(new GridLayout(5, 1));
    addComponent(leftboxes, cbModei);
    addComponent(leftboxes, cbModek);
    addComponent(leftboxes, cbModel);
    addComponent(leftboxes, cbModem);
      
    JPanel leftlabels = new JPanel(new GridLayout(5, 1));
    addComponent(leftlabels, new JLabel("Invite only"));
    JPanel tmp;
    tmp = new JPanel(new BorderLayout());
    tmp.add(new JLabel("Key "), BorderLayout.WEST);
    tmp.add(tfKey, BorderLayout.CENTER);
    addComponent(leftlabels, tmp);
    tmp = new JPanel(new BorderLayout());
    tmp.add(new JLabel("Limit "), BorderLayout.WEST);
    tmp.add(tfLimit, BorderLayout.CENTER);
    addComponent(leftlabels, tmp);
    addComponent(leftlabels, new JLabel("Moderated"));

    JPanel left = new JPanel(new BorderLayout());
    left.add(leftboxes, BorderLayout.WEST);
    left.add(leftlabels, BorderLayout.CENTER);
      
    JPanel rightboxes = new JPanel(new GridLayout(5, 1));
    addComponent(rightboxes, cbModen);
    addComponent(rightboxes, cbModep);
    addComponent(rightboxes, cbModes);
    addComponent(rightboxes, cbModet);
      
    JPanel rightlabels = new JPanel(new GridLayout(5, 1));
    addComponent(rightlabels, new JLabel("No ext. msgs"));
    addComponent(rightlabels, new JLabel("Private"));
    addComponent(rightlabels, new JLabel("Secret"));
    addComponent(rightlabels, new JLabel("Strict Topic"));
      
    JPanel right = new JPanel(new BorderLayout());
    right.add(rightboxes, BorderLayout.WEST);
    right.add(rightlabels, BorderLayout.CENTER);
      
    JPanel modes = new JPanel();
    modes.setLayout(new GridLayout(1, 2));
    modes.add(left);
    modes.add(right);
    return modes;
  }

// ------------------------------

  /** 
   * Analyzes the modes with the help of the IRCModeParser.<br />
   * It sets the selection of the checkboxes. 
   */
  private void analyzeModes() {
    int number = modeParser.getCount();
    for (int i = 1; i <= number; i++) {
      switch (modeParser.getModeAt(i)) {
        case 'i': 
          this.bModei = convertOperatorToBoolean(modeParser.getOperatorAt(i));
          cbModei.setSelected(this.bModei); 
          break;
        case 'k': 
          this.bModek = convertOperatorToBoolean(modeParser.getOperatorAt(i)); 
          cbModek.setSelected(this.bModek); 
          this.key = modeParser.getArgAt(i); 
          tfKey.setText(modeParser.getArgAt(i)); 
          break;
        case 'l': 
          this.bModel = convertOperatorToBoolean(modeParser.getOperatorAt(i)); 
          cbModel.setSelected(this.bModel); 
          this.limit = modeParser.getArgAt(i); 
          tfLimit.setText(modeParser.getArgAt(i)); 
          break;
        case 'm': 
          this.bModem = convertOperatorToBoolean(modeParser.getOperatorAt(i)); 
          cbModem.setSelected(this.bModem); 
          break;
        case 'n': 
          this.bModen = convertOperatorToBoolean(modeParser.getOperatorAt(i)); 
          cbModen.setSelected(this.bModen); 
          break;
        case 'p': 
          this.bModep = convertOperatorToBoolean(modeParser.getOperatorAt(i)); 
          cbModep.setSelected(this.bModep); 
          break;
        case 's': 
          this.bModes = convertOperatorToBoolean(modeParser.getOperatorAt(i)); 
          cbModes.setSelected(this.bModes); 
          break;
        case 't': 
          this.bModet = convertOperatorToBoolean(modeParser.getOperatorAt(i)); 
          cbModet.setSelected(this.bModet); 
          break;
      }
    }
  }

// ------------------------------

  /** 
   * Analyzes the checkboxes with the modes and generates a MODE command. 
   */
  private void generateModes() {
    key = tfKey.getText();
    String newLimit = tfLimit.getText();
    String modes = "";
    int banCountToAdd = banlistToAdd.size();
    int banCountToRemove = banlistToRemove.size();

    boolean bModei = cbModei.isSelected();
    boolean bModek = cbModek.isSelected();
    boolean bModel = cbModel.isSelected();
    boolean bModem = cbModem.isSelected();
    boolean bModen = cbModen.isSelected();
    boolean bModep = cbModep.isSelected();
    boolean bModes = cbModes.isSelected();
    boolean bModet = cbModet.isSelected();

    // possibly the limit must be reactivated because its arg might be changed 
    // thats the case when the checkbox WAS and IS checked and if the 
    // WAS-LIMIT is DIFFERENT to the IS-LIMIT
    if (bModel == this.bModel == true && !this.limit.equals(newLimit)) { 
      // this is FALSE in real! we just prohibite that it was originally 
      // unchecked so that the WAS-CHECKBOX and the IS-CHECKBOX are DIFFERENT
      this.bModel = false; 
      bModel = true;
    }

    if (this.bModei != bModei) 
      modes += convertBooleanToOperator(cbModei.isSelected()) +"i";
    if (this.bModek != bModek) 
      modes += convertBooleanToOperator(cbModek.isSelected()) +"k";
    if (this.bModel != bModel) 
      modes += convertBooleanToOperator(cbModel.isSelected()) +"l"; 
    if (this.bModem != bModem) 
      modes += convertBooleanToOperator(cbModem.isSelected()) +"m";
    if (this.bModen != bModen) 
      modes += convertBooleanToOperator(cbModen.isSelected()) +"n";
    if (this.bModep != bModep) 
      modes += convertBooleanToOperator(cbModep.isSelected()) +"p";
    if (this.bModes != bModes) 
      modes += convertBooleanToOperator(cbModes.isSelected()) +"s";
    if (this.bModet != bModet) 
      modes += convertBooleanToOperator(cbModet.isSelected()) +"t";

    for (int i = 0; i < banCountToRemove; i++)
      modes += "-b";
      
    for (int i = 0; i < banCountToAdd; i++)
      modes += "+b";

    if (this.bModek != bModek)
      modes += " "+ key; 
    if (this.bModel != bModel && bModel) 
      modes += " "+ newLimit; 

    String tmp;
    for (int i = 0; i < banCountToRemove; i++) {
      tmp = (String)banlistToRemove.get(i);
      int index = tmp.indexOf(' ');
      if (index != -1)
        tmp = tmp.substring(0, index);
      modes += " "+ tmp;
    }
      
    for (int i = 0; i < banCountToAdd; i++) {
      tmp = (String)banlistToAdd.get(i);
      int index = tmp.indexOf(' ');
      if (index != -1)
        tmp = tmp.substring(0, index);
      modes += " "+ tmp;
    }

    if (modes.trim().length() > 0)
      moep.getIRCConnection().doMode(chan, modes);
  }

// ------------------------------

  /** 
   * This method just converts a char <code>+</code> or <code>-</code> 
   * into a <code>boolean</code> with the value <code>true</code> or 
   * <code>false</code>. 
   * @param operator A <code>+</code> or <code>-</code>.
   * @return <code>true</code> for a <code>+</code>.
   */
  private boolean convertOperatorToBoolean(char operator) {
    return operator == '+';
  }

// ------------------------------

  /** 
   * This method just converts a char <code>+</code> or <code>-</code> into 
   * a <code>boolean</code> with the value <code>true</code> or 
   * <code>false</code>. 
   * @param b The <code>boolean</code>.
   * @return <code>+</code> for <code>true</code>.
   */
  private char convertBooleanToOperator(boolean b) {
    return (b) ? '+' : '-';
  }

// ------------------------------

  /**
   * Adds a <code>JComponent</code> in a new <code>FlowLayout</code> with
   * left alignment to a <code>JPanel</code>.
   * @param p The <code>JPanel</code> to which we want to add the component.
   * @param c The <code>JComponent</code> which is to add.
   */
  private void addComponent(JPanel p, JComponent c) {
    JPanel flow = new JPanel(new FlowLayout(FlowLayout.LEFT));
    flow.add(c);
    p.add(flow);
  }

// ------------------------------

}