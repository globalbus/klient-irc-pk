/*
 * Copyright (C) 2002, 2003 Christoph Schwering
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License. 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details. 
 * You should have received a copy of the GNU General Public License along with 
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
 * Place, Suite 330, Boston, MA 02111-1307, USA. 
 */

package org.schwering.irc.moep;

import java.awt.*;
import javax.swing.*;

/**
 * Shows a small frame with a progressbar. The progressbar can be manipulated by
 * the methods <code>update</code> which set a string or the percent.
 * @author Christoph Schwering &lt;ch@schwering.org&gt;
 */
public class ProgressFrame extends JFrame {

  /**
   * The progressbar.
   */
  private JProgressBar pb;

  /**
   * Creates a new small undecorated frame with a progressbar which is set to
   * <code>0 %</code>.
   */
  public ProgressFrame() {
    setTitle("Loading moepII");
    setSize(250, 50);
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension mySize = getSize();
    if (mySize.height > screenSize.height)
      mySize.height = screenSize.height;
    if (mySize.width > screenSize.width)
      mySize.width = screenSize.width;
    int x = (screenSize.width - mySize.width)/2;
    int y = (screenSize.height - mySize.height)/2;
    setLocation(x, y); 
    getContentPane().add(getPanel());
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    setResizable(false);
    setUndecorated(true);
    setVisible(true);    
  }

  /**
   * Returns the panel with the progressbar.
   * @return The new Panel.
   */
  private JPanel getPanel() {
    JPanel p = new JPanel(new BorderLayout(1,1));
    pb = new JProgressBar();
    pb.setStringPainted(true);
    pb.setString("");
    p.add(pb);
    return p;
  }

  /**
   * Sets the new percentage <code>i</code> of the progressbar.
   * @param i The new percentage.
   */
  public void update(int i) {
    pb.setValue(i);
    if (i == 100)
      dispose();
  }

  /**
   * Sets the new progressbar-string.
   * @param s The new string of the progessbar.
   */
  public void update(String s) {
    pb.setString(s);
  }
  
}