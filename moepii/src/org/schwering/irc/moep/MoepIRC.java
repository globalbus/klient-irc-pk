/*
 * Copyright (C) 2002, 2003 Christoph Schwering
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License. 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details. 
 * You should have received a copy of the GNU General Public License along with 
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
 * Place, Suite 330, Boston, MA 02111-1307, USA. 
 */

package org.schwering.irc.moep;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import org.schwering.irc.lib.*;


/**
 * <p>This class is the graphical user interface (GUI) of the moepII client.</p>
 * <p>The GUI is based on jqIRC by jChakma and Quamrul from India. 
 * Visit jChakma at <a href=http://www.geocities.com/jchakma 
 * target=_blank>geocities.com/jchakma</a>. And here's the poem for them:</p>
 * <p align=center>
 *                To some people, a friend<br />
 *            is practically anyone they know.<br />
 *                To me, friendship means<br />
 *              a much closer relationship,<br />
 *            one in which you take the time<br />
 *            to really understand each other.<br />
 *         A friend is someone you trust enough<br />
 *              to share a part of yourself<br />
 *         the rest of the world may never see.<br />
 *               That kind of friendship<br />
 *            doesn't come along everyday...<br />
 *            but that's the way it should be.
 * </p>
 * <p>But by the time the influences of jqIRC became less and less.</p>
 * <p>moepII is a simple IRC client using IRClib (org.schwering.irc.lib.*).</p>
 * @author Christoph Schwering &lt;ch@schwering.org&gt;
 * @version 0.65
 */
public class MoepIRC extends JFrame implements Runnable {

  /** 
   * The IRClib's <code>IRCConnection</code> instance contains all IRC 
   * connection methods. 
   */
  private IRCConnection conn;

  /**
   * If <code>true</code>, an <code>SSLIRCConnection</code> object is used
   * as <code>IRCConnection</code>. <code>false</code> by default.
   */
  private boolean useSSL = false;

  /**
   * If <code>true</code>, the user is not asked to accept the SSL certs.
   */
  private boolean autoAcceptSSLCerts = false;

  /** 
   * The IRC server we want to connect to. 
   */
  private String host;

  /** 
   * The beginning of the portrange.
   */
  private int portMin;

  /** 
   * The ending of the portrange.
   */
  private int portMax;

  /** 
   * The IRC server's password. 
   */
  private String password;

  /** 
   * The user's IRC nickname. 
   */
  private String nickname;

  /** 
   * The user's realname. 
   */
  private String realname;

  /** 
   * The user's username. 
   */
  private String username;

  /** 
   * The quit-message is saved in the settings-file. 
   */
  private String quitMsg;

  /** 
   * The color of highlighted chans. <br />
   * Default is blue. 
   */
  private Color highlightColor = Color.blue;

  /** 
   * Determines wether a beep is made when a query is opened. <br />
   * Default is <code>true</code>. 
   */
  private boolean querySoundHighlight = true;

  /** 
   * Determines wether a beep is made when the user's nick is mentioned in a 
   * <code>PRIVMSG</code>.<br />
   * Default is <code>false</code>. 
   */
  private boolean nickSoundHighlight = false;

  /** 
   * Automatic auto-rejoin after a kick or not? <br />
   * Default is <code>true</code>. 
   */
  private boolean autoRejoin = true;

  /** 
   * This <code>boolean</code> stands for automatic connect on startup 
   * (yes = <code>true</code>). <br />
   * Default is <code>false</code>. 
   */
  private boolean autoConnect = false;

  /** 
   * This <code>boolean</code> enables (<code>true</code>) and disables modules. 
   */
  private boolean loadModules = true;

  /** 
   * This <code>boolean</code> stands for logging on (<code>true</code>) or 
   * off.<br />
   * Default is on. 
   */
  private boolean logging = true;

  /** 
   * This <code>String</code> represents the directory of the logfiles. <br />
   * Default directory is <code>logs/</code>. 
   */
  private String loggingPath = "logs/";

  /** 
   * This <code>String</code> contains the perform which is executed on startup. 
   */
  private String perform = "";

  /** 
   * The browser position. <br />
   * This is OS-independant. <br />
   * Default empty. 
   */
  private String browserPath = "";

  /** 
   * The icon's position.<br />
   * Default is <code>moep.gif</code>. 
   */
  private String iconPath = "moep.gif";

  /** 
   * Represents the icon as a <code>Image</code>. 
   */
  private Image icon;

  /** 
   * This <code>boolean</code> indicates wether there is an IRC connection. 
   */
  private boolean isConnected = false;

  /** 
   * This <code>boolean</code> enables or disables (<code>false</code>) 
   * automatic channel cutting. 
   */
  private boolean cutChannelText = true;

  /** 
   * This indicates the max. amount of character in one panel. If it is higher, 
   * there are cutted lines at the top of the channel-text. 
   */
  private int maxTextLength = 30000;

  /** 
   * This is the font. 
   */
  private Font chanFont;

  /** 
   * This is the font. 
   */
  private Font consoleFont;

  /**
   * The color of lines from the server or the client program.
   */
  private Color serverColor = new Color(0, 0, 0);

  /**
   * The color of messages (<code>NOTICE</code> and <code>PRIVMSG</code>) of
   * other people.
   */
  private Color otherColor = new Color(200, 0, 0);

  /**
   * The color of own <code>PRIVMSG</code>s.
   */
  private Color ownColor = new Color(0, 0, 175);

  /**
   * The panels' background color.
   */
  private Color bgColor = new Color(255, 255, 255);

  /** 
   * The <code>JTabbedPane</code> for switching the chans. 
   */
  private JTabbedPane tabs;

  /**
   * The <code>JPopupMenu</code> for the tabs.
   */
  private JPopupMenu tabsPopup = getTabsPopupMenu();

  /** 
   * The <code>JMenuItem</code> to connect. It is switched enabled and disabled 
   * by <code>enableConnectMenuItem</code>. 
   */
  private JMenuItem menuItemConnect;

  /** 
   * The <code>JMenuItem</code> to connect. It is switched enabled and disabled 
   * by <code>enableConnectMenuItem</code>. 
   */
  private JMenuItem menuItemConnectTo;

  /** 
   * The <code>JMenuItem</code> to connect. It is switched enabled and disabled 
   * by <code>enableDisconnectMenuItem</code>. 
   */
  private JMenuItem menuItemDisconnect;

  /** 
   * This <code>int</code> represents the index of the last channel in the 
   * tabbedpane. <br />
   * Default is 0.<br />
   * This is used to order the channel- and query-windows in the tabbed pane: 
   * first tab is the console, then the channels, then the query-windows. 
   * But remember this does not have to be really the first channel-index; it 
   * is just theoretically the one. If there are no channels yet, e.g. after 
   * the application-start, this value is 0. 
   */
  private int lastChanIndex = 0;

  /** 
   * The width of the application window. 
   */
  private int appWidth = 600;

  /** 
   * The height of the application window. 
   */
  private int appHeight = 450;

  /** 
   * The x-position of the application window. <br />
   * By default it's <code>Integer.MIN_VALUE</code> to indicate that it 
   * should be centered in the constructor.
   */
  private int appX = Integer.MIN_VALUE;

  /** 
   * The y-position of the application window. <br />
   * By default it's <code>Integer.MIN_VALUE</code> to indicate that it 
   * should be centered in the constructor.
   */
  private int appY = Integer.MIN_VALUE;

  /** 
   * This array of <code>Class</code>es contains the modules!<br />
   * The array is filled by the <code>loadModules</code> method. 
   */
  private Class[] moduleClasses;

  /** 
   * This array of <code>Object</code>s are the instantiated modules.<br />
   * The length of the array is set in the <code>loadModules</code> method. 
   * Later this array is used by the <code>ActionListener</code>s of the 
   * <code>halt</code>-methods of the modules. 
   */
  private Object[] moduleInstances;

  /** 
   * This <code>boolean</code> indicates wether debuggins is enabled which 
   * means that the output- and error-streams are to be written into a file. 
   */
  private boolean debug = false;

  /** 
   * Contains the version of the program. 
   */
  public static final String VERSION = "0.65";

// ------------------------------

  /** 
   * Just starts the client :-).
   * @param args The arguments. At the moment they don't matter.
   */
  public static void main(String[] args) {
    new MoepIRC();
  }

// ------------------------------

  /** 
   * The constructor starts the graphical user interface of moepII. 
   */
  public MoepIRC() {
    ProgressFrame progress = new ProgressFrame();
    progress.update(0);
    progress.update("Setting L&F");
    try {
      UIManager.setLookAndFeel(
          UIManager.getCrossPlatformLookAndFeelClassName());
    } catch (Exception exc) {
      exc.printStackTrace();
    }

    progress.update(10);
    progress.update("Loading Settings");
    loadSettings();
    
    if (debug) {
      try {
        java.io.PrintStream err = new PrintStream(new FileOutputStream("ERR"));
        java.io.PrintStream out = new PrintStream(new FileOutputStream("OUT"));
        System.setOut(out);
        System.setErr(err);
      } catch (Exception exc) {
        exc.printStackTrace();
      }
    }

    if (loadModules) {
      progress.update(20);
      progress.update("Loading Modules");
      loadModules();
    }

    progress.update(30);
    progress.update("Preparing GUI");
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    addWindowListener(new WindowAdapter() { 
      public void windowClosing(WindowEvent e) { 
        quit(); 
      } 
    } ); 

    progress.update(40);
    setTitle("moepII");
    setJMenuBar(createMenuBar());
    progress.update(50);
    tabs = new JTabbedPane();
    progress.update(60);
    tabs.add(new ConsolePanel(this), Util.CONSOLEWINDOWTITLE, 
        Util.CONSOLEWINDOWINDEX); 
    tabs.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        setSelectedTab();
      }
    } );
    progress.update(65);
    tabs.addMouseListener(new MouseListener() {
      public void mouseClicked(MouseEvent e) { 
        if (e.getClickCount() == 1 && e.getButton() == MouseEvent.BUTTON3) {
          tabsPopup.show(tabs, e.getX(), e.getY());
        }
      }
      public void mouseEntered(MouseEvent e) { }
      public void mouseExited(MouseEvent e) { }
      public void mousePressed(MouseEvent e) { }
      public void mouseReleased(MouseEvent e) { }
    } ); 
    progress.update(70);
    getContentPane().add("Center", tabs);

    progress.update(80);
    try {
      icon = new ImageIcon(this.iconPath).getImage();
      setIconImage(icon);
    } catch (Exception exc) {
      // nothing
    }

    progress.update(90);
    setSize(this.appWidth, this.appHeight); 
    if (this.appX == Integer.MIN_VALUE && this.appY == Integer.MIN_VALUE) { 
      // default: center
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      Dimension mySize = getSize();
      if (mySize.height > screenSize.height)
        mySize.height = screenSize.height;
      if (mySize.width > screenSize.width)
        mySize.width = screenSize.width;
      int x = (screenSize.width - mySize.width)/2;
      int y = (screenSize.height - mySize.height)/2;
      setLocation(x, y); 
    } else {
      setLocation(this.appX, this.appY);
    }

    if (this.logging) { // create logging dir or set logging off
      progress.update("Making Logging-Dir");
      try {
        new File(this.loggingPath).mkdirs();
      } catch (Exception exc) {
        this.logging = false; 
      }
    }

    progress.update(95);
    progress.update("Making Visible");
    setVisible(true);
    progress.update(100);
    progress = null;

    if (autoConnect)
      connect(); 
  }

// ------------------------------

  /** 
   * Reads the settings-file <code>moep.conf</code> and sets the important 
   * class-vars.
   */
  private void loadSettings() {
    try {
      Properties p = new Properties();
      FileInputStream in = new FileInputStream("moep.conf");
      p.load(in);
      this.host = p.getProperty("host", "irc.quakenet.eu.org");
      this.portMin = Integer.parseInt(p.getProperty("portMin","6667"));
      this.portMax = Integer.parseInt(p.getProperty("portMax","6669"));
      this.useSSL = new Boolean(p.getProperty("useSSL")).booleanValue();
      this.autoAcceptSSLCerts = new Boolean(
          p.getProperty("autoAcceptSSLCerts")).booleanValue();
      this.password = p.getProperty("pass", ""); 
      this.realname = p.getProperty("realname", "realname");
      this.username = p.getProperty("username", "username");
      this.nickname = p.getProperty("nick", "nickname");
      this.quitMsg = p.getProperty("quitMsg");
      this.consoleFont = new Font(p.getProperty("consoleFontName"), 
          Font.PLAIN, 12);
      this.chanFont = new Font(p.getProperty("chanFontName"), 
          Integer.parseInt(p.getProperty("fontStyle")), 
          Integer.parseInt(p.getProperty("fontSize")));
      this.cutChannelText = new Boolean(
          p.getProperty("cutChannelText")).booleanValue();
      this.maxTextLength = Integer.parseInt(
          p.getProperty("maxTextLength"));
      this.autoRejoin = new Boolean(
          p.getProperty("rejoin")).booleanValue();
      this.querySoundHighlight = new Boolean(
          p.getProperty("queryHighlight")).booleanValue();
      this.nickSoundHighlight = new Boolean(
          p.getProperty("nickHighlight")).booleanValue();
      this.logging = new Boolean(
          p.getProperty("logging")).booleanValue();
      this.autoConnect = new Boolean(
          p.getProperty("autoConnect")).booleanValue();
      this.loggingPath = p.getProperty("loggingDir", "logs/");
      this.perform = p.getProperty("perform");
      this.browserPath = p.getProperty("browserPath");
      this.iconPath = p.getProperty("iconPath", "moep.gif");
      this.appWidth = Integer.parseInt(p.getProperty("appWidth"));
      this.appHeight = Integer.parseInt(p.getProperty("appHeight"));
      this.appX = Integer.parseInt(p.getProperty("appX"));
      this.appY = Integer.parseInt(p.getProperty("appY"));
      this.loadModules = new Boolean(
          p.getProperty("modules", "FALSE")).booleanValue();
      this.debug = new Boolean(
          p.getProperty("debug", "FALSE")).booleanValue();
      this.ownColor = new Color(Integer.parseInt(p.getProperty("ownColor")));
      this.bgColor = new Color(Integer.parseInt(p.getProperty("bgColor")));
      this.otherColor = new Color(Integer.parseInt(
          p.getProperty("otherColor")));
      this.serverColor = new Color(Integer.parseInt(
          p.getProperty("serverColor")));
      in.close();
    } catch (Exception exc) {
      this.host = "irc.quakenet.eu.org";
      this.portMin = 6667;
      this.portMax = 6669;
      this.useSSL = false;
      this.autoAcceptSSLCerts = false;
      this.password = ""; 
      this.realname = "realname";
      this.username = "username";
      this.nickname = "nickname";
      this.quitMsg = "";
      this.consoleFont = new Font("Default", Font.PLAIN, 12);
      this.chanFont = new Font("Default", Font.PLAIN, 12);
      this.cutChannelText = true;
      this.maxTextLength = 30000;
      this.autoRejoin = false;
      this.querySoundHighlight = true;
      this.nickSoundHighlight = false;
      this.logging = true;
      this.autoConnect = false;
      this.loggingPath = "logs/";
      this.perform = "";
      this.browserPath = ""; 
      this.iconPath = "moep.gif";
      this.appWidth = 600;
      this.appHeight = 450;
      this.appX = Integer.MIN_VALUE;
      this.appY = Integer.MIN_VALUE;
      this.loadModules = true;
      this.debug = false;
      this.otherColor = new Color(200, 0, 0);
      this.serverColor = new Color(0, 0, 0);
      this.ownColor = new Color(0, 0, 175);
      this.bgColor = new Color(255, 255, 255);

      JOptionPane.showMessageDialog(null,"You've not yet set up this copy of "+
          "the moepII IRC client.\nPlease fill out the following fields so "+
          "that the program can work correctly.", "Please setup your client",
          JOptionPane.INFORMATION_MESSAGE);
      new SetupDialog(this);
    }
  }

// ------------------------------

  /** 
   * Saves the userdata to the settings-file <code>moep.conf</code>. 
   */
  private void saveSettings() {
    try {
      Properties p = new Properties();
      FileOutputStream out = new FileOutputStream("moep.conf");
      p.setProperty("host", host);
      p.setProperty("portMin", String.valueOf(portMin));
      p.setProperty("portMax", String.valueOf(portMax));
      p.setProperty("useSSL", String.valueOf(useSSL));
      p.setProperty("autoAcceptSSLCerts", String.valueOf(autoAcceptSSLCerts));
      p.setProperty("pass", password);
      p.setProperty("realname", realname);
      p.setProperty("username", username);
      p.setProperty("nick", nickname);
      p.setProperty("quitMsg", quitMsg);
      p.setProperty("consoleFontName", consoleFont.getName());
      p.setProperty("chanFontName", chanFont.getName());
      p.setProperty("fontStyle", String.valueOf(chanFont.getStyle()));
      p.setProperty("fontSize", String.valueOf(chanFont.getSize()));
      p.setProperty("cutChannelText", String.valueOf(cutChannelText));
      p.setProperty("maxTextLength", String.valueOf(maxTextLength));
      p.setProperty("rejoin", String.valueOf(autoRejoin));
      p.setProperty("queryHighlight", 
          String.valueOf(querySoundHighlight));
      p.setProperty("nickHighlight", 
          String.valueOf(nickSoundHighlight));
      p.setProperty("logging", String.valueOf(logging));
      p.setProperty("autoConnect", String.valueOf(autoConnect));
      p.setProperty("loggingDir", loggingPath);
      p.setProperty("perform", perform);
      p.setProperty("browserPath", browserPath);
      p.setProperty("iconPath", iconPath);
      p.setProperty("appWidth", String.valueOf(getWidth()));
      p.setProperty("appHeight", String.valueOf(getHeight()));
      p.setProperty("appX", String.valueOf(getX()));
      p.setProperty("appY", String.valueOf(getY()));
      p.setProperty("modules", String.valueOf(loadModules));
      p.setProperty("debug", String.valueOf(debug));
      p.setProperty("ownColor", String.valueOf(ownColor.getRGB()));
      p.setProperty("bgColor", String.valueOf(bgColor.getRGB()));
      p.setProperty("otherColor", String.valueOf(otherColor.getRGB()));
      p.setProperty("serverColor", String.valueOf(serverColor.getRGB()));
      p.list(new PrintWriter(new BufferedWriter(
          new FileWriter("moep.conf"))));
      p.store(out,"Settings-file of moepII -- You can edit, "+
          "but you might make the program crash. In any case of emergency "+
          "just delete this file.");
      out.close();
    } catch (Exception exc) {
      exc.printStackTrace();
    }
  }

// ------------------------------

  /** 
   * Loads the module-classes.<br />
   * Reads the modules-file <code>modules.conf</code> and loads the classes into
   * the <code>Class[] moduleClasses</code>. 
   */
  private void loadModules() {
    try {
      Properties p = new Properties();
      FileInputStream in = new FileInputStream("modules.conf");
      p.load(in);
      String classname;
      ArrayList al = new ArrayList(); 
      for (int i = 0; (classname = p.getProperty("module"+ i)) != null; i++) { 
        try { 
          al.add(Class.forName(classname));
        } catch (Exception exc) {
          JOptionPane.showMessageDialog(null,"The module "+ classname +
              " could not be loaded.", "Module Error", 
              JOptionPane.ERROR_MESSAGE); 
        }
      }
      in.close();

      int count = al.size();
      moduleClasses = new Class[count];
      moduleInstances = new Object[count]; // all null at the moment
      al.toArray(moduleClasses);
    } catch (Exception exc) {
      moduleClasses = new Class[0];
    }
  }

// ------------------------------

  /**
   * Checks all modules and starts those which are intended to be started when
   * connected. 
   */
  protected void autoStartModules() {
    if (moduleClasses == null)
      return;
    for (int i = 0; i < moduleClasses.length; i++)
      if (moduleInstances[i] == null)
        if (Module.isAutoStart(moduleClasses[i])) 
          try {
            Object o = moduleClasses[i].newInstance();
            if (o instanceof Module)
              setMoep((Module)o);
            moduleInstances[i] = o;
            updateTab(Util.CONSOLEWINDOWINDEX, "# Automatically started "+
                "Module: "+ moduleClasses[i].getName(), serverColor);
          } catch (Exception exc) {
            JOptionPane.showMessageDialog(null, "A very bad error occured "+
                "when I tried to auto-start the module "+ moduleClasses[i] +".",
                "Module Error", JOptionPane.ERROR_MESSAGE); 
            exc.printStackTrace();
          }
  }


// ------------------------------

  /** 
   * Executes the perform.<br />This is done on every connection. 
   */
  protected void executePerform() {
    try {
      StringTokenizer st = new StringTokenizer(perform,"\n");
      int count = st.countTokens();
      for (int i = 0; i < count; i++) 
        parseCmd(st.nextToken()); 
    } catch (Exception exc) {
      exc.printStackTrace();
    }
  }

// ------------------------------

  /** 
   * Analyzes a hand-made command of the input-line.<br />
   * It parses it with the <code>org.schwering.irc.lib.IRCParser</code>, 
   * generates the line, which is printed out by the client and sends the 
   * command to the server.
   */
  public void parseCmd(String line) {
    if (isConnected()) {
      boolean show = true; // if /. it's hidden, by default shown
      line = line.trim(); 

      if (line.startsWith("/"))
        line = line.substring(1); 

      if (line.startsWith(".")) {
        line = line.substring(1); 
        show = false; 
      }
      
      IRCParser parser = new IRCParser(line, true);
      String cmd = parser.getCommand();
      
      if (cmd.equalsIgnoreCase("MSG")) { // PRIVMSG <chan> :<msg> alias
        String chan = parser.getParameter(1);
        String msg = line.substring(line.indexOf(parser.getParameter(1)) + 
            parser.getParameter(1).length() + 1);
        if (show)
          parsePrivmsg(chan, msg);
        else
          conn.doPrivmsg(chan, msg);
      } else if (cmd.equalsIgnoreCase("SAY")) { // PRIVMSG <ontop> :<msg> alias
        if (getSelectedIndex() != Util.CONSOLEWINDOWINDEX) { 
          String chan = ((AbstractPanel)getSelectedComponent()).getWindowName(); 
          String msg = line.substring(4);
          if (show)
            parsePrivmsg(chan, msg);
          else
            conn.doPrivmsg(chan, msg);
        }
      } else if (cmd.equalsIgnoreCase("NOT") 
          || cmd.equalsIgnoreCase("NOTICE")) { // NOTICE <chan> :<msg> alias
        String chan = parser.getParameter(1);
        String msg = line.substring(line.indexOf(parser.getParameter(1)) + 
            parser.getParameter(1).length() + 1);
        if (show)
          parseNotice(chan, msg);
        else
          conn.doNotice(chan, msg);
      } else if (cmd.equalsIgnoreCase("QUIT")) { // QUIT :<msg> alias
        conn.doQuit(parser.getParametersFrom(2)); 
        conn.close();
      } else if (line.length() > 0) {
        conn.send(line);
        if (show) {
          int index = getSelectedIndex();
          updateTab(index, "# Executed: "+ line, serverColor);
        }
      }
    }
  }

// ------------------------------

  /**
   * Sends and prints a <code>PRIVMSG</code>.
   * @param chan The channel or person to which the message is to be sent.
   * @param msg The message itself.
   */
  public void parsePrivmsg(String chan, String msg) {
    conn.doPrivmsg(chan, msg);
    int index = indexOfTab(chan);
    if (index != -1)
      updateTab(index, "<"+ conn.getNick() +"> "+ msg, ownColor);
    else
      updateTab(getSelectedIndex(), "* Message: To "+ chan +": "+ msg, 
          ownColor);
  }

// ------------------------------

  /**
   * Sends and prints a <code>NOTICE</code>.
   * @param chan The channel or person to which the message is to be sent.
   * @param msg The message itself.
   */
  public void parseNotice(String chan, String msg) {
    conn.doNotice(chan, msg);
    int index = getSelectedIndex();
    updateTab(index, "* Notice: To "+ chan +": "+ msg, ownColor);
  }

// ------------------------------

  /** 
   * Connects (after trying to disconnect) to the server by starting the
   * thread.
   */
  public void connect() {
    disconnect(); // maybe we must disconnect
    Thread threadConn = new Thread(this);
    threadConn.setDaemon(true);
    threadConn.start();
  }

// ------------------------------

  /** 
   * The method just changes the server-host and the ports and calls the 
   * <code>connect</code> method. The <code>portMin</code> and 
   * <code>portMax</code> class-vars are set to the same value so that only
   * one port is tried.
   * @param host The new IRC host.
   */
  public void connect(String host, int port) {
    this.host = host;
    this.portMin = port;
    this.portMax = port;
    connect();
  }

// ------------------------------

  /** 
   * Starts the thread which means the connection. 
   */
  public void run() {
    updateTab(Util.CONSOLEWINDOWINDEX, "# Trying to connect to "+ host, 
        serverColor);
    enableConnectMenuItem(false);
    username = username.toLowerCase();
    if (!useSSL) {
      conn = new IRCConnection(host, portMin, portMax, password, nickname, 
          username, realname);
    } else {
      SSLIRCConnection sslconn = new SSLIRCConnection(host, portMin, portMax, 
          password, nickname, username, realname);
      if (!autoAcceptSSLCerts)
        sslconn.addTrustManager(new AskTrustManager());
      conn = sslconn;
    }
    conn.addIRCEventListener(new Listener(this));
    conn.setColors(false); 
    conn.setPong(false); 
    conn.setDaemon(true);
    try {
      conn.connect();
      this.isConnected = conn.isConnected();
    } catch (Exception exc) {
      String reason = "";
      if (exc.getMessage() != null) {
        reason = ": "+ exc.getMessage();
      }
      updateTab(Util.CONSOLEWINDOWINDEX, "# Couldn't connect"+ reason, 
          serverColor);
      enableConnectMenuItem(true);
      exc.printStackTrace();
      this.isConnected = false;
    }
  }

// ------------------------------

  /** 
   * Quits from the IRC server.<br />
   * This method also sets the <code>boolean isConnected false</code>, and the 
   * <code>int lastChanIndex = 0</code> so that new channels are added at the 
   * correct position. The method also <code>halt</code>s all instantiated 
   * modules.
   */
  public void disconnect() {
    if (isConnected()) { // danger: infinite loop
      for (int i = tabs.getTabCount()-1; i > 0; i--)
        tabs.remove(i);
      updateTab(Util.CONSOLEWINDOWINDEX, "# Disconnected", serverColor);
      enableConnectMenuItem(true);
    }
    isConnected = false;
    lastChanIndex = 0;

    if (moduleInstances != null) {
      for (int i = 0; i < moduleInstances.length; i++) {
        try {
          haltModule(i);
        } catch (Exception exc) {
          // nothing
        }
      }
    }
  }

// ------------------------------

  /** 
   * Quits from the network and shuts the virtual machine down. 
   */  
  private void quit() {
    if (!isConnected() || Util.confirmDialog("Do you really want to quit?",
        "Quit")) {
      if (isConnected()) {
        conn.doQuit(quitMsg);
        conn.close();
      }
      saveSettings(); 
      System.exit(0);
    } 
  }

// ------------------------------

  /** 
   * Creates the MenuBar for the GUI. 
   * @return The complete <code>JMenuBar</code>.
   */
  private JMenuBar createMenuBar() {
    JMenuBar mbMenu = new JMenuBar();  
    // MenuBar -> File
    JMenu menuFile = new JMenu("File");
    menuFile.setMnemonic('F');
    // MenuBar -> File -> Connect
    menuItemConnect = new JMenuItem("Connect"); // class-field!
    menuItemConnect.setMnemonic('C');
    menuItemConnect.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        connect(); 
      } 
    } );
    menuFile.add(menuItemConnect);
    // MenuBar -> Options -> Connect to specified Server
    menuItemConnectTo = new JMenuItem("Connect to ..."); // class-field!
    menuItemConnectTo.setMnemonic('t');
    menuItemConnectTo.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        String input = JOptionPane.showInputDialog(null, "Please put in the "+
            "server:port you want to connect to.");
        if (input == null)
          return;
        String server = null;
        int port = -1;
        try {
          String[] tmp = input.split(":");
          server = tmp[0];
          port = Integer.parseInt(tmp[1]);
        } catch (Exception exc) {
          JOptionPane.showMessageDialog(null, "Something was wrong...", 
              "Malformed Input", JOptionPane.ERROR_MESSAGE);
          return;
        }
        useSSL = Util.confirmDialog("Is this an SSL connection?", "Use SSL?");
        connect(server, port); 
      } 
    } );
    menuFile.add(menuItemConnectTo);
    // MenuBar -> File -> Disconnect
    menuItemDisconnect = new JMenuItem("Disconnect"); // class-field!
    menuItemDisconnect.setMnemonic('D');
    menuItemDisconnect.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        if (isConnected())
          conn.doQuit(quitMsg);
          conn.close();
      } 
    } );
    menuFile.add(menuItemDisconnect);
    menuFile.addSeparator();
    // MenuBar -> File -> Setup
    JMenuItem menuItemSetup = new JMenuItem("Setup");
    menuItemSetup.setMnemonic('S');
    menuItemSetup.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        openSetupDialog();
      } 
    } );
    menuFile.add(menuItemSetup);
    menuFile.addSeparator();
    // MenuBar -> File -> Exit
    JMenuItem menuItemExit = new JMenuItem("Exit");
    menuItemExit.setMnemonic('x');
    menuItemExit.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        quit(); 
      } 
    } );
    menuFile.add(menuItemExit);

    // MenuBar -> Options
    JMenu menuOptions = new JMenu("Options");
    menuOptions.setMnemonic('O');
    // MenuBar -> Options -> ChanCenter
    JMenuItem menuItemChanCenter = new JMenuItem("ChanCenter");
    menuItemChanCenter.setMnemonic('C');
    menuItemChanCenter.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (component instanceof ChanPanel) 
          openChanCenter((ChanPanel)component);
      }
    } );
    menuOptions.add(menuItemChanCenter);
    // MenuBar -> Options -> PasteDialog
    JMenuItem menuItemPasteDialog = new JMenuItem("PasteDialog");
    menuItemPasteDialog.setMnemonic('P');
    menuItemPasteDialog.setAccelerator(KeyStroke.getKeyStroke('P', 
        Event.CTRL_MASK));
    menuItemPasteDialog.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (component instanceof ChanPanel || component instanceof QueryPanel) {
          String chan = ((AbstractPanel)component).getWindowName();
          openPasteDialog(chan);
        }
      }
    } );
    menuOptions.add(menuItemPasteDialog);
    menuOptions.addSeparator();
    // MenuBar -> Options -> Join
    JMenuItem menuItemJoin = new JMenuItem("Join");
    menuItemJoin.setMnemonic('J');
    menuItemJoin.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        String input = JOptionPane.showInputDialog(null,"Channel?");
        if (isConnected() && input != null) 
          conn.doJoin(input); 
      } 
    } );
    menuOptions.add(menuItemJoin);
    // MenuBar -> Options -> Part
    JMenuItem menuItemPart = new JMenuItem("Part");
    menuItemPart.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        String input = JOptionPane.showInputDialog(null,"Channel?");
        if (isConnected() && input != null) 
          conn.doPart(input); 
      } 
    } );
    menuOptions.add(menuItemPart);
    // MenuBar -> Options -> Nickname
    JMenuItem menuItemNickchange = new JMenuItem("Nickchange");
    menuItemNickchange.setMnemonic('N');
    menuItemNickchange.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        String input = JOptionPane.showInputDialog(null,"New Nickname?");
        if (input != null) { 
          setNick(input); 
          if (isConnected()) 
            conn.doNick(input); 
        } 
      } 
    } );
    menuOptions.add(menuItemNickchange);
    // MenuBar -> Options -> Whois
    JMenuItem menuItemWhois = new JMenuItem("Whois");
    menuItemWhois.setMnemonic('W');
    menuItemWhois.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        String input = JOptionPane.showInputDialog(null,"Nickname?");
        if (isConnected() && input != null) 
          conn.doWhois(input); 
      } 
    } );
    menuOptions.add(menuItemWhois);
    menuOptions.addSeparator();
    // MenuBar -> Options -> Execute Perform
    JMenuItem menuItemWindowExecutePerform = new JMenuItem("Execute Perform");
    menuItemWindowExecutePerform.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        executePerform();
        JOptionPane.showMessageDialog(null,"The perform was executed again.", 
            "Perform Executed", JOptionPane.INFORMATION_MESSAGE);
      } 
    } );
    menuOptions.add(menuItemWindowExecutePerform);
    // MenuBar -> Options -> Away (-> Away && -> Back)
    JMenu menuAway = new JMenu("Away");
    menuAway.setMnemonic('A');
    // Away
    JMenuItem menuItemAway = new JMenuItem("Away");
    menuItemAway.setMnemonic('A');
    menuItemAway.setAccelerator(KeyStroke.getKeyStroke(
        KeyEvent.VK_F1,Event.CTRL_MASK));
    menuItemAway.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) {
        String input = JOptionPane.showInputDialog(null,"Awaymessage?");
        if (isConnected() && input != null) 
          conn.doAway(input);
      } 
    } );
    // Back
    JMenuItem menuItemBack = new JMenuItem("Back");
    menuItemBack.setMnemonic('B');
    menuItemBack.setAccelerator(KeyStroke.getKeyStroke(
        KeyEvent.VK_F2,Event.CTRL_MASK));
    menuItemBack.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) {
        if (isConnected()) 
          conn.doAway(); 
      } 
    } );
    menuAway.add(menuItemAway);
    menuAway.add(menuItemBack);
    menuOptions.add(menuAway);
    // MenuBar -> Options -> Window (-> Lock && -> Unlock && -> Close)
    JMenu menuWindow = new JMenu("Window");
    menuWindow.setMnemonic('W');
    // Lock
    JMenuItem menuWindowLock = new JMenuItem("Lock");
    menuWindowLock.setMnemonic('L');
    menuWindowLock.setAccelerator(KeyStroke.getKeyStroke(
        KeyEvent.VK_L,Event.CTRL_MASK));
    menuWindowLock.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) {
        ((AbstractPanel)getSelectedComponent()).lockScrollbar(true);
      } 
    } );
    JMenuItem menuWindowUnlock = new JMenuItem("Unlock");
    menuWindowUnlock.setMnemonic('U');
    menuWindowUnlock.setAccelerator(KeyStroke.getKeyStroke(
        KeyEvent.VK_U,Event.CTRL_MASK));
    menuWindowUnlock.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) {
        ((AbstractPanel)getSelectedComponent()).lockScrollbar(false);
      } 
    } );
    // Close (brutal)
    JMenuItem menuItemWindowCloseWindowBrutal = new JMenuItem("Close");
    menuItemWindowCloseWindowBrutal.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (!(component instanceof ConsolePanel)) {
          String chan = ((AbstractPanel)component).getWindowName();
          if (isConnected() && component instanceof ChanPanel) 
            conn.doPart(chan);
          closePanel(chan); 
        }
      } 
    } );
    menuWindow.add(menuWindowLock);
    menuWindow.add(menuWindowUnlock);
    menuWindow.addSeparator();
    menuWindow.add(menuItemWindowCloseWindowBrutal);
    menuOptions.add(menuWindow);
    
    // nicklistmenu done by getNicklistMenu, added later

    // selectionmenu done by getSelectionMenu, added later

    // modules done by getModulesMenu, added later

    // MenuBar -> Help
    JMenu menuHelp = new JMenu("Help");
    menuHelp.setMnemonic('H');
    // MenuBar -> Help -> Info
    JMenuItem menuItemInfo = new JMenuItem("Info");
    menuItemInfo.setMnemonic('I');
    menuItemInfo.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        String info =  "moepII "+ VERSION +"\n"+
                       "    http://moepii.sourceforge.net\n"+
                       "\n"+
                       "Author:\n"+
                       "    Christoph Schwering\n"+
                       "    ch@schwering.org\n"+
                       "    http://www.schwering.org";
        JOptionPane.showMessageDialog(null, info, "moepII "+ VERSION, 
            JOptionPane.INFORMATION_MESSAGE, 
            (icon != null) ? new ImageIcon(icon) : null); 
      } 
    } );
    menuHelp.add(menuItemInfo);
    // MenuBar -> Help -> License
    JMenuItem menuItemLicense = new JMenuItem("License");
    menuItemLicense.setMnemonic('L');
    menuItemLicense.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        String copyright = "Copyright (C) 2002, 2003 Christoph Schwering "+
                           "<ch@schwering.org>\n\n"+
                           "This program is free software; you can "+
                           "redistribute it and/or\n"+
                           "modify it under the terms of the GNU General "+
                           "Public License as\n"+
                           "published by the Free Software Foundation; "+
                           "either version 2 of\n"+
                           "the License. \n\n"+
                           "This program is distributed in the hope that it "+
                           "will be useful,\n"+
                           "but WITHOUT ANY WARRANTY; without even the "+
                           "implied warranty of\n"+
                           "MERCHANTABILITY or FITNESS FOR A PARTICULAR "+
                           "PURPOSE. See the \n"+
                           "GNU General Public License for more details.\n\n"+
                           "You should have received a copy of the GNU "+
                           "General Public License\n"+
                           "along with this program; if not, write to the "+
                           "Free Software Foundation,\n"+
                           "Inc., 59 Temple Place, Suite 330, Boston, MA "+
                           "02111-1307, USA.";
        JOptionPane.showMessageDialog(null, copyright, "License",
            JOptionPane.INFORMATION_MESSAGE); 
      } 
    } );
    menuHelp.add(menuItemLicense);
    // MenuBar -> Help -> Versioncheck
    JMenuItem menuItemCheckVersion = new JMenuItem("Version");
    menuItemCheckVersion.setMnemonic('V');
    menuItemCheckVersion.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        new Thread() { 
          public void run() {
            try {
              //opens http://moepii.sourceforge.net/VERSION.TXT and reads 1 line
              String url = "http://moepii.sourceforge.net/VERSION.TXT";
              String thisv = VERSION;
              String latestv = new BufferedReader(new InputStreamReader(
                  new URL(url).openStream())).readLine();
              double dlatestv = Double.parseDouble(latestv);
              double dthisv = Double.parseDouble(VERSION);
              String msg;

              if (dthisv < dlatestv)
                msg = "You've got an old version (moepII "+ 
                    thisv +").\nYou can download moepII "+
                    latestv +" from http://moepii.sf.net";
              else if (dthisv > dlatestv)
                msg = "Probably your version "+ thisv +" is "+
                    "testing release.\nThe latest stable release is moepII "+
                    latestv +" and available on "+
                    "http://moepii.sf.net.";
              else
                msg = "You have the latest stable release of moepII ("+
                    thisv +").\nFor more information check "+
                    "moepII's homepage: http://moepii.sf.net.";
                  
              JOptionPane.showMessageDialog(null, msg, "Check Version",
                  JOptionPane.INFORMATION_MESSAGE); 
            } catch (Exception exc) {
              JOptionPane.showMessageDialog(null,"Sorry, an error occured.\n"+
                  "Please check http://moepii.sourceforge.net\nmanually for "+
                  "new versions.", "Connection Error", 
                  JOptionPane.ERROR_MESSAGE);
            }
          }
        }.start();
      } 
    } );
    menuHelp.add(menuItemCheckVersion);



    mbMenu.add(menuFile);
    mbMenu.add(menuOptions);
    mbMenu.add(getNicklistMenu());
    mbMenu.add(getSelectionMenu());
    // MenuBar -> Modules
    JMenu menuModules = getModulesMenu();
    if (this.loadModules && menuModules != null) 
      mbMenu.add(menuModules);
    mbMenu.add(menuHelp);

    enableConnectMenuItem(true); 

    return mbMenu;
  }

// ------------------------------

  /**
   * Generates a new <code>JMenu</code> for the menubar with the selection
   * options. 
   * @return The selection <code>JMenu</code>.
   */
  private JMenu getSelectionMenu() {
    JMenu menuSelection = new JMenu("Selection");
    menuSelection.setMnemonic('S');
    // MenuBar -> Selection -> Copy
    JMenuItem menuItemCopy = new JMenuItem("Copy");
    menuItemCopy.setAccelerator(KeyStroke.getKeyStroke('C',Event.CTRL_MASK));
    menuItemCopy.setMnemonic('C');
    menuItemCopy.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        ((AbstractPanel)component).copySelection(); 
      } 
    } );
    menuSelection.add(menuItemCopy);
    // MenuBar -> Selection -> Paste
    JMenuItem menuItemPaste = new JMenuItem("Paste");
    menuItemPaste.setMnemonic('P');
    menuItemPaste.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        ((AbstractPanel)component).pasteLine(); 
      } 
    } );
    menuSelection.add(menuItemPaste);
    menuSelection.addSeparator();
    // MenuBar -> Selection -> Open in Browser
    JMenuItem menuItemBrowser = new JMenuItem("Open in Browser");
    menuItemBrowser.setAccelerator(KeyStroke.getKeyStroke('O',
        Event.CTRL_MASK));
    menuItemBrowser.setMnemonic('O');
    menuItemBrowser.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        try {
          Runtime.getRuntime().exec(browserPath +" "+ getSelection());
        } catch (Exception exc) {
          updateTab(getSelectedIndex(), "# Could not open Browser: "+ 
              browserPath, serverColor);
        }
      } 
    } );
    menuSelection.add(menuItemBrowser);
    // MenuBar -> Selection -> Join
    JMenuItem menuItemJoinSelection = new JMenuItem("Join (Channel)");
    menuItemJoinSelection.setAccelerator(KeyStroke.getKeyStroke('J',
        Event.CTRL_MASK));
    menuItemJoinSelection.setMnemonic('J');
    menuItemJoinSelection.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        if (!isConnected())
          return;
        String chan = getSelection().trim();
        if (IRCUtil.isChan(chan)) {
          conn.doJoin(chan); 
        } else if (chan.length() > 0) {
          conn.doJoin("#"+ chan);
        }
      } 
    } );
    menuSelection.add(menuItemJoinSelection);
    // MenuBar -> Selection -> Whois
    JMenuItem menuItemWhoisUser = new JMenuItem("Whois (User)");
    menuItemWhoisUser.setMnemonic('W');
    menuItemWhoisUser.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        if (!isConnected())
          return;
        conn.doWhois(getSelection()); 
      } 
    } );
    menuSelection.add(menuItemWhoisUser);
    return menuSelection;
  }

// ------------------------------

  /**
   * Generates a <code>JMenu</code> with all options related with the nicklist
   * and a nickname selected in the nicklist. 
   * @return The nicklist <code>JMenu</code>.
   */
  private JMenu getNicklistMenu() {
    JMenu menuNicklist = new JMenu("Nicklist");
    menuNicklist.setMnemonic('N');
    // MenuBar -> Nicklist -> Modes
    JMenu menuModenicklist = new JMenu("Modes");
    menuModenicklist.setMnemonic('M');
    menuNicklist.add(menuModenicklist);
    // MenuBar -> Nicklist -> +Operator
    JMenuItem menuItemPosOpNicklistModes = new JMenuItem("+ Op");
    menuItemPosOpNicklistModes.setMnemonic('O');
    menuItemPosOpNicklistModes.setAccelerator(KeyStroke.getKeyStroke('.',
        Event.CTRL_MASK));
    menuItemPosOpNicklistModes.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (component instanceof ChanPanel) { 
          ChanPanel chanPanel = (ChanPanel)component;
          String[] nicks = chanPanel.getSelectedNicks();
          char[] modes = new char[nicks.length+1];
          modes[0] = '+';
          for (int i = 1; i < modes.length; i++)
            modes[i] = 'o';
          conn.doMode(chanPanel.getWindowName(), new String(modes) +" "+ 
              Util.formatArray(nicks, ' '));
        }
      } 
    } );
    menuModenicklist.add(menuItemPosOpNicklistModes);
    // MenuBar -> Nicklist -> -Operator
    JMenuItem menuItemNegOpNicklistModes = new JMenuItem("- Op");
    menuItemNegOpNicklistModes.setMnemonic('O');
    menuItemNegOpNicklistModes.setAccelerator(KeyStroke.getKeyStroke(',',
        Event.CTRL_MASK));
    menuItemNegOpNicklistModes.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (component instanceof ChanPanel) { 
          ChanPanel chanPanel = (ChanPanel)component;
          String nicks[] = chanPanel.getSelectedNicks();
          char[] modes = new char[nicks.length+1];
          modes[0] = '-';
          for (int i = 1; i < modes.length; i++)
            modes[i] = 'o';
          conn.doMode(chanPanel.getWindowName(), new String(modes) +" "+ 
              Util.formatArray(nicks, ' '));
        }
      } 
    } );
    menuModenicklist.add(menuItemNegOpNicklistModes);
    menuModenicklist.addSeparator();
    // MenuBar -> Nicklist -> +Voice
    JMenuItem menuItemPosVoiceNicklistModes = new JMenuItem("+ Voice");
    menuItemPosVoiceNicklistModes.setMnemonic('V');
    menuItemPosVoiceNicklistModes.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (component instanceof ChanPanel) { 
          ChanPanel chanPanel = (ChanPanel)component;
          String nicks[] = chanPanel.getSelectedNicks();
          char[] modes = new char[nicks.length+1];
          modes[0] = '+';
          for (int i = 1; i < modes.length; i++)
            modes[i] = 'v';
          conn.doMode(chanPanel.getWindowName(), new String(modes) +" "+ 
              Util.formatArray(nicks, ' '));
        }
      } 
    } );
    menuModenicklist.add(menuItemPosVoiceNicklistModes);
    // MenuBar -> Nicklist -> -Voice
    JMenuItem menuItemNegVoiceNicklistModes = new JMenuItem("- Voice");
    menuItemNegVoiceNicklistModes.setMnemonic('V');
    menuItemNegVoiceNicklistModes.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (component instanceof ChanPanel) { 
          ChanPanel chanPanel = (ChanPanel)component;
          String nicks[] = chanPanel.getSelectedNicks();
          char[] modes = new char[nicks.length+1];
          modes[0] = '-';
          for (int i = 1; i < modes.length; i++)
            modes[i] = 'v';
          conn.doMode(chanPanel.getWindowName(), new String(modes) +" "+ 
              Util.formatArray(nicks, ' '));
        }
      } 
    } );
    menuModenicklist.add(menuItemNegVoiceNicklistModes);
    menuModenicklist.addSeparator();
    // MenuBar -> Nicklist -> Kick
    JMenuItem menuItemKickNicklistModes = new JMenuItem("Kick");
    menuItemKickNicklistModes.setMnemonic('K');
    menuItemKickNicklistModes.setAccelerator(KeyStroke.getKeyStroke('K',
        Event.CTRL_MASK));
    menuItemKickNicklistModes.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (component instanceof ChanPanel) { 
          ChanPanel chanPanel = (ChanPanel)component;
          String[] nicks = chanPanel.getSelectedNicks();
          String chan = chanPanel.getWindowName();
          for (int i = 0; i < nicks.length; i++)
            conn.doKick(chan, nicks[i]);
        }
      } 
    } );
    menuModenicklist.add(menuItemKickNicklistModes);
    // MenuBar -> Nicklist -> Modes -> Kick (Msg)
    JMenuItem menuItemKickMsgNicklistModes = new JMenuItem("Kick (Msg)");
    menuItemKickMsgNicklistModes.setMnemonic('K');
    menuItemKickMsgNicklistModes.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (component instanceof ChanPanel) { 
          ChanPanel chanPanel = (ChanPanel)component;
          String[] nicks = chanPanel.getSelectedNicks();
          String chan = chanPanel.getWindowName();
          if (nicks.length == 0)
            return;
          String msg = JOptionPane.showInputDialog(null, "Kickmessage?");
          if (msg == null)
            msg = "";
          for (int i = 0; i < nicks.length; i++) 
            conn.doKick(chan, nicks[i]);
        }
      } 
    } );
    menuModenicklist.add(menuItemKickMsgNicklistModes);
    // MenuBar -> Nicklist -> Slap
    JMenuItem menuItemSlapNicklist = new JMenuItem("Slap");
    menuItemSlapNicklist.setAccelerator(KeyStroke.getKeyStroke('S',
        Event.CTRL_MASK));
    menuItemSlapNicklist.setMnemonic('S');
    menuItemSlapNicklist.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        AbstractPanel panel = (AbstractPanel)getSelectedComponent();
        String target = panel.getWindowName();
        String[] nicks = new String[0];
        if (panel instanceof ChanPanel) {
          ChanPanel chanPanel = (ChanPanel)panel;
          nicks = chanPanel.getSelectedNicks();
        } else if (panel instanceof QueryPanel) {
          QueryPanel queryPanel = (QueryPanel)panel;
          nicks = new String[] { queryPanel.getWindowName() };
        }
        if (nicks.length > 0) {
          String line = "slaps "+ Util.formatArray(nicks, ' ') +" with a "+ 
              "pizza from 74883";
          conn.doPrivmsg(target, line);
          // here we go directly to the method of the innerclass and NOT to 
          // the updateText(String chan, String line) method, because this 
          // is faster AND more useful because of the boolean for the nickname
          // which brings us our nickname in front of the msg automatically
          panel.updateText(line, ownColor, true); 
        }
      } 
    } );
    menuNicklist.add(menuItemSlapNicklist);
    // MenuBar -> Nicklist -> Whois
    JMenuItem menuItemWhoinicklist = new JMenuItem("Whois");
    menuItemWhoinicklist.setMnemonic('W');
    menuItemWhoinicklist.setAccelerator(KeyStroke.getKeyStroke('W',
        Event.CTRL_MASK));
    menuItemWhoinicklist.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (component instanceof ChanPanel) { 
          ChanPanel chanPanel = (ChanPanel)component;
          String[] nicks = chanPanel.getSelectedNicks();
          if (nicks.length > 0)
            for (int i = 0; i < nicks.length; i++)
              conn.doWhois(nicks[i]);
          else {
            String nick = getSelection();
            if (nick != null)
              conn.doWhois(nick);
          }
        } else if (component instanceof QueryPanel) {
          QueryPanel queryPanel = (QueryPanel)component;
          String nick = getSelection();
          if (nick == null || nick.length() == 0)
            nick = queryPanel.getWindowName();
          if (nick != null)
            conn.doWhois(nick);
        }
      } 
    } );
    menuNicklist.add(menuItemWhoinicklist);
    // MenuBar -> Nicklist -> Open Query
    JMenuItem menuItemOpenQueryNicklist = new JMenuItem("Open Query");
    menuItemOpenQueryNicklist.setMnemonic('Q');
    menuItemOpenQueryNicklist.setAccelerator(KeyStroke.getKeyStroke('Q',
        Event.CTRL_MASK));
    menuItemOpenQueryNicklist.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (component instanceof ChanPanel) {
          String[] nicks = ((ChanPanel)component).getSelectedNicks();
          for (int i = 0; i < nicks.length; i++)
            addQuery(nicks[i], true);
        }
      } 
    } );
    menuNicklist.add(menuItemOpenQueryNicklist);
    // MenuBar -> Nicklist -> Order List
    JMenuItem menuItemOrderNicklist = new JMenuItem("Order List");
    menuItemOrderNicklist.setMnemonic('L');
    menuItemOrderNicklist.setAccelerator(KeyStroke.getKeyStroke('N',
        Event.CTRL_MASK));
    menuItemOrderNicklist.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (component instanceof ChanPanel)
          ((ChanPanel)component).orderList();
      } 
    } );
    menuNicklist.add(menuItemOrderNicklist);
    return menuNicklist;
  }

// ------------------------------

  /** 
   * Generates a <code>Menu</code> with the modules loaded by 
   * <code>loadModules</code> into the classfield <code>moduleClasses</code>. 
   * Every successfully loaded class is marked with one <code>MenuItem</code> 
   * which gets a <code>ActionListener</code> in which the object is 
   * instantiated and, if the module extends <code>MoepIRC.Module</code> class, 
   * a <code>ModuleListener</code> is added. 
   * @return The modules' <code>Menu</code>.
   */
  private JMenu getModulesMenu() {
    if (moduleClasses == null || moduleClasses.length <= 0)
      return null; // -> no menu displayed
  
    JMenu menuModules = new JMenu("Modules");
    menuModules.setMnemonic('M');

    // JMenuBar -> Modules -> <Every Module one JMenu> -> Start (& Halt)
    int lastDot; // dot between package and classname
    String classname; 
    Class cls; 
    JMenu menuModule;
    ModuleMenuItem menuModuleStart; 
    ModuleMenuItem menuModuleHalt; 
    
    for (int i = 0; i < moduleClasses.length; i++) {
      cls = moduleClasses[i];
      classname = cls.getName(); // set classname
      if ((lastDot = classname.lastIndexOf('.')) != -1) // cut package
        classname = classname.substring(lastDot + 1); 
      // the name in the menu is: <i> <name> 
      menuModule = new JMenu(classname);
      menuModule.setMnemonic(classname.charAt(0));
      menuModuleStart = new ModuleMenuItem("Execute", i);
      menuModuleStart.setMnemonic('E');
      menuModuleStart.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) { 
          // creates new instance; if there's a halt() method in the module
          // the object is added to the moduleInstances array
          try {
            if (!isConnected())
              throw new Exception("Not connected");
            int moduleIndex = ((ModuleMenuItem)e.getSource()).getIndex();
            Class cls = moduleClasses[moduleIndex];
            if (moduleIndex != -1 && moduleInstances[moduleIndex] == null) {
              Object instance = cls.newInstance(); 
              if (instance instanceof Module) {
                setMoep((Module)instance);
                if (Module.isHaltable(cls))
                  moduleInstances[moduleIndex] = instance;
              }
            }
          } catch (Exception exc) {
            JOptionPane.showMessageDialog(null, "The module could not be "+
                "accessed.\nProbably it has privileged access or you're "+
                "disconnected.", "Module Error", JOptionPane.ERROR_MESSAGE);
            exc.printStackTrace();
          }
        }
      } );
      menuModule.add(menuModuleStart);
      
      // add a Halt-menuitem if it's haltable
      if (Module.isHaltable(cls)) {
        menuModuleHalt = new ModuleMenuItem("Halt", i);
        menuModuleHalt.setMnemonic('H');
        menuModuleHalt.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            try {
              int moduleIndex = ((ModuleMenuItem)e.getSource()).getIndex();
              haltModule(moduleIndex);
            } catch (Exception exc) {
              JOptionPane.showMessageDialog(null, "The module could not be "+
                  "halted.", "Module Error", JOptionPane.ERROR_MESSAGE);
              exc.printStackTrace();
            }
          }
        } );
        menuModule.add(menuModuleHalt);
      }
      menuModules.add(menuModule);
    }

    return menuModules;
  }

// ------------------------------

  /**
   * Generates a new <code>JPopupMenu</code> for the right-click popup of the
   * <code>JTabbedPane</code>. 
   * @return The tabs' <code>JPopupMenu</code>.
   */
  private JPopupMenu getTabsPopupMenu() {
    JPopupMenu popup = new JPopupMenu("Tabs");
    // Tabs -> Part
    JMenuItem menuItemPart = new JMenuItem("Part");
    menuItemPart.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (!(component instanceof ConsolePanel)) {
          String chan = ((AbstractPanel)component).getWindowName();
          if (isConnected() && component instanceof ChanPanel) 
            conn.doPart(chan);
          else if (component instanceof QueryPanel)
            closePanel(chan);
        }
      } 
    } );
    popup.add(menuItemPart);
    // Tabs -> Close (brutal)
    JMenuItem menuItemClose = new JMenuItem("Close Brutally");
    menuItemClose.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Component component = getSelectedComponent();
        if (!(component instanceof ConsolePanel)) {
          String chan = ((AbstractPanel)component).getWindowName();
          if (isConnected() && component instanceof ChanPanel) 
            conn.doPart(chan);
          closePanel(chan); 
        }
      } 
    } );
    popup.add(menuItemClose);
    return popup;
  }

// ------------------------------

  /** 
   * Enables and disables the menuitems File -&lgt; Connect and File -&gt; 
   * Disconnect so that they're always the opposite. 
   * @param enableConnect If <code>true</code>, the connect-menuitems are
   *                      enabled and the disconnect-menuitem is disabled.
   */
  private void enableConnectMenuItem(boolean enableConnect) {
    menuItemConnect.setEnabled(enableConnect);
    menuItemConnectTo.setEnabled(enableConnect);
    menuItemDisconnect.setEnabled(!enableConnect);
  }
// ------------------------------

  /** 
   * Halts a module. To do that, it sets the <code>moduleInstances</code>-entry 
   * <code>false</code> and tries to execute the <code>halt</code> method of 
   * the module. 
   * @param moduleIndex The module's index which was stored in a 
   *                    <code>ModuleMenuItem</code>.
   * @throws Exception If anything fails, i.e. the module is out of bounds or
   *                   <code>null</code>.
   */
  private void haltModule(int moduleIndex) throws Exception {
    Object instance = moduleInstances[moduleIndex];
    ((Module)instance).halt();
    instance = null;
    moduleInstances[moduleIndex] = null;
  }

// ------------------------------

  /** 
   * Returns the selected text of an <code>AbstractPanel</code>.
   * @return The selected text of an <code>AbstractPanel</code>.
   */
  public String getSelection() {
    String s = "";
    Component component = getSelectedComponent();
    AbstractPanel panel = (AbstractPanel)component;
    s = panel.getSelection();
    panel.removeSelection();
    return s;
  }
  
// ------------------------------

  /** 
   * Returns the selected <code>Component</code>. 
   * @return The selected <code>Component</code>. 
   */
  public Component getSelectedComponent() {
    int index = getSelectedIndex();
    return tabs.getComponentAt(index);
  }
  
// ------------------------------

  /** 
   * Returns the selected index in the tabs.
   * @return The selected index.
   */
  public int getSelectedIndex() {
    return tabs.getSelectedIndex();
  }

// ------------------------------

  /** 
   * Returns the index of a channel in the tabs.
   * @param title The title which is to search.
   * @return The index of a channel or <code>-1</code> if nothing is found.
   */
  public int indexOfTab(String title) {
    return tabs.indexOfTab(title);
  }

// ------------------------------

  /** 
   * Returns the <code>Component</code> of a tab.
   * @param index The tab's index.
   * @return The <code>Component</code> of the tab at <code>index</code>.
   */
  public Component getComponentAt(int index) {
    return tabs.getComponentAt(index);
  }

// ------------------------------

  /** 
   * Returns the title of a tab.
   * @param index The tab's index.
   * @return The name of the tab at <code>index</code>.
   */
  public String getTitleAt(int index) {
    return tabs.getTitleAt(index);
  }

// ------------------------------

  /** 
   * Sets the title of a tab.
   * @param index The tab's index.
   * @param title The new title.
   */
  public void setTitleAt(int index, String title) {
    tabs.setTitleAt(index, title);
  }

// ------------------------------

  /** 
   * Returns the count of tabs.
   * @return The count of tabs.
   */
  public int getTabCount() {
    return tabs.getTabCount();
  }

// ------------------------------

  /** 
   * A highlighted chan should be unhighlighted when it gets selected. That is 
   * done by this method. 
   */
  public void setSelectedTab() {
    if (tabs.getModel().isSelected()) {
      int index = getSelectedIndex();
      String title = tabs.getTitleAt(index);
      tabs.setBackgroundAt(index, null);
      tabs.revalidate();
      setTitle("moepII - "+ title);
      ((AbstractPanel)tabs.getComponentAt(index)).requestFocus();
    }
  }

// ------------------------------

  /** 
   * Analyzes a hand-made command of the input-line with the 
   * <code>org.schwering.irc.lib.IRCParser</code>, generates the line, which is 
   * printed out by the client and sends the command to the server. 
   */
  public void doSoundHighlight() {
    if (querySoundHighlight) 
      Toolkit.getDefaultToolkit().beep();
  }

// ------------------------------

  /** 
   * Changes the background of a given tab.<br />
   * The tab is given as its name in a string. 
   * @param title The tab's title.
   */
  public void doTabHighlight(String title) {
    int index = indexOfTab(title);
    doTabHighlight(index);
  }

// ------------------------------

  /** 
   * Changes the background of a given tab.<br />
   * The tab is given as its index. 
   * @param index The tab's index.
   */
  private void doTabHighlight(int index) {
    if (getSelectedIndex() != index) {
      tabs.setBackgroundAt(index, highlightColor);
      tabs.revalidate();
    }
  }

// ------------------------------

  /** 
   * Changes the content of a chan-textarea. <br />
   * It uses the <code>updateTab(int, String, String, Color)</code> method. 
   * @param title The tab's title.
   * @param line The new line.
   * @param c The line's color.
   * @return The tab's index.
   */
  public int updateTab(String title, String line, Color c) {
    int index = indexOfTab(title);
    return updateTab(index, title, line, c);
  }
  
// ------------------------------

  /** 
   * Changes the content of a chan-textarea. <br />
   * It uses the <code>updateTab(int, String, String, Color)</code> method. 
   * @param index The tab's index.
   * @param line The new line.
   * @param c The line's color.
   * @return The tab's name.
   */
  public String updateTab(int index, String line, Color c) {
    String title = tabs.getTitleAt(index);
    updateTab(index, title, line, c);
    return title;
  }
  
// ------------------------------

  /** 
   * Changes the content of a chan-textarea. This method is used by the 
   * <code>updateTab(int, String)</code> and 
   * <code>updateTab(String, String)</code> methods. 
   * @param index The tab's index.
   * @param title The tab's title.
   * @param line The new line.
   * @param c The line's color.
   * @return The tab's possibly new index.
   */
  public int updateTab(int index, String title, String line, Color c) {
    if (index == -1) { 
      if (IRCUtil.isChan(title)) {
        addChan(title);
      } else {
        addQuery(title, false);
      }
      index = indexOfTab(title);
    }
    Component component = tabs.getComponentAt(index);
    ((AbstractPanel)component).updateText(line, c); 
    return index;
  }

// ------------------------------

  /** 
   * Adds a nickname to the nicklist of a given channel. Uses the 
   * <code>addNick(int, String)</code> method. 
   * @param chan The channel's name.
   * @param nicks The nickname array which is to add.
   */
  public void addNicks(String chan, String[] nicks) {
    int index = indexOfTab(chan);
    addNicks(index, nicks);
  }

// ------------------------------

  /** 
   * Adds a nickname to the nicklist of a given channel. 
   * @param index The channel's index.
   * @param nicks The nickname array which is to add.
   */
  public void addNicks(int index, String[] nicks) {
    if (index != -1) {
      Component component = tabs.getComponentAt(index);
      ChanPanel chanPanel = (ChanPanel)component;
      for (int i = 0; i < nicks.length; i++)
        chanPanel.addNick(nicks[i]);
    }
  }

// ------------------------------

  /** 
   * Adds a nickname to the nicklist of a given channel. Uses the 
   * <code>addNick(int, String)</code> method. 
   * @param chan The channel's name.
   * @param nick The nickname which is to add.
   */
  public void addNick(String chan, String nick) {
    int index = indexOfTab(chan);
    addNick(index, nick);
  }

// ------------------------------

  /** 
   * Adds a nickname to the nicklist of a given channel. 
   * @param index The channel's index.
   * @param nick The nickname which is to add.
   */
  public void addNick(int index, String nick) {
    if (index != -1) {
      Component component = tabs.getComponentAt(index);
      ChanPanel chanPanel = (ChanPanel)component;
      chanPanel.addNick(nick);
    }
  }

// ------------------------------

  /** 
   * Removes all nicknames from the nicklist of a given chan.<br />
   * The channel's name indicates the channel. 
   * @param chan The channel's name.
   */
  public void removeAllNicks(String chan) {
    int index = indexOfTab(chan);
    removeAllNicks(index);
  }

// ------------------------------

  /** 
   * Removes all nicknames from the nicklist of a given chan.<br />
   * The index represents the channel's position in the tabs. 
   * @param index The channel's index.
   */
  public void removeAllNicks(int index) {
    Component component = tabs.getComponentAt(index);
    if (component instanceof ChanPanel) {
      ((ChanPanel)component).removeAllNicks();
    }
  }

// ------------------------------

  /** 
   * Removes a nickname from the nicklist of a given channel. Returns the 
   * console of the removed user: 
   * <ul>
   * <li> <code>"@"</code> = Operator </li>
   * <li> <code>"+"</code> = Visible </li>
   * <li> <code>""</code> = Normal nickname </code>
   * </ul>
   * <code>null</code> is returned if no nickname was removed. <br />
   * Uses the <code>removeNick(int, String)</code>-method. 
   * @param chan The channel's name.
   * @param nick The nickname which is to remove.
   * @return An "@", "+", or "" which stands for the mode the user had.
   */
  public String removeNick(String chan, String nick) {
    int index = indexOfTab(chan);
    return removeNick(index, nick);
  }

// ------------------------------

  /** 
   * Removes a nickname from the nicklist of a given channel. Returns the 
   * console of the removed user: 
   * <ul>
   * <li> <code>"@"</code> = Operator </li>
   * <li> <code>"+"</code> = Visible </li>
   * <li> <code>""</code> = Normal nickname </code>
   * </ul>
   * <code>null</code> is returned if no nickname was removed. 
   * @param index The channel's index.
   * @param nick The nickname which is to remove.
   * @return An "@", "+", or "" which stands for the mode the user had. If no
   *         user was found, <code>null</code> is returned.
   */
  public String removeNick(int index, String nick) {
    Component component = tabs.getComponentAt(index);
    ChanPanel chanPanel = (ChanPanel)component;
    if (chanPanel.removeNick("@"+ nick))
      return "@";
    else if (chanPanel.removeNick("+"+ nick))
      return "+";
    else if (chanPanel.removeNick(nick))
      return "";
    return null;
  }

// ------------------------------

  /** 
   * Adds a banmask to the chancenter of a given channel. Uses the 
   * <code>addBan(int, String, String, long)</code>-method. 
   * @param chan The channel's name.
   * @param banmask The banmask which is to add.
   * @param nick The nickname who set the ban.
   * @param time The time in milliseconds when the ban was set.
   */
  public void addBan(String chan, String banmask, String nick, long time) {
    int index = indexOfTab(chan);
    addBan(index, banmask, nick, time);
  }

// ------------------------------

  /** 
   * Adds a banmask to the chancenter of a given channel. 
   * @param index The channel's index.
   * @param banmask The banmask which is to add.
   * @param nick The nick who set the ban.
   * @param time The time in milliseconds when the ban was set.
   */
  public void addBan(int index, String banmask, String nick, long time) {
    if (index != -1) {
      Component component = tabs.getComponentAt(index);
      ChanPanel chanPanel = (ChanPanel)component;
      chanPanel.addBan(banmask, nick, time);
    }
  }

// ------------------------------

  /** 
   * Removes all banmasks from the ArrayList for banmasks of a given chan.<br />
   * The channel's name indicates the channel. 
   * @param chan The channel's name.
   */
  public void removeAllBans(String chan) {
    int index = indexOfTab(chan);
    removeAllBans(index);
  }

// ------------------------------

  /** 
   * Removes all banmasks from the ArrayList for banmasks of a given chan.<br />
   * The index represents the channel's position in the tabs. 
   * @param index The channel's index.
   */
  public void removeAllBans(int index) {
    Component component = tabs.getComponentAt(index);
    if (component instanceof ChanPanel) {
      ((ChanPanel)component).removeAllBans();
    }
  }

// ------------------------------

  /** 
   * Removes a banmask to the chancenter of a given channel. Uses the 
   * <code>removeBan(int, String)</code> method. 
   * @param chan The channel's name.
   * @param banmask The banmask which is to remove.
   */
  public void removeBan(String chan, String banmask) {
    int index = indexOfTab(chan);
    removeBan(index, banmask);
  }

// ------------------------------

  /** 
   * Removes a banmask to the chancenter of a given channel. 
   * @param index The channel's index.
   * @param banmask The banmask which is to remove.
   */
  public void removeBan(int index, String banmask) {
    if (index != -1) {
      Component component = tabs.getComponentAt(index);
      ChanPanel chanPanel = (ChanPanel)component;
      chanPanel.removeBan(banmask);
    }
  }

// ------------------------------

  /** 
   * Updates _all_ modes with a complete string which contains all the modes 
   * for the chancenter. 
   * @param index The channel's index.
   * @param modes The new modes.
   */
  public void updateModes(int index, String modes) {
    if (index != -1) {
      Component component = tabs.getComponentAt(index);
      ChanPanel chanPanel = (ChanPanel)component;
      chanPanel.updateModes(modes);
    }
  }

// ------------------------------

  /** 
   * Updates _one_ mode of a channel. 
   * @param index The channel's index.
   * @param operator The operator (+ or -).
   * @param mode The mode (i.e. i).
   * @param arg The argument.
   */
  public void updateMode(int index, char operator, char mode, String arg) {
    if (index != -1) {
      Component component = tabs.getComponentAt(index);
      ChanPanel chanPanel = (ChanPanel)component;
      chanPanel.updateMode(operator, mode, arg);
    }
  }

// ------------------------------

  /** 
   * On a query there should be created a new tab and a new 
   * <code>QueryPanel</code>. If the <code>boolean</code> is <code>true</code>, 
   * the query is selected in the tabbar and is on top. 
   * @param nick The nickname of the query.
   * @param select If <code>true</code>, the new query is made on-top.
   * @return The new <code>QueryPanel</code>.
   */
  public QueryPanel addQuery(String nick, boolean select) {
    int index = indexOfTab(nick);
    if (index == -1) {
      QueryPanel queryPanel = new QueryPanel(this, nick);
      tabs.add(queryPanel, nick);
      if (select)
        tabs.setSelectedComponent(queryPanel);
      tabs.revalidate();
      if (!select)
        doSoundHighlight();
      return queryPanel;
    } else {
      Component component = tabs.getComponentAt(index);
      return (QueryPanel)component;
    }
  }
  
// ------------------------------

  /**
   * On a join there should be created a new tab and a new 
   * <code>ChanPanel</code>. The channel is the last tab of the channels but 
   * before the queries.
   * @param chan The channel's name.
   * @return The new <code>ChanPanel</code>.
   */
  public ChanPanel addChan(String chan) {
    int index = indexOfTab(chan);
    if (index == -1) {
      ChanPanel chanPanel = new ChanPanel(this, chan);
      tabs.add(chanPanel, chan, lastChanIndex + 1); 
      tabs.setSelectedComponent(chanPanel);
      tabs.revalidate();
      lastChanIndex++;
      return chanPanel;
    } else {
      Component component = getComponentAt(index);
      return (ChanPanel)component;
    }
  } 
  
// ------------------------------

  /** 
   * On a part there should be closed the concerning tab and chan/query. <br />
   * This method is for both, <code>ChanPanel</code>s and 
   * <code>QueryPanel</code>s. 
   * @param chan The channel's name or the query's nickname.
   * @return The new <code>JTabbedPane</code>.
   */
  public void closePanel(String chan) {
    int index = indexOfTab(chan);
     if (index != -1) {
      if (logging) { // close logging filewriter
        Component component = tabs.getComponentAt(index);
        ((AbstractPanel)component).close();
      }
      tabs.removeTabAt(index); 
      if (index <= lastChanIndex) 
        lastChanIndex--; 
    }
    setSelectedTab();
    tabs.revalidate();
  }

// ------------------------------

  /**
   * Opens the setup window.
   */
  public void openSetupDialog() {
    new SetupDialog(this);
  }

// ------------------------------

  /**
   * Opens the paste dialog.
   * @param chan The chan belonging to the paste dialog.
   */
  public void openPasteDialog(String chan) {
    new PasteDialog(this, chan);
  }

// ------------------------------

  /**
   * Opens a chan center.
   * @param chanPanel The chanpanel belonging to the chancenter.
   */
  public void openChanCenter(ChanPanel chanPanel) {
    chanPanel.openChanCenter();
  }

// ------------------------------

  /**
   * Sets the owning class of the module to <code>this</code>.
   * @param module The module.
   */
  public void setMoep(Module module) {
    module.setMoep(this);
  }

// ------------------------------

  /** 
   * Returns value of the <code>isConnected boolean</code> which indicates if 
   * the connection is established or not at the moment. 
   * @return <code>true</code> if the client is connected to an IRC server.
   */
  public boolean isConnected() {
    return isConnected;
  }

// ------------------------------

  /**
   * Returns the <code>IRCConnection</code>.
   * @return The <code>IRCConnection</code>.
   */
  public IRCConnection getIRCConnection() {
    return conn;
  }

// ------------------------------

  /**
   * Returns <code>true</code> if SSL is used.
   * @return <code>true</code> if SSL is used.
   */
  public boolean getUseSSL() {
    return useSSL;
  }
   
// ------------------------------

  /**
   * Sets wether SSL is used.
   * @param b <code>true</code> if SSL should be used.
   */
  public void setUseSSL(boolean b) {
    useSSL = b;
  }
   
// ------------------------------

  /**
   * Returns <code>true</code> if SSL certs are accepted automatically.
   * @return <code>true</code> if SSL certs are accepted automatically.
   */
  public boolean getAutoAcceptSSLCerts() {
    return autoAcceptSSLCerts;
  }
   
// ------------------------------

  /**
   * Sets wether SSL certs are accepted automatically.
   * @param b <code>true</code> if SSL certs should be accepted automatically.
   */
  public void setAutoAcceptSSLCerts(boolean b) {
    autoAcceptSSLCerts = b;
  }
   
// ------------------------------

  /**
   * Returns the IRC hostname.
   * @return The IRC hostname.
   */
  public String getHost() {
    return host;
  }

// ------------------------------

  /**
   * Sets the IRC hostname.
   * @param address The IRC server's address.
   */
  public void setHost(String address) {
    host = address;
  }

// ------------------------------

  /**
   * Returns the min. port.
   * @return The min. port.
   */
  public int getMinPort() {
    return portMin;
  }

// ------------------------------

  /**
   * Sets the min. port.
   * @param port The new min. port.
   */
  public void setMinPort(int port) {
    portMin = port;
  }

// ------------------------------

  /**
   * Returns the max. port.
   * @return The max. port.
   */
  public int getMaxPort() {
    return portMax;
  }

// ------------------------------

  /**
   * Sets the max. port.
   * @param port The new max. port.
   */
  public void setMaxPort(int port) {
    portMax = port;
  }

// ------------------------------

  /**
   * Returns the IRC password.
   * @return The IRC password.
   */
  public String getPassword() {
    return password;
  }

// ------------------------------

  /**
   * Sets the IRC password.
   * @param pswd The new password.
   */
  public void setPassword(String pswd) {
    password = pswd;
  }

// ------------------------------

  /**
   * Returns the nickname.
   * @return The nickname.
   */
  public String getNick() {
    return nickname;
  }

// ------------------------------

  /** 
   * Just changes the class-var <code>nick</code> and the 
   * <code>IRCConnection</code>'s nicknam variable. Used by the 
   * <code>IRCEventListener</code> to change this class's class-var.
   * @param nick The new nickname.
   */
  public void setNick(String nick) {
    nickname = nick;
  }

// ------------------------------

  /**
   * Returns the realname.
   * @return The realname.
   */
  public String getRealname() {
    return realname;
  }

// ------------------------------

  /**
   * Sets the realname.
   * @param name The realname.
   */
  public void setRealname(String name) {
    realname = name;
  }

// ------------------------------

  /**
   * Returns the username.
   * @return The username.
   */
  public String getUsername() {
    return username;
  }

// ------------------------------

  /**
   * Sets the username.
   * @param user The username.
   */
  public void setUsername(String user) {
    username = user;
  }

// ------------------------------

  /**
   * Returns the quitmsg.
   * @return The quitmsg.
   */
  public String getQuitMsg() {
    return quitMsg;
  }

// ------------------------------

  /**
   * Sets the quit-message.
   * @param msg The new quit-msg.
   */
  public void setQuitMsg(String msg) {
    quitMsg = msg;
  }

// ------------------------------

  /**
   * Returns the highlight color.
   * @return The highlight color.
   */
  public Color getHighlightColor() {
    return highlightColor;
  }

// ------------------------------

  /**
   * Sets the highlight color.
   * @param c The color.
   */
  public void setHighlightColor(Color c) {
    highlightColor = c;
  }

// ------------------------------

  /**
   * Returns <code>true</code> if query sound highlight is on.
   * @return <code>true</code> if query sound highlight is on.
   */
  public boolean getQuerySoundHighlight() {
    return querySoundHighlight;
  }

// ------------------------------

  /**
   * Sets query sound highlight.
   * @param b <code>true</code> if sound highlight should be on.
   */
  public void setQuerySoundHighlight(boolean b) {
    querySoundHighlight = b;
  }

// ------------------------------

  /**
   * Returns <code>true</code> if nick sound highlight is on.
   * @return <code>true</code> if nick sound highlight is on.
   */
  public boolean getNickSoundHighlight() {
    return nickSoundHighlight;
  }

// ------------------------------

  /**
   * Sets nick sound highlight.
   * @param b <code>true</code> if sound highlight should be on.
   */
  public void setNickSoundHighlight(boolean b) {
    nickSoundHighlight = b;
  }

// ------------------------------

  /** 
   * Returns wether auto-rejoin is on or not.
   * @return wether auto-rejoin is on or not.
   */
  public boolean getAutoRejoin() {
    return autoRejoin;
  }

// ------------------------------

  /**
   * Sets auto-rejoin.
   * @param b <code>true</code> if the client should rejoin after a kick.
   */
  public void setAutoRejoin(boolean b) {
    autoRejoin = b;
  }

// ------------------------------

  /** 
   * Returns wether auto-connect is on or not.
   * @return wether auto-connect is on or not.
   */
  public boolean getAutoConnect() {
    return autoConnect;
  }

// ------------------------------

  /**
   * Sets auto-connect.
   * @param b <code>true</code> if the client should connect automatically.
   */
  public void setAutoConnect(boolean b) {
    autoConnect = b;
  }

// ------------------------------

  /** 
   * Returns wether modules are to be loaded on or not.
   * @return wether modules are to be loaded or not.
   */
  public boolean getLoadModules() {
    return loadModules;
  }

// ------------------------------

  /**
   * Sets wether modules are to be loaded.
   * @param b <code>true</code> if the client should load the modules.
   */
  public void setLoadModules(boolean b) {
    loadModules = b;
  }

// ------------------------------

  /** 
   * Returns wether logging is on or not.
   * @return wether logging is on or not.
   */
  public boolean getLogging() {
    return logging;
  }

// ------------------------------

  /**
   * Sets logging.
   * @param b <code>true</code> if the client should log.
   */
  public void setLogging(boolean b) {
    logging = b;
  }

// ------------------------------

  /** 
   * Returns the logging path.
   * @return The logging path.
   */
  public String getLoggingPath() {
    return loggingPath;
  }

// ------------------------------

  /**
   * Sets logging path.
   * @param path The logging path.
   */
  public void setLoggingPath(String path) {
    loggingPath = path;
  }

// ------------------------------

  /** 
   * Returns the perform.
   * @return The perform.
   */
  public String getPerform() {
    return perform;
  }

// ------------------------------

  /**
   * Sets the perform.
   * @param p The new perform.
   */
  public void setPerform(String p) {
    perform = p;
  }

// ------------------------------

  /** 
   * Returns the browser path.
   * @return The logging path.
   */
  public String getBrowserPath() {
    return browserPath;
  }

// ------------------------------

  /**
   * Sets the browser path.
   * @param path The browser path.
   */
  public void setBrowserPath(String path) {
    browserPath = path;
  }

// ------------------------------

  /** 
   * Returns wether the channel-text is cutted or not.
   * @return Wether the channel-text is cutted or not.
   */
  public boolean getCutChannelText() {
    return cutChannelText;
  }

// ------------------------------

  /**
   * Sets wether the channel-text is cutted or not.
   * @param b <code>true</code> to enable channel-text-cutting.
   */
  public void setCutChannelText(boolean b) {
    cutChannelText = b;
  }

// ------------------------------

  /** 
   * Returns the maximum of channel-text until cutting.
   * @return The maximum of channel-text until cutting.
   */
  public int getMaxTextLength() {
    return maxTextLength;
  }

// ------------------------------

  /**
   * Sets the maximum of channel-text until cutting.
   * @param limit The new limit.
   */
  public void setMaxTextLength(int limit) {
    maxTextLength = limit;
  }

// ------------------------------

  /**
   * Returns the channel font.
   * @return The channel font.
   */
  public Font getChanFont() {
    return chanFont;
  }

// ------------------------------

  /**
   * Sets the channel font.
   * @param f The new font.
   */
  public void setChanFont(Font f) {
    chanFont = f;
    if (tabs != null)
      for (int i = 0; i < tabs.getTabCount(); i++)
        if (i != Util.CONSOLEWINDOWINDEX)
          ((AbstractPanel)getComponentAt(i)).setTextFont(f);
  }

// ------------------------------

  /**
   * Returns the console font.
   * @return The console font.
   */
  public Font getConsoleFont() {
    return consoleFont;
  }

// ------------------------------

  /**
   * Sets the console font.
   * @param f The new font.
   */
  public void setConsoleFont(Font f) {
    consoleFont = f;
    if (tabs != null)
      ((AbstractPanel)getComponentAt(Util.CONSOLEWINDOWINDEX)).setTextFont(f);
  }

// ------------------------------

  /**
   * Returns the background color.
   * @return The background color.
   */
  public Color getBgColor() {
    return bgColor;
  }

// ------------------------------

  /**
   * Sets the background color.
   * @param c The new color.
   */
  public void setBgColor(Color c) {
    bgColor = c;
  }

// ------------------------------

  /**
   * Returns the own color.
   * @return The own color.
   */
  public Color getOwnColor() {
    return ownColor;
  }

// ------------------------------

  /**
   * Sets the own color.
   * @param c The new color.
   */
  public void setOwnColor(Color c) {
    ownColor = c;
  }

// ------------------------------

  /**
   * Returns the server color.
   * @return The server color.
   */
  public Color getServerColor() {
    return serverColor;
  }

// ------------------------------

  /**
   * Sets the server color.
   * @param c The new color.
   */
  public void setServerColor(Color c) {
    serverColor = c;
  }

// ------------------------------

  /**
   * Returns the other's color.
   * @return The other's color.
   */
  public Color getOtherColor() {
    return otherColor;
  }

// ------------------------------

  /**
   * Sets the other's color.
   * @param c The new color.
   */
  public void setOtherColor(Color c) {
    otherColor = c;
  }

// ------------------------------

}