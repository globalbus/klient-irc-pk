/*
 * Copyright (C) 2002, 2003 Christoph Schwering
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License. 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details. 
 * You should have received a copy of the GNU General Public License along with 
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
 * Place, Suite 330, Boston, MA 02111-1307, USA. 
 */

package org.schwering.irc.moep;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/** 
 * The setup window. 
 */
public class SetupDialog extends JDialog {

  /**
   * The owning class.
   */
  private MoepIRC moep;

  /** 
   * Represents the bottom OK and Cancel buttons. 
   */    
  private JPanel bottom;

  /** 
   * The host textfield. 
   */
  private JTextField tfHost;

  /**
   * The use-ssl checkbox.
   */
  private JCheckBox cbUseSSL;

  /** 
   * The min port textfield. 
   */
  private JTextField tfPortMin;

  /** 
   * The max port textfield. 
   */
  private JTextField tfPortMax;

  /** 
   * The textfield for the pass. 
   */
  private JTextField tfPass;

  /** 
   * The nickname textfield. 
   */
  private JTextField tfNick;

  /** 
   * The realname textfield. 
   */
  private JTextField tfRealname;

  /** 
   * The username textfield. 
   */
  private JTextField tfUsername;

  /**
   * The quit-message textfield. 
   */
  private JTextField tfQuitMsg;

  /** 
   * The sound-highlight checkbox. 
   */
  private JCheckBox cbQuerySoundHighlight;

  /**
   * The nick-highlight checkbox. 
   */
  private JCheckBox cbNickSoundHighlight;

  /**
   * The sound-highlight checkbox. 
   */
  private JCheckBox cbRejoin;

  /**
   * The sound-highlight checkbox. 
   */
  private JCheckBox cbAutoConnect;

  /**
   * The checkbox for the channel-text-cut-function. 
   */
  private JCheckBox cbCutChannelText;

  /**
   * The logging checkbox. 
   */
  private JCheckBox cbLogging;

  /**
   * The logging checkbox. 
   */
  private JCheckBox cbLoadModules;

  /**
   * The accept-ssl-certificates checkbox.
   */
  private JCheckBox cbAutoAcceptSSLCerts;

  /**
   * The directory for the logfiles textfield. 
   */
  private JTextField tfLoggingDir;

  /**
   * The textfield for the path of the browser. 
   */
  private JTextField tfBrowserPath;

  /**
   * The number of max. characters in a channel-window textfield. 
   */
  private JTextField tfMaxTextLength;

  /**
   * The list contains all fonts with the selected console-font. 
   */
  private JComboBox cboxConsoleFont;

  /**
   * The list contains all fonts with the selected chan-font. 
   */
  private JComboBox cboxChanFont;

  /**
   * The textfield for the size of the font in the channels. 
   */
  private JTextField tfFontSize;

  /**
   * The textarea for the perform. 
   */
  private JTextArea taPerform; 

// ------------------------------

  /** 
   * The constructor creates the GUI of the setup-window. 
   * @param owner The owning <code>MoepIRC</code> instance.
   */
  public SetupDialog(MoepIRC owner) {
    super(owner, "Setup", true);
    this.moep = owner;
    this.bottom = makeBottomPanel();
    JTabbedPane tabs = new JTabbedPane();
    tabs.add("IRC", makeConnectionPanel());
    tabs.add("Colors", makeColorsPanel());
    tabs.add("General", makeGeneralPanel());
    tabs.add("Perform", makePerformPanel());

    getContentPane().add("Center", tabs);
    setSize(270,310); 
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension mySize = getSize();
    if (mySize.height > screenSize.height) 
      mySize.height = screenSize.height;
    if (mySize.width > screenSize.width) 
      mySize.width = screenSize.width;
    int x = (screenSize.width - mySize.width)/2;
    int y = (screenSize.height - mySize.height)/2;
    setLocation(x, y); 
    setVisible(true);
  }

// ------------------------------

  /** 
   * Returns the bottom panel with OK and Cancel button. 
   * @return The bottom panel with OK and Cancel button. 
   */
  private JPanel makeBottomPanel() {
    bottom = new JPanel();
    JButton okay = new JButton("OK");
    JButton cancel = new JButton("Cancel");
    okay.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) { 
        Font consoleFont = new Font(((String)cboxConsoleFont.getSelectedItem()), 
            Font.PLAIN, new Integer(tfFontSize.getText().trim()).intValue());
        Font chanFont = new Font(((String)cboxChanFont.getSelectedItem()), 
            Font.PLAIN, new Integer(tfFontSize.getText().trim()).intValue());
        moep.setHost(tfHost.getText());
        moep.setUseSSL(cbUseSSL.isSelected());
        moep.setMinPort(new Integer(tfPortMin.getText().trim()).intValue());
        moep.setMaxPort(new Integer(tfPortMax.getText().trim()).intValue());
        moep.setPassword(tfPass.getText()); 
        moep.setRealname(tfRealname.getText());
        moep.setUsername(tfUsername.getText());
        moep.setNick(tfNick.getText());
        moep.setQuitMsg(tfQuitMsg.getText());
        moep.setConsoleFont(consoleFont);
        moep.setChanFont(chanFont);
        moep.setMaxTextLength(Integer.parseInt(tfMaxTextLength.getText()));
        moep.setCutChannelText(new Boolean(
            cbCutChannelText.isSelected()).booleanValue());
        moep.setAutoRejoin(new Boolean(cbRejoin.isSelected()).booleanValue());
        moep.setQuerySoundHighlight(new Boolean(
            cbQuerySoundHighlight.isSelected()).booleanValue());
        moep.setNickSoundHighlight(new Boolean(
            cbNickSoundHighlight.isSelected()).booleanValue());
        moep.setLogging(new Boolean(cbLogging.isSelected()).booleanValue());
        moep.setLoadModules(new Boolean(
            cbLoadModules.isSelected()).booleanValue());
        moep.setAutoConnect(new Boolean(
            cbAutoConnect.isSelected()).booleanValue());
        moep.setAutoAcceptSSLCerts(new Boolean(
            cbAutoAcceptSSLCerts.isSelected()).booleanValue());
        moep.setLoggingPath(tfLoggingDir.getText());
        moep.setPerform(taPerform.getText());
        moep.setBrowserPath(tfBrowserPath.getText());
        if (moep.isConnected()) 
          moep.getIRCConnection().doNick(moep.getNick()); // request new nick 
        dispose();
      }
    } );
    cancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) { 
        dispose();
      }
    } );
    bottom.add(okay);
    bottom.add(cancel);
    return bottom;
  }

// ------------------------------

  /** 
   * Returns a complete panel for the connection setup. 
   * @return A complete panel for the connection setup. 
   */
  private JPanel makeConnectionPanel() {
    int fieldLength = 14; 
    tfHost = new JTextField(moep.getHost(), fieldLength);
    cbUseSSL = new JCheckBox();
    cbUseSSL.setSelected(moep.getUseSSL());
    tfPortMin = new JTextField(String.valueOf(moep.getMinPort()), 5);
    tfPortMax = new JTextField(String.valueOf(moep.getMaxPort()), 5);
    tfPass = new JTextField(moep.getPassword(), fieldLength);
    tfNick = new JTextField(moep.getNick(), fieldLength);
    tfRealname = new JTextField(moep.getRealname(), fieldLength);
    tfUsername = new JTextField(moep.getUsername(), fieldLength);
    tfQuitMsg = new JTextField(moep.getQuitMsg(), fieldLength);

    JPanel left = new JPanel(new GridLayout(9, 1));
    addComponent(left, new JLabel(" Hostname: "));
    addComponent(left, new JLabel(" Portrange: "));
    addComponent(left, new JLabel(" Use SSL: "));
    addComponent(left, new JLabel(" Password: "));
    addComponent(left, new JLabel(" Nickname: "));
    addComponent(left, new JLabel(" Realname: "));
    addComponent(left, new JLabel(" Username: "));
    addComponent(left, new JLabel(" Quit Msg: "));

    JPanel right = new JPanel(new GridLayout(9, 1));
    addComponent(right, tfHost);
    JPanel tmp = new JPanel(new BorderLayout());
    tmp.add(tfPortMin, BorderLayout.WEST);
    tmp.add(new JLabel("  to  "), BorderLayout.CENTER);
    tmp.add(tfPortMax, BorderLayout.EAST);
    addComponent(right, tmp);
    addComponent(right, cbUseSSL);
    addComponent(right, tfPass);
    addComponent(right, tfNick);
    addComponent(right, tfRealname);
    addComponent(right, tfUsername);
    addComponent(right, tfQuitMsg);

    JPanel c = new JPanel(new BorderLayout()); 
    c.add(left, BorderLayout.WEST);
    c.add(right, BorderLayout.CENTER);

    JPanel p = new JPanel();
    p.setLayout(new BorderLayout());
    p.add(c, BorderLayout.CENTER);
    p.add(makeBottomPanel(), BorderLayout.SOUTH);
    return p;
  }

// ------------------------------

  /** 
   * Returns a complete panel for the color setup. 
   * @return A complete panel for the color setup. 
   */
  private JPanel makeColorsPanel() {
    Dimension colorButtonSize = new Dimension(90, 22);
    JButton bChooseBgColor = new JButton("Choose...");
    bChooseBgColor.setPreferredSize(colorButtonSize);
    bChooseBgColor.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(moep, "Choose background color..", 
            moep.getBgColor());
        if (c != null)
          moep.setBgColor(c);
      }
    } );
    JButton bChooseOwnColor = new JButton("Choose...");
    bChooseOwnColor.setPreferredSize(colorButtonSize);
    bChooseOwnColor.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(moep, "Choose own color..", 
            moep.getOwnColor());
        if (c != null)
          moep.setOwnColor(c);
      }
    } );
    JButton bChooseOtherColor = new JButton("Choose...");
    bChooseOtherColor.setPreferredSize(colorButtonSize);
    bChooseOtherColor.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(moep, "Choos other colore.", 
            moep.getOtherColor());
        if (c != null)
          moep.setOtherColor(c);
      }
    } );
    JButton bChooseServerColor = new JButton("Choose...");
    bChooseServerColor.setPreferredSize(colorButtonSize);
    bChooseServerColor.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(moep, "Choose server color", 
            moep.getServerColor());
        if (c != null)
          moep.setServerColor(c);
      }
    } );
    cboxConsoleFont = new JComboBox();
    cboxConsoleFont.setPreferredSize(new Dimension(152,22));
    cboxChanFont = new JComboBox();
    cboxChanFont.setPreferredSize(new Dimension(152,22));
    tfFontSize = new JTextField(String.valueOf(moep.getChanFont().getSize()), 
        3);

    // fill the font lists
    GraphicsEnvironment g = GraphicsEnvironment.getLocalGraphicsEnvironment();
    String[] fonts = g.getAvailableFontFamilyNames();
    for (int i = 0; i < fonts.length; i++) {
      cboxConsoleFont.addItem(fonts[i]);
      cboxChanFont.addItem(fonts[i]);
    }
    cboxConsoleFont.setSelectedItem(moep.getConsoleFont().getName());
    cboxChanFont.setSelectedItem(moep.getChanFont().getName());

    JPanel left = new JPanel(new GridLayout(7, 1));
    addComponent(left, new JLabel(" Console Font: "));
    addComponent(left, new JLabel(" Channel Font: "));
    addComponent(left, new JLabel(" Font Size: "));
    addComponent(left, new JLabel(" Background: "));
    addComponent(left, new JLabel(" Own Text: "));
    addComponent(left, new JLabel(" Other Text: "));
    addComponent(left, new JLabel(" Neutral Text: "));

    JPanel right = new JPanel(new GridLayout(7, 1));
    addComponent(right, cboxConsoleFont);
    addComponent(right, cboxChanFont);
    addComponent(right, tfFontSize);
    addComponent(right, bChooseBgColor);
    addComponent(right, bChooseOwnColor);
    addComponent(right, bChooseOtherColor);
    addComponent(right, bChooseServerColor);

    JPanel c = new JPanel(new BorderLayout()); 
    c.add(left, BorderLayout.WEST);
    c.add(right, BorderLayout.CENTER);

    JPanel p = new JPanel();
    p.setLayout(new BorderLayout());
    p.add(c, BorderLayout.CENTER);
    p.add(makeBottomPanel(), BorderLayout.SOUTH);
    return p;
  }

// ------------------------------

  /** 
   * Returns a complete panel for the general setup. 
   * @return A complete panel for the general setup. 
   */
  private JPanel makeGeneralPanel() {
    int fieldLength = 14; 
    cbQuerySoundHighlight = new JCheckBox(); 
    cbQuerySoundHighlight.setSelected(moep.getQuerySoundHighlight());
    cbNickSoundHighlight = new JCheckBox();
    cbNickSoundHighlight.setSelected(moep.getNickSoundHighlight());
    cbRejoin = new JCheckBox();
    cbRejoin.setSelected(moep.getAutoRejoin());
    cbAutoConnect = new JCheckBox();
    cbAutoConnect.setSelected(moep.getAutoConnect());
    cbLogging = new JCheckBox();
    cbLogging.setSelected(moep.getLogging());
    cbLoadModules = new JCheckBox();
    cbLoadModules.setSelected(moep.getLoadModules());
    cbCutChannelText = new JCheckBox();
    cbCutChannelText.setSelected(moep.getCutChannelText());
    cbAutoAcceptSSLCerts = new JCheckBox();
    cbAutoAcceptSSLCerts.setSelected(moep.getAutoAcceptSSLCerts());
    tfLoggingDir = new JTextField(moep.getLoggingPath(), fieldLength - 2);
    tfBrowserPath = new JTextField(moep.getBrowserPath(), fieldLength - 4);
    tfMaxTextLength = new JTextField(String.valueOf(moep.getMaxTextLength()), 
        4);

    JPanel left = new JPanel(new GridLayout(9, 1));
    addComponent(left, cbAutoConnect);
    addComponent(left, cbRejoin);
    addComponent(left, cbQuerySoundHighlight);
    addComponent(left, cbNickSoundHighlight);
    addComponent(left, cbCutChannelText);
    addComponent(left, cbLogging);
    addComponent(left, cbLoadModules);
    addComponent(left, cbAutoAcceptSSLCerts);
    addComponent(left, new JLabel(""));

    JPanel right = new JPanel(new GridLayout(9, 1));
    addComponent(right, new JLabel("Connect on Startup "));
    addComponent(right, new JLabel("Rejoin on Kick "));
    addComponent(right, new JLabel("Beep on Query "));
    addComponent(right, new JLabel("Beep on Nickmention "));
    JPanel tmp;
    tmp = new JPanel(new BorderLayout());
    tmp.add(new JLabel("Halve Text after "), BorderLayout.WEST);
    tmp.add(tfMaxTextLength, BorderLayout.CENTER);
    tmp.add(new JLabel("Chars "), BorderLayout.EAST);
    addComponent(right, tmp);
    tmp = new JPanel(new BorderLayout());
    tmp.add(new JLabel("Logging to "), BorderLayout.WEST);
    tmp.add(tfLoggingDir, BorderLayout.CENTER);
    addComponent(right, tmp);
    addComponent(right, new JLabel("Enable Modules "));
    addComponent(right, new JLabel("Accept SSL X509 Certificates"));
    tmp = new JPanel(new BorderLayout());
    tmp.add(new JLabel("Browser Path: "), BorderLayout.WEST);
    tmp.add(tfBrowserPath);
    addComponent(right, tmp);

    JPanel c = new JPanel(new BorderLayout()); 
    c.add(left, BorderLayout.WEST);
    c.add(right, BorderLayout.CENTER);

    JPanel p = new JPanel();
    p.setLayout(new BorderLayout());
    p.add(c, BorderLayout.CENTER);
    p.add(makeBottomPanel(), BorderLayout.SOUTH);
    return p;
  }

// ------------------------------

  /** 
   * Returns a complete panel for the perform setup.<br />
   * It has a <code>JTextArea</code> in a <code>JScrollPane</code> centered. 
   * @return A complete panel for the perform setup.
   */
  private JPanel makePerformPanel() {
    taPerform = new JTextArea(moep.getPerform());
    JScrollPane scrollpane = new JScrollPane(taPerform, 
        JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
        JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    JPanel p = new JPanel();
    p.setLayout(new BorderLayout());
    p.add(scrollpane, BorderLayout.CENTER);
    p.add(makeBottomPanel(), BorderLayout.SOUTH);
    return p;
  }

// ------------------------------

  /**
   * Adds a <code>JComponent</code> in a new <code>FlowLayout</code> with
   * left alignment to a <code>JPanel</code>.
   * @param p The <code>JPanel</code> to which we want to add the component.
   * @param c The <code>JComponent</code> which is to add.
   */
  private void addComponent(JPanel p, JComponent c) {
    JPanel flow = new JPanel(new FlowLayout(FlowLayout.LEFT));
    flow.add(c);
    p.add(flow);
  }

}