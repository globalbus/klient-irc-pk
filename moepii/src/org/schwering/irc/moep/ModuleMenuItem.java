/*
 * Copyright (C) 2002, 2003 Christoph Schwering
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License. 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details. 
 * You should have received a copy of the GNU General Public License along with 
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
 * Place, Suite 330, Boston, MA 02111-1307, USA. 
 */

package org.schwering.irc.moep;

import javax.swing.JMenuItem;

/** 
 * This class represents a indexed <code>JMenuItem</code>. 
 * It is used by the menu of the modules. We need the index of the modules to 
 * know <i>which</i> module was started / stopped -- therefore we need the 
 * index. 
 * @author Christoph Schwering &lt;ch@schwering.org&gt;
 */
public class ModuleMenuItem extends JMenuItem {

  /** 
   * The given index of the menuitem. 
   */
  private int index = -1;

// ------------------------------

  /** 
   * Creates a new <code>JMenuItem</code> and sets the given index. 
   * @param title The menu's title.
   * @param index The menu's index.
   */
  public ModuleMenuItem(String title, int index) {
    super(title);
    this.index = index;
  }

// ------------------------------

  /** 
   * Returns the given index. 
   */
  public int getIndex() {
    return index;
  }
      
}
