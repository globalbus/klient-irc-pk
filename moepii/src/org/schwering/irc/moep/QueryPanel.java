/*
 * Copyright (C) 2002, 2003 Christoph Schwering
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License. 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details. 
 * You should have received a copy of the GNU General Public License along with 
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
 * Place, Suite 330, Boston, MA 02111-1307, USA. 
 */

package org.schwering.irc.moep;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

/** 
 * This is the <code>IRCPanel</code> for all queries. 
 */
public class QueryPanel extends AbstractPanel implements ActionListener {

  /** 
   * The constructor calls the <code>makePanel</code>-method. 
   * @param owner The owning class.
   * @param name The query's name.
   */
  public QueryPanel(MoepIRC owner, String name) {
    super(owner, name);
    makePanel();
  }
    
// ------------------------------

  /** 
   * Makes the panel with textarea, inputline and nicklist. <br />
   * It also creates the splitpane and the scrollbars. 
   */
  private void makePanel() {
    text.setFont(moep.getChanFont());
    input.setFont(moep.getChanFont());
    input.addActionListener(this);
    topicfield.setEditable(false);
    part.setForeground(Color.blue);
    part.addActionListener(this);
      
    add(scrollpane, BorderLayout.CENTER); 

    if (moep.getLogging()) {
      File f = new File(moep.getLoggingPath() +"/"+ name +".log");
      try {
        f.createNewFile(); 
        logger = new FileWriter(f, true); 
      } catch (Exception exc) {
        exc.printStackTrace();
      }
    }
  }
    
// ------------------------------

  /** 
   * Listens for actions of the buttons and the textarea.<br />
   * Empty lines are ignored, normal lines are sent as <code>PRIVMSG</code>
   * to the recipient and lines led by a <code>/</code> are interpreted as
   * commands and given to the <code>parseCmd</code> method.
   * @param e The <code>ActionEvent</code>.
   */
  public void actionPerformed(ActionEvent e) {
    Object source = e.getSource();
    if (source.equals(input)) {
      String line = input.getText().trim();
      if (line.length() > 0) { 
        if (line.charAt(0) == '/') { 
          moep.parseCmd(line);
          resetLine();
          addCmdToHistory(line); // save command
        } else { 
          conn.doPrivmsg(name,line);
          updateText(line, moep.getOwnColor(), true);
          resetLine();
          addCmdToHistory(line); // save command
        }
      }
    } else if (source.equals(part) && moep.isConnected()) {
      moep.closePanel(name);
    }
  }
    
}