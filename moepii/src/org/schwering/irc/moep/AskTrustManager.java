/*
 * Copyright (C) 2002, 2003 Christoph Schwering
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License. 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details. 
 * You should have received a copy of the GNU General Public License along with 
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
 * Place, Suite 330, Boston, MA 02111-1307, USA. 
 */

package org.schwering.irc.moep;

import javax.security.cert.X509Certificate;
import javax.security.cert.CertificateException;
import org.schwering.irc.lib.SSLDefaultTrustManager;

/**
 * The trustmanager for X509 certificates asks wether the user accepts
 * it or not.
 * @author Christoph Schwering &lt;ch@schwering.org&gt;
 */
public class AskTrustManager extends SSLDefaultTrustManager {

  /**
   * Invoked when the client should check wether he trusts the server or not.
   * This method trusts the server. But this method can be overriden and then
   * ask the user wether he truts the client or not.
   * @param chain The peer certificate chain.
   * @throws CertificateException If the certificate chain is not trusted by 
   *         this <code>TrustManager</code>.
   */
  public boolean isServerTrusted(X509Certificate chain[]) {
    boolean accept = false;
    for (int i = 0; !accept && i < chain.length; i++)
      accept = Util.confirmDialog("Do you want to accept this server's X509 "+ 
          "certificate?", "SSL X509 Certificate");
    return accept;
  }

}