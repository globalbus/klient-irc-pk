/*
 * Copyright (C) 2002, 2003 Christoph Schwering
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License. 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details. 
 * You should have received a copy of the GNU General Public License along with 
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
 * Place, Suite 330, Boston, MA 02111-1307, USA. 
 */

package org.schwering.irc.moep;

import java.awt.*;
import java.awt.event.*;

/** 
 * This inner-class is the console which shows the server-messages. 
 * @author Christoph Schwering &lt;ch@schwering.org&gt;
 */
public class ConsolePanel extends AbstractPanel implements ActionListener {

  /** 
   * The constructor calls the <code>makePanel</code>-method. 
   * @param owner The owning class.
   */
  public ConsolePanel(MoepIRC owner) {
    super(owner, Util.CONSOLEWINDOWTITLE);
    makePanel();
  }
    
// ------------------------------

  /** 
   * Makes the panel with textarea, inputline and nicklist. <br />
   * It also creates the splitpane and the scrollbars. 
   */
  private void makePanel() {
    text.setFont(moep.getConsoleFont());
    input.addActionListener(this);
    input.setFont(moep.getConsoleFont());
    topicfield.setEditable(false);
    part.setEnabled(false);
    add(scrollpane, BorderLayout.CENTER); // add scrollpane
  }
    
// ------------------------------

  /** 
   * Listens for actions of the buttons and the textarea. 
   * All input are just given to the <code>parseCmd</code> method.
   * @param e The <code>ActionEvent</code>.
   */
  public void actionPerformed(ActionEvent e) {
    Object source = e.getSource();
    if (source.equals(input)) {
      String line = input.getText().trim();
      if (line.length() > 0) {
        moep.parseCmd(line);
        resetLine();
        addCmdToHistory(line);
      }
    }
  }
    
}
