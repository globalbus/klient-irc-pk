/*
 * Copyright (C) 2002, 2003 Christoph Schwering
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License. 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details. 
 * You should have received a copy of the GNU General Public License along with 
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
 * Place, Suite 330, Boston, MA 02111-1307, USA. 
 */

package org.schwering.irc.moep;

import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;

/**
 * This class contains some utilities for the moepII IRC client.
 * @author Christoph Schwering &lt;ch@schwering.org&gt;
 */
public class Util  {

  /** 
   * The dateformat used for the timestamps. 
   */
  public final static SimpleDateFormat TIMESTAMP = 
      new SimpleDateFormat("HH:mm:ss");

  /**
   * The dateformat used for the timestamps which includes the year, month 
   * and day.
   */
  public final static SimpleDateFormat BIGTIMESTAMP = 
      new SimpleDateFormat("HH:mm:ss yyyy-MM-dd"); 
  
  /** 
   * The Console-Window's title. 
   */
  public static final String CONSOLEWINDOWTITLE = "Console ";

  /** 
   * The <code>int</code> of the Console-Window in the tabs. 
   */
  public static final int CONSOLEWINDOWINDEX = 0; 

// ------------------------------

  /**
   * Disallow any instances.
   */
  private Util() {
    // nothing
  }

// ------------------------------

  /**
   * Returns <code>true</code> if the "Yes" button is clicked in a "Yes/No"
   * popup.
   * @return <code>true</code> for yes.
   */
  public static boolean confirmDialog(String msg, String title) {
    return (JOptionPane.showConfirmDialog(null, msg, title, 
        JOptionPane.YES_NO_OPTION) == 0);
  }

// ------------------------------

  /**
   * Formats an array in a new string. All items are separated with 
   * <code>sep</code>.<br />
   * For example: If you have a <code>String[] arr = String[] { "elem1", 
   * "elem2", "elem3" }</code> and call the method <code>formatArray(arr, 
   * ", ")</code>, the string <code>elem1, elem2, elem3</code> is returned.
   * @param arr The array whose elements are to form into the string.
   * @param sep The string which is to be used as seperator.
   * @return A string with all elements of the array seperated by 
   *         <code>sep</code>.
   */
  public static String formatArray(String[] arr, char sep) {
    if (arr.length == 0)
      return "";
    StringBuffer sb = new StringBuffer();
    int sepcount = arr.length - 1;
    for (int i = 0; i < sepcount; i++)
      sb.append(arr[i] + sep);
    sb.append(arr[sepcount]);
    return sb.toString();
  }


}