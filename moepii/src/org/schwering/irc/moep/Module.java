/*
 * Copyright (C) 2002, 2003 Christoph Schwering
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License. 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details. 
 * You should have received a copy of the GNU General Public License along with 
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple 
 * Place, Suite 330, Boston, MA 02111-1307, USA. 
 */

package org.schwering.irc.moep;

/** 
 * <p>A module for moepII must extend <code>Module</code>.</p>
 * <p>The client automatically executes 
 * <code>setModuleListener(ModuleListener)</code> so that the module can send 
 * something to IRC via <code>irc.sendLine(String line)</code>. </p>
 * <p>The extending module must define a <code>init()</code> method which 
 * should be used instead of the constructor for the action of the module.</p>
 * @author Christoph Schwering &lt;ch@schwering.org&gt;
 * @version 0.4
 */
public abstract class Module implements Runnable {

  /**
   * The owning class. This class-var gives access to the whole moepII client.
   * For example, you can add listeners to the <code>IRCConnection</code> using
   * <code>moep.getIRCConnection().addIRCEventListener(new Listener())</code>.
   */
  protected MoepIRC moep;

// ------------------------------

  /** 
   * The constructor starts the <code>Thread</code>. 
   */
  public Module() {
  }

// ------------------------------

  /** 
   * Sets the owning class.
   * @param owner The owning <code>MoepIRC</code>.
   */
  public final void setMoep(MoepIRC owner) {
    moep = owner;
    Thread t = new Thread(this);
    t.setDaemon(true);
    t.start();
  }

// ------------------------------

  /** 
   * Just calls the <code>init</code> method in a new <code>Thread</code>. 
   */
  public final void run() {
    init();
  }

// ------------------------------
  
  /** 
   * This method must be overridden by extending modules and here the 
   * module's first action should take place. 
   */
  protected abstract void init();

// ------------------------------

  /** 
   * Checks if a class defines a <code>halt()</code> method. This method 
   * is used by <code>MoepIRC</code> to check if a module is haltable. 
   * @param cls The class which is to check.
   * @return <code>true</code> if the class defines a <code>halt</code> method.
   */
  protected static final boolean isHaltable(Class cls) {
    try {
      cls.getDeclaredMethod("halt", new Class[0]);
      return true;
    } catch (Exception exc) {
      return false;
    }
  }

// ------------------------------

  /** 
   * Checks wether a class defines a <code>AUTOSTART</code> field of ANY TYPE
   * with ANY VALUE (it does NOT matter! A <code>boolean</code> with value 
   * <code>false</code> also means that the module will be started 
   * automatically! This method is used by <code>MoepIRC</code> to check if a 
   * module is auto-start. If the class is not haltable as checked in 
   * <code>isHaltable</code>, it returns <code>false</code>.
   * @param cls The class which is to check.
   * @return <code>true</code> if the class is haltable and set as auto-start
   *         with a field <code>AUTOSTART</code> of ANY TYPE with ANY VALUE(!).
   */
  protected static final boolean isAutoStart(Class cls) {
    if (!isHaltable(cls))
      return false;
    try {
      cls.getDeclaredField("AUTOSTART");
      return true;
    } catch (Exception exc) {
      return false;
    }
  }

// ------------------------------

  /** 
   * This method must be overwritten by modules which will be haltable! This 
   * method must be declared in the module, because the <code>MoepIRC</code> 
   * client will not give an halt-possibility, if it is not declared. For 
   * example, an away-and-back-module should declare such a <code>halt</code> 
   * method, because the user will want to stop the away-postings. But a 
   * module, which is done very quickly, should not declare a 
   * <code>halt</code>, because there's nothing to stop. 
   */
  public void halt() {
    try {
      finalize();
    } catch (Throwable trw) {
      trw.printStackTrace();
    }
  }
  
// ------------------------------

}