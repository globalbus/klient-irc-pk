Changelog of moepII (IRC client program):

moepII 0.65 (21.10.2004):
 * Added:   Module NickAdmin which administers several nicknames.
 * Added:   The fonts are set immediately after being chosed in the setup.
 * Removed: The OK/Cancel question in the setup.
 * Changed: Updated whole client for IRClib 1.05.
 * Changed: All popup menus are now JPopupMenus instead of AWT components.
 * Fixed:   The order-list-function had a bug. 

moepII 0.64 (23.11.2003):
 * Changed: The nicklist is now much better and faster. More than one selection
            is possible and everything is much faster. These improvements were
            achieved with JList instead of List and new algorithms for sorting,
            adding and removing nicks.
 * Fixed:   The window-lock- and order-list-functions had the same accelerator 
            (Ctrl + L); now you can press Ctrl + N to order the nicklist.
 * Fixed:   Sometimes it it only disconnected instead of shutting down the 
            whole application due to an IRClib bug.

moepII 0.63 (12.11.2003):
 * Changed: Updated whole client for IRClib 1.03.
 * Changed: ChanCenter's title doesn't contain the word "ChanCenter" anymore.
 * Fixed:   It doesn't beep when *you* open a query.
 * Fixed:   DCCGet had problems with long IP addresses; thanks to Thomas Kurz
            <Thomas.Kurz@shs.de>
 * Fixed:   DCCGet threw an exception when the host address was surrounded by
            quotationmarks ("").

moepII 0.62 (13.09.2003):
 * Added:   A small frame which shows the progress when starting the client.
 * Changed: Updated whole client for IRClib 0.90. Nevertheless moepII requires
            Java 1.4!
 * Changed: The version-update is now more ugly.
 * Changed: The Nicklist -> Whois and Ctrl + W is now available for queries. If
            no text is selected, the person you're talking with is whoised.
 * Changed: The Nicklist -> Slap and Ctrl + S is now available for queries.
 * Fixed:   Beginning and ending of the portrange were in the wrong order; had no
            consequences than the wrong display.
 * Fixed:   Nicklist -> Whois and Ctrl + W sent a WHOIS command without parameters
            if no nickname and no text was selected. Now it does nothing.

moepII 0.61 (26.08.2003):
 * Fixed:   The amount of users was not resetted when removeAllNicks() in the
            ChanPanel class was invoked.
 * Fixed:   Client disconnected immediatly after being connected if no modules
            were loaded because of two unwanted System.out.printlns I had
            forgotten to remove.

moepII 0.60 (18.08.2003):
 * Added:   The Module.getMoep method returns a reference to the owning moepII
            client. This opens the door to great possibilities; for example the
            module can add an own IRCEventListener to moepII's IRCConnection and
            much more.
 * Added:   The Module.setModuleListener(ModuleListener) method to set the only
            module listener.
 * Added:   A module can be made autostartable which means that the client starts
            it whenether it connects. Such a module must define a class-field
            with the name 'AUTOSTART' and be haltable (define a halt method).
            The type and value of the 'AUTOSTART' field do not matter.
 * Added:   The module DCCSend which is able to send files via DCC to other IRC
            clients which support DCC. The module offers not 100% of the comfort
            and possibilities (no resume, not multiple transfers at the same
            time) of other clients, but it runs and I like it.
 * Added:   The module DCCGet which is able to receive multiple files via DCC.
            Like DCCSend, it doesn't offer the same possiblities of other IRC
            clients, but it can simply receive multiple files at the same time.
 * Added:   The class Start without any package which starts the client. So you
            can start the client using the command 'java -cp moep.jar Start'.
 * Removed: The Module.addModuleListener(ModuleListener) method because only one
            listener can be set.
 * Removed: The out.sendLine method in modules. Use moep.parseCmd with the same
            arguments instead.
 * Changed: Updated whole client for IRClib 0.82.
 * Changed: The client comes now with the modules DCCGet, DCCSend, IPInfo and
            Winamp (for Windows). But they are not enabled (you can do this by
            creating a 'modules.conf' and adding them there with
            moduleN=modulename while N is the index beginning with 0; this is
            also explained in the FAQs).
 * Changed: The code is splitted into 15 files. Thus, also some changes in the
            setup work quicker and modules have much greater possibilities
            because they have access total access to the MoepIRC and the
            IRCConnection class.

moepII 0.51 (08.08.2003):
 * Added:   A right-click popup menu on the tabs.
 * Added:   A right-click popup menu in the input-line.
 * Added:   The ChanCenter shows who set a ban with a timestamp.
 * Changed: The ChanCenter cannot be opened with a double-click in the textpane
            anymore; instead the right-click menu of the textpane provides an
            option to open it.
 * Changed: Text does not scroll down when some text is selected.
 * Changed: When using File -> Connect to the user is asked wether he wants an
            SSL connection or not.

moepII 0.50 (01.08.2003):
 * Added:   SSL IRC connections!
 * Added:   A right-click popup menu in the nicklist.
 * Added:   Doubleclick in the channel makes the ChanCenter visible, if no text
            is selected.
 * Added:   A right-click popup menu in the chanpane.
 * Added:   The button "Add" to set bans in the ChanCenter.
 * Removed: The buttons under the nicklist.
 * Removed: Menus to set the selected text as topic or add it to the existing
            topic.
 * Changed: Inserted some separators.
 * Changed: Updated whole client for IRClib 0.80.
 * Changed: The exception's message is shown when connecting fails.
 * Fixed:   Bug in ChanCenter that "OK" button threw an NullPointerException when
            the limit was empty before.
 * Fixed:   The splitpane which divides the nicklist from the textpane stayed
            statically when resizing the window.

moepII 0.49 (24.07.2003):
 * Added:   Background color.
 * Added:   Nicklist and inputfield are now in colors, too.
 * Added:   The possibility to ban hostmasks in the ChanCenter.
 * Changed: Updated whole client for IRClib 0.71.
 * Changed: Privmsgs to persons or channels which have no window open are
            printed.
 * Fixed:   Terrible GUI. The Setupwindw and ChanCenter got redesigned.
 * Fixed:   A limit can be changed directly with the ChanCenter without removing
            it first and then setting a new one.

moepII 0.48 (22.07.2003):
 * Added:   Own text, other's text and server's text can be in different colors.
 * Added:   The shortcut NOT for NOTICE.
 * Added:   The NOTICE doesn't need the colon (':') to divide the recipients
            from the message anymore.
 * Changed: Revised code of moepII and improved javadoc- documentation.
 * Changed: The versioncheck is now done in a new thread.
 * Changed: Now compiled under Java 1.4.2.
 * Changed: Updated whole client for IRClib 0.70.
 * Changed: PRIVMSGs and NOTICEs sent as MSG, NOT or NOTICE command are no longer
            printed with "# Executed ..." but as normal messages and notices.
 * Fixed:   Mnemonic of execute of modules.

moepII 0.47 (19.06.2003):
 * Added:   Wrote a module for Windows OS and WinAmp player which posts the
            WinAmp Mp3-title.
 * Changed: The algorithm which calculates the delays between the line pasted by
            the PasteDialgo should now be better:
            <length of line> ^ 1.5 + 100 * <number of line> + 1200 milliseconds.
 * Fixed:   Modules which are not haltable can be executed more than one time
            now.

moepII 0.46 (10.06.2003):
 * Changed: The focus is requested when clicking on the tabs or joining a
            channel.
 * Changed: The algorithm to get the delays between line pasted by the
            PasteDialog is now: <number of line> * 17 * <length of line>
 * Changed: Updated whole client for IRClib 0.62.
 * Fixed:   Because of new IRClib version the perform should now be always
            executed. But I dunno how often I've claimed that yet and I also
            dunno if it is the truth now.

moepII 0.45 (31.05.2003):
 * Added:   The PasteDialog which is used to paste longer texts (e.g. code-
            segments) to one channel.
 * Changed: Updated whole client for IRClib 0.52.
 * Changed: If debugging is disabled, nothing is printed out to console.
 * Changed: The accelerator 'Ctrl' + 'p' goes now to the PasteDialog.

moepII 0.44 (26.05.2003):
 * Added:   Sound highlight (beep) when the nickname is contained in an incoming
            PRIVMSG can be activated.
 * Changed: Updated whole client for IRClib 0.50. See its changelog for more
            information.
 * Changed: Logfiles' entries are now leaded by a short report when the session
            began.
 * Fixed:   MoepIRC.IRCPanel.updateText tried to log the Console's traffic.
 * Fixed:   Replaced 'reveiced' with 'received' :-)

moepII 0.43 (13.05.2003):
 * Added:   Published a example-modules (awayscript).
 * Changed: Updated homepage (module-page) and FAQ.
 * Changed: Lengthened the 'Halve chattext after' textfield in the setup.
 * Fixed:   Changed many 'private'-modifiers which were not compliant to Java
            1.4.1_02 into 'protected'.
 * Fixed:   The icon is now also shown at the first start when nothing is
            configured yet.

moepII 0.42 (09.05.2003):
 * Added:   Command-shortcut '/SAY <msg>' for '/PRIVMSG <on-top-chan> :<msg>'.
 * Added:   Modules can be en- and disabled generally in the setup.
 * Added:   A module should extend the class Module.
 * Added:   A module's action is executed in a new Thread.
 * Added:   A module can send lines to the IRC with irc.sendLine(String line).
 * Added:   A module can be halted. Condition is, that the module extends the
            class Module and declares an own halt-method without any parameters
            (the halt-method of the Module-class must be overridden).
 * Added:   A versioncheck which looks for updates via HTTP.
 * Removed: The history is no longer available with !-commands but only with the
            UP- and DOWN-arrows.
 * Changed: Some 'abstract' and some 'protected' declarations.
 * Changed: Extended command-history up to ten commands instead of five.
 * Changed: 'Module' isn't shown in the menubar if no modules were loaded.
 * Changed: 'Module' -> '<module>' and 'Module' -> '<module>' -> 'Start' &
            'Halt' have now a mnemonic.
 * Fixed:   The perform is now executed when connection really always.
 * Fixed:   Not only too long, also nicknames which are invalid for any other
            reason are recognized; the way is completely different from 0.41
            and completely independent from the non-RFC-compliant RPL_ISUPPORT
            reply.
 * Fixed:   'Help' -> 'Info' doesn't throw an Exception if there isn't an icon
            anymore.

moepII 0.41 (29.04.2003):
 * Added:   'File' -> 'Connect' and 'File' -> 'Disconnect' are en- and disabled.
 * Added:   Perform can be executed again with 'Options' -> 'Execute Perform'.
 * Added:   User-modes event is now treated; user-modes are reported in on-top
            IRCPanel.
 * Added:   Console is printed to files if a property 'debug' is set 'true' in
            the configuration.
 * Added:   When the textarea with the chat-text is clicked one time, the focus
            is set to the command-line below.
 * Changed: User isn't asked if he really wants to quit if he isn't connected.
 * Fixed:   Noticed that indpendent isn't spelled independant. :-)
 * Fixed:   Too long nicknames are cutted to nine chars or, if a ISUPPORT reply
            with a NICKLEN parameter is received, to the maximum parameter the
            network allows. This fixes horrible errors when connecting with a
            too long nickname.
 * Fixed:   Perform is now executed correctly on all RFC 1459 compliant 
            networks.
 * Fixed:   If the NICK command of the registration is wrong (no nickname, 
            nickname already in use), the client requests a new one from the 
            user or quits.
 * Fixed:   Strange behaviour of the cut-channel-text function.

moepII 0.40 (12.04.2003):
 * Added:   Small modules written in Java can be loaded into the client.
 * Removed: 'New Topic'-button; topic is now changed by performing an action on
            the topic-textfield.
 * Changed: The accelerator 'Ctrl' + 'w' for whois now searches now first for a
            selected string in the chat-textarea and if this is empty for a
            selected nick in the nicklist.
 * Changed: New queries opened by the user of the client are selected on top
            while queries opened on PRIVMSGs are not on top.
 * Changed: Moved the X-button to the right top.
 * Changed: Extended documentation.
 * Changed: The settings-files are have no '.conf' as fileextension instead of
            '.ini'.
 * Changed: The moep.jar has no longer ZIP-compression.
 * Changed: The fonts of status- and channel-windows can be chosen from a list.
 * Changed: The client comes now only in JLF, the default cross platform look
            and feel.
 * Changed: Got some new icons and logos; thanks to Carsten Wiesbaum alias Radi
            Skull <carsten.wiesbaum@web.de>.
 * Fixed:   The first colon is no longer parsed when using '/MSG'-command.

moepII 0.31 (22.03.2003):
 * Added:   Buttons at the bottom of the nicklist for fast actions: +o, -o and
            kick.
 * Added:   Command-shortcut '/MSG <chan> <msg>' for '/PRIVMSG <chan> :<msg>'
            because it is so widely used. 
 * Changed: Queries are opened by another way.
 * Changed: Away- and unaway replies are now shown in the active window.
 * Changed: Channel-cut-function can be en- and disabled.
 * Changed: When trying to join a selected channel and its name does not start
            with a channel-character, a '#' is attached in front of it.
 * Fixed:   Unlocking the scrollbar runs now. 
 * Fixed:   Missing classes in manifest-file.

moepII 0.30 (14.03.2003):
 * Added:   The UP- and DOWN-arrows of the keyboard are now used to scroll up 
            and down in the command-history which takes the last five commands.
 * Added:   Option to lock the scrollbar so that the client does not scroll down
            automatically when a new line to the window was sent. 
 * Added:   Wrote a documentation which explains the setup with the new setup-
            window and the basics of the use of IRC with moepII.
 * Changed: The !-command-access to the command-history got much better. Now the
            last five commands of every window are saved and you can call them
            with the !-command: !h for help, !p prints out the last 5, !<n> 
            calls the <n>th command and ! calls the last command.
 * Changed: The ChanCenter's title now also holds the count of operators in the
            channel.
 * Changed: Added the timestamp to the logfiles.
 * Fixed:   Some String-calculations like many indexOf-methods got faster 
            because of less stupid use.
 * Fixed:   The channels and queries are now seperated from another.
 * Fixed:   The channel's name is broken with three dots if it is too long for 
            the title of the ChanCenter.
 * Fixed:   The client now can handle all types of channel names described in 
            RFC 2812: They may begin with #, &, !, +.
 * Fixed:   A possible flood bug was that on every mode change the client 
            requested the channel's modes. That's fixed now.
 * Fixed:   Several exceptions of the channel-cut-function are now fixed.
 * Fixed:   Now a disconnect should be recognized and shown by the client 
            correctly.
 * Fixed:   The bug that after a disconnect and reconnect the user could not 
            join any channels is fixed.
 * Fixed:   The name is changed also on the IRC connection if there is one 
            established when the user changes his nickname in the setup.
 * Fixed:   Perform is now executed correctly: when the server sends a 005-
            reply.
 * Fixed:   The hiding of executed commands with by beginning them with a dot 
            '.' now runs correctly. 
 * Fixed:   Exception when removing a key in capitals with a lower-case written
            key (e.g. in a chan +k myKEY a mode -k MYkey).
 * Fixed:   Strange exception when joining a channel with an active key.

moepII 0.20 (04.03.2003):
 * Added:   Shortcuts for important actions in the menu. So we probably need no
            right-click-popups.
 * Added:   A setup-window where the IRC connection settings, some client 
            settings and the perform can be set.
 * Added:   The ChanCenter with information about the modes and bans of the 
            channel.
 * Added:   Screenposition and -size is now saved and used again after 
            applicationrestart.
 * Changed: Only 'Yes' and 'No' at exit dialog.
 * Fixed:   Sometimes a new empty window is opened when someone joins.
 * Fixed:   Highlighting bug ArrayIndexOutOfBounds on queries.
 * Fixed:   After the reply to /topic <chan> the channel is not opened if it 
            does not exist.
 * Fixed:   Visual highlight of channels in the tabbar is no more after you 
            switched from active to another channel at the unselected channel.
 * Fixed:   After a kick-ban you can close the channel.
 * Fixed:   The nicklist resetted after a kick.
 * Fixed:   The nicklist is ordered on every join: 1. opers (@), 2. voiced (+),
            3. normal. To order the list by status and by abc ascending, go
            'Nicklist' -> 'Order List'.
 * Fixed:   Timestamps at /WHOIS is now correct.
 * Fixed:   Topic-information are now shown .
 * Fixed:   When the other user of a query changed the name, only in the tab his
            name was updated, so that you couldn't send him messages anymore.
 * Fixed:   On closing/parting a channel with the X-Button, the channel is not 
            re-opened for a very short time because of the incoming PART-event 
            anymore.
 * Fixed:   The GUI's low performance should be better because of a new thread 
            for the connection.
 * Fixed:   Not all channels were closed on disconnect.
 * Fixed:   The banlist is not double any more after a channel-rejoin.
 * Fixed:   If nickname is in use, the client does not nerv with his request for
            a new nickname anymore.
 * Fixed:   So many other fucking bugs.

moepII 0.11 (06.02.2003):
 * Dunno what I changed... probably almost everything.

moepII 0.10 (05.02.2003):
 * First running version.

moepII 0.00 (12.01.2003):
 * Deleted all code and rebootet :-)

moepII 0.00 (22.09.2002):
 * Began writing some code which was so stupid that it never had a chance to do
   what I wanted.
