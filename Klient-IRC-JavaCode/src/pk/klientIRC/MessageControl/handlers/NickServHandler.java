package pk.klientIRC.MessageControl.handlers;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IMessageControl;
import pk.klientIRC.Contracts.IMessageControlHandler;
import pk.klientIRC.Contracts.IMessageControlMain;
import pk.klientIRC.Contracts.ISend;
import pk.klientIRC.Localization.Messages;
@Component
class NickServHandler extends JPanel implements IMessageControlHandler{

	private static final long serialVersionUID = 1L;
	private static final String HANDLERNAME = "NickServ"; //$NON-NLS-1$
	private IMessageControl messageControl;
	private NickServPanel nickServPanel;
	private boolean enabled=false;
	@Autowired
	public NickServHandler(IMessageControlMain messageControl, ISend send){
		this.setLayout(new BorderLayout());
		messageControl.setName(HANDLERNAME);
		this.messageControl=messageControl;
		nickServPanel=new NickServPanel(send);
		JScrollPane scrollPane = new JScrollPane(nickServPanel);
		scrollPane.setPreferredSize(new Dimension(200,100));
		this.add(scrollPane, BorderLayout.NORTH );
		JScrollPane scrollpane = new JScrollPane(messageControl.getPanel());
		scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		this.add(scrollpane, BorderLayout.CENTER);
	}
	@Override
	public String getHandlerName(){
		return HANDLERNAME;
	}
	@Override
	public JPanel getPanel(){
		return this;
	}
	@Override
	public void addTopicMsg(String text) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addNickChangeMsg(String text) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void addMessage(String text) {
		messageControl.addMessage(text);//messages from nickserv
		
	}
	@Override
	public void addMessage(String currentNick, String msg) {
		messageControl.addMessage(currentNick, msg);//own messages
	}
	@Override
	public IMessageControl getInstance() {
		return null;
		//we should not clone this.
	}
	@Override
	public JTextPane getTextPane() {
		return messageControl.getTextPane();
	}
	@Override
	public StyledDocument getStyledDocument() {
		// TODO Auto-generated method stub
		return messageControl.getStyledDocument();
	}
	@Override
	public boolean isEnabledHandler() {
		return enabled;
	}
	@Override
	public void setEnabledHandler(boolean b) {
		enabled=b;
		
	}
	@Override
	public void clear() {
		messageControl.clear();
	}

}

class NickServPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	private ISend send;
	private boolean visibility1=false;
	private boolean visibility2=false;
	JButton buttonId;
	JButton buttonGhost;
	JButton buttonShow1;
	JButton buttonShow2;
	JButton buttonLogIn;
	JButton buttonRegister;
	JButton buttonUnregister;
	JButton buttonGroup;
	JButton buttonUngroup;
	JButton buttonHelp;
	JButton buttonSetEmail;
	JButton buttonSetPass;
	JButton buttonSetName;
	
	NickServPanel(ISend send){
		this.send=send;
		this.setLayout(new InvisibleGridLayout(0,1));
		
		buttonId=new JButton(Messages.getString("NickServHandler.IdentifyButton")); //$NON-NLS-1$
		buttonGhost=new JButton(Messages.getString("NickServHandler.GhostButton")); //$NON-NLS-1$
		buttonShow1=new JButton();
		buttonShow2=new JButton();
		buttonLogIn=new JButton(Messages.getString("NickServHandler.LoginButton")); //$NON-NLS-1$
		buttonRegister=new JButton(Messages.getString("NickServHandler.RegisterButton")); //$NON-NLS-1$
		buttonUnregister=new JButton(Messages.getString("NickServHandler.UnregisterButton")); //$NON-NLS-1$
		buttonGroup=new JButton(Messages.getString("NickServHandler.AddNickButton")); //$NON-NLS-1$
		buttonUngroup=new JButton(Messages.getString("NickServHandler.RemoveNickButton")); //$NON-NLS-1$
		buttonHelp=new JButton(Messages.getString("NickServHandler.HelpButton")); //$NON-NLS-1$
		buttonSetEmail=new JButton(Messages.getString("NickServHandler.ChangeEmailButton")); //$NON-NLS-1$
		buttonSetPass=new JButton(Messages.getString("NickServHandler.ChangePasswordButton")); //$NON-NLS-1$
		buttonSetName=new JButton(Messages.getString("NickServHandler.ChangeNameButton")); //$NON-NLS-1$
		
		buttonId.addActionListener(this);
		buttonGhost.addActionListener(this);	
		buttonRegister.addActionListener(this);
		buttonUnregister.addActionListener(this);
		buttonLogIn.addActionListener(this);
		buttonGroup.addActionListener(this);
		buttonUngroup.addActionListener(this);
		buttonHelp.addActionListener(this);
		buttonShow1.addActionListener(this);
		buttonShow2.addActionListener(this);
		buttonSetEmail.addActionListener(this);
		buttonSetPass.addActionListener(this);		
		buttonSetName.addActionListener(this);
		
		this.add(buttonId);
		this.add(buttonGhost);
		this.add(buttonShow1);
		this.add(buttonLogIn);
		this.add(buttonRegister);
		this.add(buttonUnregister);
		this.add(buttonGroup);
		this.add(buttonUngroup);
		this.add(buttonShow2);
		this.add(buttonSetEmail);
		this.add(buttonSetPass);
		this.add(buttonSetName);
		this.add(buttonHelp);
		
		switchVisilibity1();
		switchVisilibity2();
	}
	
	private void switchVisilibity1(){
		buttonRegister.setVisible(visibility1);
		buttonLogIn.setVisible(visibility1);
		buttonGroup.setVisible(visibility1);
		buttonUnregister.setVisible(visibility1);
		buttonUngroup.setVisible(visibility1);
		visibility1=!visibility1;
		if(visibility1){
			buttonShow1.setText(Messages.getString("NickServHandler.AccountManagmentShowButton")); //$NON-NLS-1$
		}else{
			buttonShow1.setText(Messages.getString("NickServHandler.AccountManagementHideButton")); //$NON-NLS-1$
		}
	}
	
	private void switchVisilibity2(){
		buttonSetEmail.setVisible(visibility2);
		buttonSetPass.setVisible(visibility2);
		buttonSetName.setVisible(visibility2);
		visibility2=!visibility2;
		if(visibility2){
			buttonShow2.setText(Messages.getString("NickServHandler.SettingsShowButton")); //$NON-NLS-1$
		}else{
			buttonShow2.setText(Messages.getString("NickServHandler.SettingsHideButton")); //$NON-NLS-1$
		}
	}
	
	private void pushMessage(String text){
		send.pushMessage(text);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String val = ""; //$NON-NLS-1$
		String val1 = ""; //$NON-NLS-1$
		if(e.getSource()==buttonId){
			val = JOptionPane.showInputDialog(Messages.getString("NickServHandler.PasswordDialog"), val); //$NON-NLS-1$
			if(val!=null){
				pushMessage("/msg NickServ ID "+val); //$NON-NLS-1$
			}	
		}else if(e.getSource()==buttonGhost){
			val = JOptionPane.showInputDialog(Messages.getString("NickServHandler.NickDialog"), val); //$NON-NLS-1$
			if(val!=null){
				val1 = JOptionPane.showInputDialog(Messages.getString("NickServHandler.PasswordDialog"), val1); //$NON-NLS-1$
				if(val1!=null){
					pushMessage("/msg NickServ GHOST "+val+" "+val1); //$NON-NLS-1$ //$NON-NLS-2$
				}	
			}
		}else if(e.getSource()==buttonLogIn){
			val = JOptionPane.showInputDialog(Messages.getString("NickServHandler.NickDialog"), val); //$NON-NLS-1$
			if(val!=null){
				val1 = JOptionPane.showInputDialog(Messages.getString("NickServHandler.PasswordDialog"), val1); //$NON-NLS-1$
				if(val1!=null){
					pushMessage("/msg NickServ ID "+val+" "+val1); //$NON-NLS-1$ //$NON-NLS-2$
				}	
			}
		}else if(e.getSource()== buttonRegister){
			val = JOptionPane.showInputDialog(Messages.getString("NickServHandler.PasswordDialog"), val); //$NON-NLS-1$
			if(val!=null){
				val1 = JOptionPane.showInputDialog(Messages.getString("NickServHandler.EmailDialog"), val1); //$NON-NLS-1$
				if(val1!=null){
					pushMessage("/msg NickServ REGISTER "+val+" "+val1); //$NON-NLS-1$ //$NON-NLS-2$
				}	
			}
		}else if(e.getSource()==buttonUnregister){
			val = JOptionPane.showInputDialog(Messages.getString("NickServHandler.NickDialog"), val); //$NON-NLS-1$
			if(val!=null){
				val1 = JOptionPane.showInputDialog(Messages.getString("NickServHandler.PasswordDialog"), val1); //$NON-NLS-1$
				if(val1!=null){
					pushMessage("/msg NickServ DROP "+val+" "+val1); //$NON-NLS-1$ //$NON-NLS-2$
				}	
			}
		}else if(e.getSource()==buttonGroup){
			pushMessage("/msg NickServ GROUP "); //$NON-NLS-1$
		}else if(e.getSource()==buttonUngroup){
			val = JOptionPane.showInputDialog(Messages.getString("NickServHandler.NickDialog"), val); //$NON-NLS-1$
			if(val!=null){
				pushMessage("/msg NickServ UNGROUP "+val); //$NON-NLS-1$
			}
		}else if(e.getSource()==buttonSetEmail){
			val = JOptionPane.showInputDialog(Messages.getString("NickServHandler.NewEmailDialog"), val); //$NON-NLS-1$
			if(val!=null){
				pushMessage("/msg NickServ SET EMAIL "+val); //$NON-NLS-1$
			}
		}else if(e.getSource()==buttonSetPass){
			val = JOptionPane.showInputDialog(Messages.getString("NickServHandler.NewPasswordDialog"), val); //$NON-NLS-1$
			if(val!=null){
				pushMessage("/msg NickServ SET PASSWORD "+val); //$NON-NLS-1$
			}	
		}else if(e.getSource()==buttonSetName){
			val = JOptionPane.showInputDialog(Messages.getString("NickServHandler.NewAccountDialog"), val); //$NON-NLS-1$
			if(val!=null){
				pushMessage("/msg NickServ SET ACCOUNTNAME "+val); //$NON-NLS-1$
			}
		}else if(e.getSource()==buttonHelp){
			pushMessage("/msg NickServ help");//$NON-NLS-1$
		}else if(e.getSource()==buttonShow1){
			switchVisilibity1();
		}else if(e.getSource()==buttonShow2){
			switchVisilibity2();
		}
	}
}