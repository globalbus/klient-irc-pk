package pk.klientIRC.MessageControl.handlers;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.StyledDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IMessageControl;
import pk.klientIRC.Contracts.IMessageControlHandler;
import pk.klientIRC.Contracts.IMessageControlMain;
import pk.klientIRC.Contracts.ISend;
import pk.klientIRC.Localization.Messages;

@Component
class ChanServHandler extends JPanel implements IMessageControlHandler{
	private static final long serialVersionUID = 1L;
	private static final String HANDLERNAME = "ChanServ"; //$NON-NLS-1$
	private IMessageControl messageControl;
	private ChanServPanel chanServPanel;
	private boolean enabled = false;

	@Autowired
	private ChanServHandler(IMessageControlMain messageControl, ISend send) {
		this.setLayout(new BorderLayout());
		messageControl.setName(HANDLERNAME);
		this.messageControl = messageControl;
		chanServPanel = new ChanServPanel(send);
		JScrollPane scrollPane = new JScrollPane(chanServPanel);
		scrollPane.setPreferredSize(new Dimension(200, 100));
		this.add(scrollPane, BorderLayout.NORTH);
		JScrollPane scrollpane = new JScrollPane(messageControl.getPanel());
		scrollpane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		this.add(scrollpane, BorderLayout.CENTER);
	}

	@Override
	public String getHandlerName() {
		return HANDLERNAME;
	}

	@Override
	public JPanel getPanel() {
		return this;
	}

	@Override
	public void addTopicMsg(String text) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addNickChangeMsg(String text) {
		// TODO Auto-generated method stub

	}

	@Override
	public void addMessage(String text) {
		messageControl.addMessage(text);

	}

	@Override
	public void addMessage(String currentNick, String msg) {
		messageControl.addMessage(currentNick, msg);
	}

	@Override
	public IMessageControl getInstance() {
		return null;
		// we should not clone this.
	}

	@Override
	public JTextPane getTextPane() {
		return messageControl.getTextPane();
	}

	@Override
	public StyledDocument getStyledDocument() {
		// TODO Auto-generated method stub
		return messageControl.getStyledDocument();
	}

	@Override
	public boolean isEnabledHandler() {
		return enabled;
	}

	@Override
	public void setEnabledHandler(boolean b) {
		enabled = b;

	}

	@Override
	public void clear() {
		messageControl.clear();

	}

}

class ChanServPanel extends JPanel implements ActionListener{
	private static final long serialVersionUID = 1L;

	private ISend send;
	private boolean visibility1 = false;
	private boolean visibility2 = false;

	JButton buttonHelp;
	JButton buttonRegister;
	JButton buttonUnregister;
	JButton buttonAccessList;
	JButton buttonAccessAdd;
	JButton buttonAccessDel;
	JButton buttonAkickList;
	JButton buttonAkickAdd;
	JButton buttonAkickDel;
	JButton buttonQuiet;
	JButton buttonUnquiet;
	JButton buttonTopic;
	JButton buttonShow1;
	JButton buttonShow2;

	public ChanServPanel(ISend send) {
		this.send = send;
		this.setLayout(new InvisibleGridLayout(0, 1));

		buttonHelp = new JButton(Messages.getString("ChanServHandler.ButtonHelp")); //$NON-NLS-1$
		buttonRegister = new JButton(Messages.getString("ChanServHandler.ButtonRegister")); //$NON-NLS-1$
		buttonUnregister = new JButton(Messages.getString("ChanServHandler.ButtonUnregister")); //$NON-NLS-1$
		buttonAccessList = new JButton(Messages.getString("ChanServHandler.ButtonList")); //$NON-NLS-1$
		buttonAccessAdd = new JButton(Messages.getString("ChanServHandler.ButtonGive")); //$NON-NLS-1$
		buttonAccessDel = new JButton(Messages.getString("ChanServHandler.ButtonTake")); //$NON-NLS-1$
		buttonAkickList = new JButton(Messages.getString("ChanServHandler.ButtonListAutokick")); //$NON-NLS-1$
		buttonAkickAdd = new JButton(Messages.getString("ChanServHandler.ButtonAddAutokick")); //$NON-NLS-1$
		buttonAkickDel = new JButton(Messages.getString("ChanServHandler.ButtonRemoveAutokick")); //$NON-NLS-1$
		buttonQuiet = new JButton(Messages.getString("ChanServHandler.ButttonMute")); //$NON-NLS-1$
		buttonUnquiet = new JButton(Messages.getString("ChanServHandler.ButtonUnmute")); //$NON-NLS-1$
		buttonTopic = new JButton(Messages.getString("ChanServHandler.ButtonChannelTopic")); //$NON-NLS-1$
		buttonShow1 = new JButton();
		buttonShow2 = new JButton();

		buttonHelp.addActionListener(this);
		buttonRegister.addActionListener(this);
		buttonUnregister.addActionListener(this);
		buttonAccessList.addActionListener(this);
		buttonAccessAdd.addActionListener(this);
		buttonAccessDel.addActionListener(this);
		buttonAkickList.addActionListener(this);
		buttonAkickAdd.addActionListener(this);
		buttonAkickDel.addActionListener(this);
		buttonQuiet.addActionListener(this);
		buttonUnquiet.addActionListener(this);
		buttonTopic.addActionListener(this);
		buttonShow1.addActionListener(this);
		buttonShow2.addActionListener(this);
		
		this.add(buttonRegister);
		this.add(buttonUnregister);
		this.add(buttonTopic);
		this.add(buttonShow1);
		this.add(buttonAccessList);
		this.add(buttonAccessAdd);
		this.add(buttonAccessDel);
		this.add(buttonShow2);
		this.add(buttonAkickList);
		this.add(buttonAkickAdd);
		this.add(buttonAkickDel);
		this.add(buttonQuiet);
		this.add(buttonUnquiet);
		this.add(buttonHelp);

		switchVisilibity1();
		switchVisilibity2();
	}

	private void switchVisilibity1() {
		buttonAccessList.setVisible(visibility1);
		buttonAccessAdd.setVisible(visibility1);
		buttonAccessDel.setVisible(visibility1);
		visibility1 = !visibility1;
		if (visibility1) {
			buttonShow1.setText(Messages.getString("ChanServHandler.PrivilegesShow")); //$NON-NLS-1$
		} else {
			buttonShow1.setText(Messages.getString("ChanServHandler.PrivilegesHide")); //$NON-NLS-1$
		}
	}

	private void switchVisilibity2() {
		buttonAkickList.setVisible(visibility2);
		buttonAkickAdd.setVisible(visibility2);
		buttonAkickDel.setVisible(visibility2);
		buttonQuiet.setVisible(visibility2);
		buttonUnquiet.setVisible(visibility2);
		visibility2 = !visibility2;
		if (visibility2) {
			buttonShow2.setText(Messages.getString("ChanServHandler.ModeratorShow")); //$NON-NLS-1$
		} else {
			buttonShow2.setText(Messages.getString("ChanServHandler.ModeratorHide")); //$NON-NLS-1$
		}
	}

	private void pushMessage(String text) {
		send.pushMessage(text);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String val = ""; //$NON-NLS-1$
		String val1 = ""; //$NON-NLS-1$
		if(e.getSource()==buttonRegister){
			val = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.ChannelName"), val); //$NON-NLS-1$
			if (val != null) {
				pushMessage("/msg ChanServ DROP #" + val); //$NON-NLS-1$
			}
		}else if(e.getSource()==buttonUnregister){
			val = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.ChannelName"), val); //$NON-NLS-1$
			if (val != null) {
				pushMessage("/msg ChanServ REGISTER #" + val); //$NON-NLS-1$
			}
		}else if(e.getSource()==buttonTopic){
			val = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.ChannelName"), val); //$NON-NLS-1$
			if (val != null) {
				val1 = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.Topic"), val1); //$NON-NLS-1$
				if (val1 != null) {
					pushMessage("/msg ChanServ UNQUIET #" + val + " " //$NON-NLS-1$ //$NON-NLS-2$
							+ val1);
				}
			}
		}else if(e.getSource()==buttonAccessList){
			val = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.ChannelName"), val); //$NON-NLS-1$
			if (val != null) {
				pushMessage("/msg ChanServ ACCESS #" + val + " LIST"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}else if(e.getSource()==buttonAccessAdd){
			if (val != null) {
				val1 = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.UserName"), val1); //$NON-NLS-1$
				if (val != null) {
					pushMessage("/msg ChanServ ACCESS #" + val + " ADD " //$NON-NLS-1$ //$NON-NLS-2$
							+ val1);
				}
			}
		}else if(e.getSource()==buttonAccessDel){
			val = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.ChannelName"), val); //$NON-NLS-1$
			if (val != null) {
				val1 = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.UserName"), val1); //$NON-NLS-1$
				if (val != null) {
					pushMessage("/msg ChanServ ACCESS #" + val + " DEL " //$NON-NLS-1$ //$NON-NLS-2$
							+ val1);
				}
			}
		}else if(e.getSource()==buttonAkickList){
			val = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.ChannelName"), val); //$NON-NLS-1$
			if (val != null) {
				pushMessage("/msg ChanServ AKICK #" + val + " LIST"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}else if(e.getSource()==buttonAkickAdd){
			val = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.ChannelName"), val); //$NON-NLS-1$
			if (val != null) {
				val1 = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.UserName"), val1); //$NON-NLS-1$
				if (val != null) {
					pushMessage("/msg ChanServ AKICK #" + val + " ADD " //$NON-NLS-1$ //$NON-NLS-2$
							+ val1);
				}
			}
		}else if(e.getSource()==buttonAkickDel){
			val = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.ChannelName"), val); //$NON-NLS-1$
			if (val != null) {
				val1 = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.UserName"), val1); //$NON-NLS-1$
				if (val != null) {
					pushMessage("/msg ChanServ AKICK #" + val + " DEL " //$NON-NLS-1$ //$NON-NLS-2$
							+ val1);
				}
			}
		}else if(e.getSource()==buttonQuiet){
			val = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.ChannelName"), val); //$NON-NLS-1$
			if (val != null) {
				val1 = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.UserName"), val1); //$NON-NLS-1$
				if (val != null) {
					pushMessage("/msg ChanServ QUIET #" + val + " " + val1); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}
		}else if(e.getSource()==buttonUnquiet){
			val = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.ChannelName"), val); //$NON-NLS-1$
			if (val != null) {
				val1 = JOptionPane.showInputDialog(Messages.getString("ChanServHandler.UserName"), val1); //$NON-NLS-1$
				if (val != null) {
					pushMessage("/msg ChanServ UNQUIET #" + val + " " //$NON-NLS-1$ //$NON-NLS-2$
							+ val1);
				}
			}
		}else if(e.getSource()==buttonHelp){
			pushMessage("/msg ChanServ help"); //$NON-NLS-1$
		}else if(e.getSource()==buttonShow1){
			switchVisilibity1();
		}else if(e.getSource()==buttonShow2){
			switchVisilibity2();
		}		
	}
}