package pk.klientIRC.MessageControl.handlers;

import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IMessageControl;
import pk.klientIRC.Contracts.IMessageControlHandler;
import pk.klientIRC.Contracts.ISend;
@Component
class FriggHandler implements IMessageControlHandler{

		private static final String HANDLERNAME = "frigg"; //$NON-NLS-1$
		private ISend send;
		@Autowired
		private FriggHandler(ISend send){
			this.send=send;
		}
		@Override
		public String getHandlerName(){
			return HANDLERNAME;
		}
		@Override
		public JPanel getPanel(){
			return null;
		}
		@Override
		public boolean isEnabledHandler() {
			return true;//for invisible handler
		}
		@Override
		public void addTopicMsg(String text) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void addNickChangeMsg(String text) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public void addMessage(String text) {

		}
		@Override
		public void addMessage(String currentNick, String msg) {
			if(msg.equalsIgnoreCase("VERSION")) //$NON-NLS-1$
				send.pushMessage(String.format("/msg %s pk.KlientIRC", HANDLERNAME)); //$NON-NLS-1$
		}
		@Override
		public IMessageControl getInstance() {
			return null;
		}
		@Override
		public JTextPane getTextPane() {
			return null;
		}
		@Override
		public StyledDocument getStyledDocument() {
			return null;
		}
		@Override
		public void setEnabledHandler(boolean b) {
			//do nothing
			
		}
		@Override
		public void setName(String target) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public void clear() {
			// TODO Auto-generated method stub
			
		}
	}
