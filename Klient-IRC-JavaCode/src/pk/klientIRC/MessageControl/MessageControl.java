package pk.klientIRC.MessageControl;

import java.awt.GridLayout;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import pk.klientIRC.BeanFactory;
import pk.klientIRC.Contracts.IConfigProvider;
import pk.klientIRC.Contracts.IMessageControl;
import pk.klientIRC.Contracts.IMessageControlFormatting;
import pk.klientIRC.Contracts.IMessageControlMain;
import pk.klientIRC.Contracts.IVisibilityConfig;

@Component
@Scope("prototype")
@Primary
class MessageControl implements IMessageControlMain {

	private JTextPane textpane;
	private StyledDocument styledDocument;
	private Style nickStyle;
	private Style messageStyle;
	private JPanel contentPanel;
	// Constants
	private static final String NICKSTYLE = "NickStyle"; //$NON-NLS-1$
	private static final String MESSAGESTYLE = "MessageStyle"; //$NON-NLS-1$
	private static final String NICKCHANGESTYLE = "NickChangeStyle"; //$NON-NLS-1$
	private static final String TOPICSTYLE = "TopicStyle"; //$NON-NLS-1$
	private static final String NEWLINE = "\n"; //$NON-NLS-1$
	private static final String TIMESTYLE = "TimeStyle"; //$NON-NLS-1$

	private Style nickChangeStyle;
	private Style topicStyle;
	private Style timeStyle;
	private String name;

	@Autowired(required = false)
	private void setPlugins(List<IMessageControlFormatting> plugins) {
		// register plugins
		for (IMessageControlFormatting plugin : plugins)
			plugin.initListener(this);
	}

	@Autowired
	private MessageControl(IConfigProvider provider) {
		IVisibilityConfig visualConfig = provider
				.getConfig(IVisibilityConfig.class);
		if (visualConfig == null) {
			provider.loadDefaults();
			visualConfig = provider.getConfig(IVisibilityConfig.class);
		}
		contentPanel = new JPanel();
		textpane = new JTextPane();
		textpane.setEditable(false);
		contentPanel.add(textpane);
		contentPanel.setLayout(new GridLayout());
		styledDocument = new DefaultStyledDocument();
		textpane.setStyledDocument(styledDocument);
		//
		nickStyle = styledDocument.addStyle(NICKSTYLE, null);
		StyleConstants.setBold(nickStyle, true);
		StyleConstants.setForeground(nickStyle,
				visualConfig.getNickStyleColor());
		//
		messageStyle = styledDocument.addStyle(MESSAGESTYLE, null);
		StyleConstants.setBold(messageStyle, false);
		StyleConstants.setForeground(nickStyle,
				visualConfig.getMessageStyleColor());
		//
		timeStyle = styledDocument.addStyle(TIMESTYLE, null);
		StyleConstants.setBold(timeStyle, false);
		StyleConstants.setForeground(timeStyle,
				visualConfig.getTimeStyleColor());
		//
		nickChangeStyle = styledDocument.addStyle(NICKCHANGESTYLE, null);
		StyleConstants.setForeground(nickChangeStyle,
				visualConfig.getNickChangeStyleColor());
		//
		topicStyle = styledDocument.addStyle(TOPICSTYLE, null);
		StyleConstants.setForeground(topicStyle,
				visualConfig.getTopicStyleColor());
	}

	@Override
	public void addMessage(String text) {
		String text1 = text + NEWLINE;
		insertStyledMessage(text1, messageStyle, true);
	}

	@Override
	public void addMessage(String currentNick, String msg) {
		insertStyledMessage(currentNick + ": ", nickStyle, true); //$NON-NLS-1$
		insertStyledMessage(msg + NEWLINE, messageStyle, false);

	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	private void setCaret() {
		int len = styledDocument.getLength();// move caret to the end
		textpane.setCaretPosition(len);
	}

	@Override
	public JPanel getPanel() {
		return contentPanel;
	}

	@Override
	public IMessageControl getInstance() {
		return BeanFactory.getInstance().getMessageControl();
	}

	@Override
	public JTextPane getTextPane() {
		return textpane;
	}

	@Override
	public StyledDocument getStyledDocument() {
		return styledDocument;
	}

	@Override
	public void addTopicMsg(String text) {
		String text1 = text + NEWLINE;
		insertStyledMessage(text1, topicStyle, true);

	}

	private void insertStyledMessage(final String message, final Style style,
			final boolean time) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public synchronized void run() {
				try {
					if (time)
						styledDocument.insertString(styledDocument
								.getEndPosition().getOffset() - 1,
								getTimeStamp() + " ", timeStyle); //$NON-NLS-1$
					styledDocument.insertString(styledDocument.getEndPosition()
							.getOffset() - 1, message, style);
					setCaret();
				} catch (BadLocationException e) {
					//e.printStackTrace();
				}

			}
		});

	}

	@Override
	public void addNickChangeMsg(String text) {
		String text1 = text + NEWLINE;
		insertStyledMessage(text1, nickChangeStyle, true);

	}

	public String getTimeStamp() {
		return DateFormat.getTimeInstance(DateFormat.SHORT).format(new Date());
	}

	@Override
	public void clear() {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public synchronized void run() {
				try {
					styledDocument.remove(0, styledDocument.getEndPosition()
							.getOffset()-1);
				} catch (BadLocationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
}
