package pk.klientIRC.MessageControl.plugins;

import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.springframework.stereotype.Component;

import emoticons.EmoticonLoader;

import pk.klientIRC.Contracts.IMessageControl;
import pk.klientIRC.Contracts.IMessageControlFormatting;

@Component
public class Emoticons implements IMessageControlFormatting {
	Map<String, ImageIcon> references;

	private Emoticons() {
		references = new HashMap<String, ImageIcon>();
		ImageIcon image = new ImageIcon(
				EmoticonLoader.class.getResource("1.gif")); //$NON-NLS-1$
		ImageIcon image2 = new ImageIcon(
				EmoticonLoader.class.getResource("2.gif")); //$NON-NLS-1$
		references.put(":)", image); //$NON-NLS-1$
		references.put(":(", image2); //$NON-NLS-1$
	}

	@Override
	public void initListener(IMessageControl parent) {
		final JTextPane textpane = parent.getTextPane();
		textpane.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent event) {

				final DocumentEvent e = event;
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						if (e.getDocument() instanceof StyledDocument) {
							try {
								StyledDocument doc = (StyledDocument) e
										.getDocument();
								int start = e.getOffset();
								String text = doc.getText(start, e.getLength());
								for (String item : references.keySet()) {
									int i = text.indexOf(item);
									while (i >= 0) {
										final SimpleAttributeSet attrs = new SimpleAttributeSet(
												doc.getCharacterElement(
														start + i)
														.getAttributes());
										if (StyleConstants.getIcon(attrs) == null) {
											StyleConstants.setIcon(attrs,
													references.get(item));
											doc.remove(start + i, item.length());
											doc.insertString(start + i, item,
													attrs);
										}
										i = text.indexOf(item,
												i + item.length());
									}
								}
							} catch (BadLocationException e1) {
								e1.printStackTrace();
							}
						}
					}
				});
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
			}
		});
	}
}
