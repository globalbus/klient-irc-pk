package pk.klientIRC.MessageControl.plugins;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IMessageControl;
import pk.klientIRC.Contracts.IMessageControlFormatting;
import pk.klientIRC.Contracts.IMultithreader;

@Component
public class EmbedImage<input> implements IMessageControlFormatting {

	static String REGEX = "(((ht|f)tp(s?)\\://){1}\\S+)"; //$NON-NLS-1$
	Pattern patt;
	private IMultithreader multithread;
	@Autowired
	private EmbedImage(IMultithreader multithread) {
		this.multithread=multithread;
		patt = Pattern.compile(REGEX);

	}

	@Override
	public void initListener(final IMessageControl parent) {
		final JTextPane textpane = parent.getTextPane();

		textpane.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent event) {

				final DocumentEvent e = event;
				multithread.addToPool(new Runnable() {
					@Override
					public void run() {
						if (e.getDocument() instanceof StyledDocument) {
							try {
								StyledDocument doc = (StyledDocument) e
										.getDocument();
								int start = e.getOffset();
								String text = doc.getText(start, e.getLength());
								Matcher m = patt.matcher(text);
								while (m.find()) {
									String replace = m.group(1);
									int i = text.indexOf(replace);
									final SimpleAttributeSet attrs = new SimpleAttributeSet(
											doc.getCharacterElement(start + i)
													.getAttributes());
									if (StyleConstants.getIcon(attrs) == null) {
										String type = null;
										URL u = new URL(replace);
										URLConnection uc = null;
										uc = u.openConnection();
										type = uc.getContentType();
										ImageIcon image;
										if (type!=null && type.startsWith(
												"image")) { //$NON-NLS-1$
											image = getImage(uc
													.getInputStream());
											doc.remove(start + i,
													replace.length());
											StyleConstants
													.setIcon(attrs, image);
											doc.insertString(start+i, "\n", null);//new line //$NON-NLS-1$
											doc.insertString(start + i +1,	replace, attrs);

										}
									}
								}
							} catch (BadLocationException | IOException | IllegalArgumentException e1) {
								// e1.printStackTrace();
							}

						}
					}
				});
			}

			private ImageIcon getImage(InputStream input) throws IOException {
				BufferedImage image = ImageIO.read(input);
				return new ImageIcon(image);
				
			}

			@SuppressWarnings("unused")
			private BufferedImage resize(BufferedImage image, int height,
					int width) {
				BufferedImage resizedImage = new BufferedImage(width, height,
						BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = resizedImage.createGraphics();
				g.drawImage(image, 0, 0, width, height, null);
				g.dispose();
				return resizedImage;
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
			}
		});
	}

}
