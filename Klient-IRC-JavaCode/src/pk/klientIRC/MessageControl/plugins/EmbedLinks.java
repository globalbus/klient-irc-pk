package pk.klientIRC.MessageControl.plugins;

import java.awt.Desktop;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.springframework.beans.factory.annotation.Autowired;

import pk.klientIRC.Contracts.IMessageControl;
import pk.klientIRC.Contracts.IMessageControlFormatting;
import pk.klientIRC.Contracts.IMultithreader;

@org.springframework.stereotype.Component
public class EmbedLinks implements IMessageControlFormatting {
	static String REGEX = "((mailto\\:|(news|(ht|f)tp(s?))\\://){1}\\S+)"; //$NON-NLS-1$
	Pattern patt;
	private IMultithreader multithread;
	@Autowired
	private EmbedLinks(IMultithreader multithread) {
		this.multithread=multithread;
		patt = Pattern.compile(REGEX);

	}

	@Override
	public void initListener(final IMessageControl parent) {
		final JTextPane textpane = parent.getTextPane();
		textpane.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent event) {

				final DocumentEvent e = event;

				multithread.addToPool(new Runnable() {

					@Override
					public void run() {
						if (e.getDocument() instanceof StyledDocument) {
							try {
								final StyledDocument doc = (StyledDocument) e
										.getDocument();
								final int start = e.getOffset();
								String text = doc.getText(start, e.getLength());
								Matcher m = patt.matcher(text);
								while (m.find()) {
									final String replace = m.group(1);
									final int i = text.indexOf(replace);
									final SimpleAttributeSet attrs = new SimpleAttributeSet(
											doc.getCharacterElement(start + i)
													.getAttributes());
									if (StyleConstants.getComponent(attrs) == null) {
									final JLabel link = new JLabel(replace);
									link.addMouseListener(new MouseAdapter() {
										@Override
										public void mouseClicked(MouseEvent e) {
											if (e.getClickCount() > 0) {
												if (Desktop
														.isDesktopSupported()) {
													Desktop desktop = Desktop
															.getDesktop();
													try {
														URI uri = new URI(
																replace);
														desktop.browse(uri);

													} catch (IOException ex) {
														ex.printStackTrace();
													} catch (URISyntaxException ex) {
														ex.printStackTrace();
													} 
												}
											}
										}
									});
									StyleConstants.setComponent(attrs, link);
									doc.remove(start + i, replace.length());
									doc.insertString(start + i, replace, attrs);

								}}
							} catch (BadLocationException e1) {
								e1.printStackTrace();
							}

						}
					}
				});
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
			}
		});
	}

}
