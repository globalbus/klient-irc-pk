package pk.klientIRC.MessageControl.plugins;

import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IMessageControl;
import pk.klientIRC.Contracts.IMessageControlFormatting;
import pk.klientIRC.Contracts.IMultithreader;

@Component
public class EmbedCode implements IMessageControlFormatting{
	//support for pastebin.pl
	static String REGEX = "(((ht|f)tp(s?)\\://){0}\\S+pastebin\\S+)"; //$NON-NLS-1$
	Pattern patt;
	private IMultithreader multithread;
	@Autowired
	private EmbedCode(IMultithreader multithread) {
		this.multithread=multithread;
		patt = Pattern.compile(REGEX);

	}

	@Override
	public void initListener(final IMessageControl parent) {
		final StyledDocument document = parent.getStyledDocument();		
		document.addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent event) {

				final DocumentEvent e = event;
				multithread.addToPool(new Runnable() {
					@Override
					public void run() {
						if (e.getDocument() instanceof StyledDocument) {
							try {
								StyledDocument doc = (StyledDocument) e
										.getDocument();
								int start = e.getOffset();
								String text = doc.getText(start, e.getLength());
								Matcher m = patt.matcher(text);
								while (m.find()) {
									String replace = m.group(1);
									int i = text.indexOf(replace);
									final SimpleAttributeSet attrs = new SimpleAttributeSet(
											doc.getCharacterElement(start + i)
													.getAttributes());
										String type = null;
										URI download = new URI(replace);
										
										URL u = new URL("http://"+download.getHost() +"/pastebin.php?dl=" + download.getPath().substring(1)); //$NON-NLS-1$ //$NON-NLS-2$
										URLConnection uc = null;
										uc = u.openConnection();
										type = uc.getContentType();
										String downloaded = readText(uc.getInputStream());
										if (type!=null) {
											doc.remove(start + i,
													replace.length());
											doc.insertString(start + i,	"\n", null); //$NON-NLS-1$
											JTextArea paste = new JTextArea(downloaded);
											paste.setBackground(Color.BLACK);
											paste.setForeground(Color.white);
											paste.setWrapStyleWord(true);
											paste.setEditable(false);
											StyleConstants.setComponent(attrs, paste);
											doc.insertString(start + i +1,	downloaded, attrs);
									}
								}
							} catch (BadLocationException | IOException | URISyntaxException e1) {
								e1.printStackTrace();
							}

						}
					}
				});
			}
			public String readText(InputStream inputStream){
				char[] buffer = new char[1024];
				String output=""; //$NON-NLS-1$
				int i = 0;
				try (InputStreamReader reader = new InputStreamReader(inputStream)) {
					while (true) {
						i = reader.read(buffer);
						output+= String.copyValueOf(buffer, 0, i);
						if (i < 1024)
							break;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return output;
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
			}
		});
	}


}
