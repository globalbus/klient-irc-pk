package pk.klientIRC.Notifications;

import icons.IconLoader;

import java.awt.AWTException;
import java.awt.Dialog.ModalityType;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JDialog;
import javax.swing.JLabel;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;

import pk.klientIRC.Contracts.IConfigProvider;
import pk.klientIRC.Contracts.INotifyCondition;
import pk.klientIRC.Contracts.INotifyConfig;

import sounds.SoundsLoader;

@Aspect
public class MessageNotify {
	private INotifyConfig notifyConfig;

	@Autowired
	public MessageNotify(IConfigProvider configProvider) {
		SystemTray tray = SystemTray.getSystemTray();
		try {
			trayIcon = new TrayIcon(ImageIO.read(IconLoader.class
					.getResource("logo.png")), "KlientIRC"); //$NON-NLS-1$ //$NON-NLS-2$
			trayIcon.setImageAutoSize(true);
			tray.add(trayIcon); // dodanie naszej ikony do zasobnika
								// systemowego
		} catch (AWTException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		clips = new TreeMap<String, Clip>();
		notifyConfig = configProvider.getConfig(INotifyConfig.class);
		if (notifyConfig == null) {
			configProvider.loadDefaults();
			notifyConfig = configProvider.getConfig(INotifyConfig.class);
		}
		for (INotifyCondition condition : notifyConfig.getNotifyConditions()) {
			if (condition.isNotifiedBySound()) {
				try {
					audioStream = AudioSystem
							.getAudioInputStream(SoundsLoader.class
									.getResource(condition.getNotifySound()));
					clip = AudioSystem.getClip();
					clip.open(audioStream);
					clips.put(condition.getRegex(), clip);
				} catch (UnsupportedAudioFileException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (LineUnavailableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	Map<String, Clip> clips;

	@Pointcut("execution(* pk.klientIRC.MessageControl.MessageControl.addMessage(String, String))")
	public void getMessageWithNoArgument() {
	}

	private AudioInputStream audioStream;
	private Clip clip;

	private TrayIcon trayIcon;

	@Before("pk.klientIRC.Notifications.MessageNotify.getMessageWithNoArgument()")
	public void executeNotification(JoinPoint joinPoint) {

		String messageNick = (String) joinPoint.getArgs()[0];
		String messageText = (String) joinPoint.getArgs()[1];
		for (INotifyCondition condition : notifyConfig.getNotifyConditions()) {
			if (messageText.matches(condition.getRegex())) {
				if (condition.isNotifiedBySound()) {
					if (clip.isRunning())
						clip.stop();
					clip = clips.get(condition.getRegex());
					clip.setFramePosition(0);
					clip.start();
				}

				if (condition.isNotifiedByTrayBlink()) {
					if (condition.getPopupMessage() != null
							&& condition.getPopupMessage().equals("")) //$NON-NLS-1$
						trayIcon.displayMessage(null,
								condition.getPopupMessage(), MessageType.INFO);
					else
						trayIcon.displayMessage(messageNick, messageText,
								MessageType.INFO);
				}

				if (condition.isNotifiedByPopup()) {
					JDialog dialog = new JDialog();
					dialog.setModalityType(ModalityType.MODELESS);
					JLabel label = new JLabel();

					if (condition.getPopupMessage() != null
							&& condition.getPopupMessage().equals("")) {//$NON-NLS-1$
						dialog.setTitle(null);
						label.setText(condition.getPopupMessage());
						dialog.add(label);

					} else {
						dialog.setTitle(messageNick);
						label.setText(messageText);
						dialog.add(label);

					}
					dialog.setVisible(true);
				}

				break;
			}
		}

	}
}
