package pk.klientIRC;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import Archiwum.Archiwum.Odczyt;

import pk.klientIRC.Contracts.IBeanFactory;
import pk.klientIRC.Contracts.IMainFrame;
import pk.klientIRC.Contracts.IMessageControl;

/**
 * @author globalbus 
 * Bean Factory implementation, provides runtime beans
 *         Singleton
 */
public class BeanFactory implements IBeanFactory {
	private static final IBeanFactory instance;

	private BeanFactory() {
	}

	static {
		instance = new BeanFactory();
	}

	public static IBeanFactory getInstance() {
		return instance;
	}

	private ClassPathXmlApplicationContext container = new ClassPathXmlApplicationContext(
			"META-INF/spring/beans.xml"); //$NON-NLS-1$

	@Override
	public IMessageControl getMessageControl() {
		return (IMessageControl) container.getBean("messageControl"); //$NON-NLS-1$
		// hardcoded bean names, getting beans by interface causes really big
		// problems with AOP Proxy
	}

	@Override
	public IMainFrame getMainFrame() {
		return (IMainFrame) container.getBean("mainFrame"); //$NON-NLS-1$
	}

	@Override
	public Odczyt getArchiveFrame() {
		return (Odczyt) container.getBean("oknoArchiwum"); //$NON-NLS-1$
	}

}
