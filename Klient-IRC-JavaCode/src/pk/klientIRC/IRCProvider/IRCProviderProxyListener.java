package pk.klientIRC.IRCProvider;

import java.util.LinkedList;
import java.util.List;

import org.schwering.irc.lib.IRCConnection;
import org.schwering.irc.lib.IRCConstants;
import org.schwering.irc.lib.IRCEventListener;
import org.schwering.irc.lib.IRCModeParser;
import org.schwering.irc.lib.IRCUser;
import org.schwering.irc.lib.IRCUtil;

import pk.klientIRC.Contracts.IIRCEventOwnListener;
import pk.klientIRC.Contracts.IReceive;
import pk.klientIRC.Contracts.IServerConfiguration;

//Handle all IRCEvents - mostly pass-through
class IRCProviderProxyListener implements IRCEventListener {
	private IIRCEventOwnListener eventListener;// real event listener
	private IRCConnection connection;
	private IServerConfiguration configuration;

	IRCProviderProxyListener(IRCConnection connection,
			IServerConfiguration configuration2) {
		this.connection = connection;
		this.configuration = configuration2;
	}

	public void addListener(IReceive receive) {
		eventListener = receive;
	}

	@Override
	public void onRegistered() {
		eventListener.onRegistered();

	}

	@Override
	public void onDisconnected() {
		eventListener.onDisconnected();

	}

	@Override
	// handle internally/externally
	public void onError(String msg) {
		eventListener.onError(msg);
	}

	@Override
	// handle internally/externally
	public void onError(int num, String msg) {
		if (num == IRCConstants.ERR_NICKNAMEINUSE) {
			connection.doNick(configuration.getNickNext());
			
		}
		eventListener.onError(num, msg);

	}

	@Override
	public void onInvite(String chan, IRCUser user, String passiveNick) {
		eventListener.onInvite(chan, user.toString(), passiveNick);

	}

	@Override
	public void onJoin(String chan, IRCUser user) {
		eventListener.onJoin(chan, user.toString());

	}

	@Override
	public void onKick(String chan, IRCUser user, String passiveNick, String msg) {
		eventListener.onKick(chan, user.toString(), passiveNick, msg);

	}

	@Override
	public void onMode(String chan, IRCUser user, IRCModeParser modeParser) {
		eventListener.onMode(chan, user.toString(), modeParser);

	}

	@Override
	public void onMode(IRCUser user, String passiveNick, String mode) {
		eventListener.onMode(user.toString(), passiveNick, mode);

	}

	@Override
	public void onNick(IRCUser user, String newNick) {
		eventListener.onNick(user.toString(), newNick);

	}

	@Override
	public void onNotice(String target, IRCUser user, String msg) {
		eventListener.onNotice(target, user.toString(), msg);

	}

	@Override
	public void onPart(String chan, IRCUser user, String msg) {
		eventListener.onPart(chan, user.toString(), msg);
	}

	@Override
	// internal handling
	public void onPing(String ping) {
		connection.doPong(ping);
	}

	@Override
	public void onPrivmsg(String target, IRCUser user, String msg) {
		eventListener.onPrivmsg(target, user.toString(), msg);
	}

	@Override
	public void onQuit(IRCUser user, String msg) {
		eventListener.onQuit(user.toString(), msg);

	}

	@Override
	public void onReply(int num, String value, String msg) {
		if (num == IRCUtil.RPL_CHANNELMODEIS) { // mode <chan> -> channelmodes

		} else if (num == IRCUtil.RPL_BANLIST) { // who -> add nicks to nicklist


		} else if (num == IRCUtil.RPL_NAMREPLY) { // who -> add nicks to
													// nicklist
			
			List<String> list = new LinkedList<String>();
			String[] it =msg.split(" "); //$NON-NLS-1$
			
			for(int i=0;i<it.length;i++)
			{
				list.add(it[i]);
			}
			eventListener.OnRegisterNicks(value.split(" ")[2], list); //$NON-NLS-1$

		} else if (num == IRCUtil.RPL_TOPIC) { // on-join topic
			String[] it =value.split(" "); //$NON-NLS-1$
			 eventListener.onTopic(it[1], msg);

		} else if (num == IRCUtil.RPL_AUTHNAME || num == IRCUtil.RPL_WHOISUSER
				|| num == IRCUtil.RPL_WHOISOPERATOR
				|| num == IRCUtil.RPL_WHOISSERVER
				|| num == IRCUtil.RPL_WHOISCHANNELS || num == IRCUtil.RPL_AWAY
				|| num == IRCUtil.RPL_WHOISIDLE
				|| num == IRCUtil.RPL_ENDOFWHOIS) {
			eventListener.onMessage(msg);
//		} else if (num == IRCUtil.RPL_NOWAWAY || num == IRCUtil.RPL_UNAWAY) { // away&back


		} else
			eventListener.onReply(num, value, msg);

	}

	@Override
	public void onTopic(String chan, IRCUser user, String topic) {
		eventListener.onTopicChange(chan, user.toString(), topic);

	}

	@Override
	public void unknown(String prefix, String command, String middle,
			String trailing) {
		eventListener.unknown(prefix, command, middle, trailing);

	}
}
