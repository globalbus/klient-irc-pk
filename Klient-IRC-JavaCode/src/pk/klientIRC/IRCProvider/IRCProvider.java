package pk.klientIRC.IRCProvider;

import java.io.IOException;

import org.schwering.irc.lib.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IConfigProvider;
import pk.klientIRC.Contracts.IIRCProvider;
import pk.klientIRC.Contracts.IReceive;
import pk.klientIRC.Contracts.IServerConfiguration;

//adapter for external library
@Component
class IRCProvider implements IIRCProvider {
	private IRCConnection connection;
	private IReceive eventListener;
	private IRCProviderProxyListener proxy;
	private IConfigProvider config;

	@Autowired
	private IRCProvider(IConfigProvider config) {
		this.config = config;
	}

	@Override
	public void connect() throws IOException {
		IServerConfiguration configuration = config.getConfig(IServerConfiguration.class);
		if(configuration==null){
			config.loadDefaults();
			configuration = config.getConfig(IServerConfiguration.class);
		}
		connection = new IRCConnection(configuration.getServerAddress(),
				configuration.getServerPort(),
				configuration.getServerPort() + 3, null,
				configuration.getNickNext(), configuration.getUserName(),
				configuration.getRealName());
		connection.setDaemon(true);
		connection.setColors(false);// TODO - move to configuration
		connection.setPong(true);// Ping autorespond
		connection.setEncoding(configuration.getEncoding());
		proxy = new IRCProviderProxyListener(connection, configuration);
		proxy.addListener(eventListener);
		connection.addIRCEventListener(proxy);// IRCProvider is also a basic
												// listener to moving calls to
												// other interface
		connection.connect();
	}

	@Override
	public void addListener(IReceive receive) {
		this.eventListener = receive;
	}

	@SuppressWarnings("nls")
	@Override
	public void sendRaw(String text) {
		text = text.trim();

		if (text.startsWith("/")) // it's a command
		{
			String[] splitted = text.split(" ");
			if (splitted[0].equalsIgnoreCase(("/JOIN"))) {
				if (splitted.length == 2)
					this.joinChannel(splitted[1]);
				else if (splitted.length == 3)
					connection.doJoin(splitted[1], splitted[2]);
			} else if (splitted[0].equalsIgnoreCase(("/MSG"))) {
				String message = "";
				for (int i = 2; i < splitted.length; i++)
					message += splitted[i] + " ";
				doMsg(splitted[1], message);
			} else if (splitted[0].equalsIgnoreCase(("/WHO"))) {
				if (splitted.length == 2)
					connection.doWhois(splitted[1]);
			} else if (splitted[0].equalsIgnoreCase(("/NICK"))) {
				if (splitted.length == 2)
					connection.doNick(splitted[1]);
			} else if (splitted[0].equalsIgnoreCase(("/QUIT"))) {
				if (splitted.length == 1)
					connection.doQuit();
				else if (splitted.length == 2)
					connection.doQuit(splitted[1]);
			} else if (splitted[0].equalsIgnoreCase(("/AWAY"))) {
				if (splitted.length == 1)
					connection.doAway();
				else if (splitted.length == 2)
					connection.doAway(splitted[1]);
			} else if (splitted[0].equalsIgnoreCase(("/Attach")))
				try {
					this.connect();
				} catch (IOException e) {
					proxy.onError(e.getLocalizedMessage());
				}
			else if (splitted[0].equalsIgnoreCase(("/ChanServ"))) {
				String message = "";
				for (int i = 1; i < splitted.length; i++)
					message += splitted[i] + " ";
				doMsg("ChanServ", message);
			} else if (splitted[0].equalsIgnoreCase(("/NickServ"))) {
				String message = "";
				for (int i = 1; i < splitted.length; i++)
					message += splitted[i] + " ";
				doMsg("NickServ", message);
			}
			else if(splitted[0].equalsIgnoreCase(("/PART"))){
				if (splitted.length == 2)
					connection.doPart(splitted[1]);
				else {
					String message = "";
					for (int i = 2; i < splitted.length; i++)
						message += splitted[i] + " ";
					connection.doPart(splitted[1], message);
				}
			}
		}

		else
			doMsg(eventListener.getContext(), text);
	}

	public void doMsg(String chan, String text) {
		connection.doPrivmsg(chan, text);
		proxy.onPrivmsg(chan, getCurrentIRCUser(), text);
	}

	@Override
	public boolean isConnected() {
		return connection.isConnected();
	}

	@Override
	public void disconnect() {
		connection.close();
	}

	@Override
	public String getCurrentNick() {
		return connection.getNick();
	}

	public IRCUser getCurrentIRCUser() {
		return new IRCUser(connection.getNick(), connection.getUsername(),
				connection.getHost());
	}

	@Override
	public void doQuit() {
		connection.doQuit(config.getConfig(IServerConfiguration.class).getQuitMsg());
	}

	@Override
	public void joinChannel(String chan) {
		connection.doJoin(chan);

	}

	@Override
	public void changeNick(String val) {
		connection.doNick(val);

	}

	@Override
	public void whoIs() {
		connection.doWhois(eventListener.getContext());
	}
	
	@Override
	public void whoIs(String nick) {
		connection.doWhois(nick);
	}

}
