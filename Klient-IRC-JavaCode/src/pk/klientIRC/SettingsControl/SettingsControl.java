package pk.klientIRC.SettingsControl;

import icons.IconLoader;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.ISettingControl;
import pk.klientIRC.Contracts.ISettingsControlTab;
import pk.klientIRC.Localization.Messages;

@Component
class SettingsControl extends JDialog implements ISettingControl{
	JDialog thisReference =this;
	private static final long serialVersionUID = 1L;
	@Autowired(required=false)
	private SettingsControl(final List<ISettingsControlTab> settingsTab){
		this.setBounds(100,100,400,400);
		try {
			this.setIconImage(ImageIO.read(IconLoader.class.getResource("logo.png"))); //$NON-NLS-1$
		} catch (IOException e1) {
		}
		this.setLayout(new BorderLayout());
		JTabbedPane tabs = new JTabbedPane();
		for(ISettingsControlTab item : settingsTab)
		{
			tabs.add(item.getName(), item.getPanel());
		}
		this.add(tabs, BorderLayout.CENTER);
		JPanel buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		JButton buttonOk = new JButton(Messages.getString("SettingsControl.OKButton")); //$NON-NLS-1$
		buttonOk.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				for(ISettingsControlTab item : settingsTab)
					item.validate();
				
				JOptionPane.showMessageDialog(thisReference, Messages.getString("SettingsControl.RestartPrompt")); //$NON-NLS-1$
				thisReference.setVisible(false);
			}
			
		});
		JButton buttonCancel = new JButton(Messages.getString("SettingsControl.CancelButton")); //$NON-NLS-1$
		buttonCancel.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				thisReference.setVisible(false);			
			}
			
		});
		buttons.add(buttonOk);
		buttons.add(buttonCancel);
		this.add(buttons, BorderLayout.SOUTH);
	}
	@Override
	public JDialog getDialog() {
		return this;
	}

}
