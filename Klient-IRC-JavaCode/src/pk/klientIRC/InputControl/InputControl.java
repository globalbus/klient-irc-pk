package pk.klientIRC.InputControl;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import Checker.Checker.ISpellCheck;

import pk.klientIRC.Contracts.IInputControl;
import pk.klientIRC.Contracts.ISend;
import pk.klientIRC.Localization.Messages;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Visual Component to input commands.
 * 
 * @author globalbus
 * 
 */
@Component
class InputControl extends JPanel implements IInputControl {
	private static final long serialVersionUID = 1L;
	private ISend send;
	private JTextPane textpane;
	private JButton button;

	private LinkedList<String> memory;
	private int currentMemoryElement;// it's a thread safe method to keep
	private DefaultStyledDocument styledDocument;
	private Style wrongStyle;
	private ISpellCheck check;
	private Style defaultStyle;

	// current element of memory list
	@Autowired
	private InputControl(ISend send, ISpellCheck check) {
		this.check=check;
		this.send = send;
		memory = new LinkedList<String>();
		currentMemoryElement = 0;
		addBindings();

	}

	private void addBindings() {
		textpane = new JTextPane();
		textpane.setEditable(true);
		styledDocument = new DefaultStyledDocument();
		textpane.setStyledDocument(styledDocument);
		styledDocument.addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void insertUpdate(final DocumentEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						if (e.getDocument() instanceof StyledDocument) {
							try {
								StyledDocument doc = (StyledDocument) e
										.getDocument();
								if (e.getLength() == 1) { // for writing
									int start = e.getOffset();
									String text = doc.getText(start,
											e.getLength());
									if (text.equals(" ")) { //$NON-NLS-1$
										checkDocument();
									}
								}
							} catch (BadLocationException e1) {
								e1.printStackTrace();
							}
						}
					}
				});

			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				// TODO Auto-generated method stub

			}

		});
		button = new JButton();
		button.setText(Messages.getString("Send.ButtonSendName")); //$NON-NLS-1$
		button.setHorizontalAlignment(SwingConstants.RIGHT);
		button.setAction(new EnterAction());
		setLayout(new BorderLayout(0, 0));
		this.add(textpane);
		this.add(button, BorderLayout.EAST);
		InputMap inputMap = textpane.getInputMap();
		KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,
				Event.SHIFT_MASK);
		inputMap.put(key, DefaultEditorKit.insertBreakAction);
		key = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
		inputMap.put(key, new EnterAction());
		key = KeyStroke.getKeyStroke(KeyEvent.VK_UP, Event.SHIFT_MASK);// map
																		// keys
		inputMap.put(key, new UpAction());
		key = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, Event.SHIFT_MASK);
		inputMap.put(key, new DownAction());
		
		defaultStyle = styledDocument.getLogicalStyle(0);
		wrongStyle = styledDocument.addStyle("WRONG", null); //$NON-NLS-1$
		StyleConstants.setBold(wrongStyle, true);
		StyleConstants.setForeground(wrongStyle, Color.RED);
	}

	private void checkDocument(){
		try {
			String[] words = styledDocument.getText(0, styledDocument.getLength()).split(" "); //$NON-NLS-1$
			int counter = 0;
			for(int i=0;i<words.length;i++)
			{
				if(!check.checkWord(words[i]))
				{
					//if(!styledDocument.getLogicalStyle(counter).equals(wrongStyle))
						styledDocument.replace(counter, words[i].length(), words[i], wrongStyle);
				}
				else
					//if(styledDocument.getLogicalStyle(counter).equals(wrongStyle))
						styledDocument.replace(counter, words[i].length(), words[i], defaultStyle);
				counter+=words[i].length()+1;
			}
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
	}
	class EnterAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public EnterAction() {
			putValue(Action.NAME, "Send"); //$NON-NLS-1$
			putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
			putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_ENTER));
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			pushMessage();
		}
	}

	class UpAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public UpAction() {
			putValue(Action.NAME, "Previous"); //$NON-NLS-1$
			putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
			putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_UP));
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			toOlderMessage();

		}
	}

	class DownAction extends AbstractAction {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public DownAction() {
			putValue(Action.NAME, "Next"); //$NON-NLS-1$
			putValue(Action.SHORT_DESCRIPTION, getValue(Action.NAME));
			putValue(Action.MNEMONIC_KEY, new Integer(KeyEvent.VK_DOWN));
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			toEarlierMessage();
		}
	}

	@Override
	public void pushMessage() {
		send.pushMessage(textpane.getText());
		memory.addFirst(textpane.getText());
		currentMemoryElement = 0;
		textpane.setText(""); //$NON-NLS-1$
	}

	/**
	 * Retrieves older message
	 * 
	 */
	private void toOlderMessage() {
		ListIterator<String> memoryIterator = memory
				.listIterator(currentMemoryElement);
		if (memoryIterator.hasNext()) {
			textpane.setText(memoryIterator.next());
			currentMemoryElement++;
		} else {
			textpane.setText(""); //$NON-NLS-1$
		}
	}

	/**
	 * Retrieves earlier message
	 * 
	 */
	private void toEarlierMessage() {
		ListIterator<String> memoryIterator = memory
				.listIterator(currentMemoryElement);
		if (memoryIterator.hasPrevious()) {
			textpane.setText(memoryIterator.previous());
			currentMemoryElement--;
		} else
			textpane.setText(""); //$NON-NLS-1$
	}

	@Override
	public JPanel getPanel() {
		return this;
	}
}
