package pk.klientIRC.Send;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IIRCProvider;
import pk.klientIRC.Contracts.ISend;

/**
 * @author globalbus Use it as Singleton
 */
@Component
class Send implements  ISend {

	/**
	 * 
	 */
	private IIRCProvider connection;	

	@Autowired
	private Send(IIRCProvider provider) {
		connection = provider;
	}

	@Override
	public void pushMessage(String text) {
		connection.sendRaw(text);
	}

}
