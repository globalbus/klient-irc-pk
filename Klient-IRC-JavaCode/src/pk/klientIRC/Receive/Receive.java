package pk.klientIRC.Receive;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.schwering.irc.lib.IRCModeParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IIRCProvider;
import pk.klientIRC.Contracts.IMessageControl;
import pk.klientIRC.Contracts.IMessageControlHandler;
import pk.klientIRC.Contracts.IMessagePanelFacade;
import pk.klientIRC.Contracts.IReceive;
import pk.klientIRC.Localization.Messages;

/**
 * @author globalbus Use it as Singleton
 */
@Component
class Receive implements IReceive, ActionListener {
	private JPanel contentPanel;
	private Map<String, IMessagePanelFacade> trash;
	private IMessageControl console;
	private JTabbedPane tabs;
	private IIRCProvider provider;
	private Map<String, IMessageControlHandler> handlers = new HashMap<String, IMessageControlHandler>();
	private IMessagePanelFacade prototype;
	private JPopupMenu tabPopup;
	private JMenuItem pmClose, pmClear;

	@Autowired(required = false)
	private void setHandlers(List<IMessageControlHandler> handlers) {
		for (IMessageControlHandler handler : handlers) {
			this.handlers.put(handler.getHandlerName().toLowerCase(), handler);
		}
	}

	@Autowired
	private Receive(IIRCProvider provider, IMessagePanelFacade control) {
		contentPanel = new JPanel();
		this.provider = provider;
		provider.addListener(this);
		trash = new HashMap<String, IMessagePanelFacade>();
		tabs = new JTabbedPane();
		tabPopup = new JPopupMenu();
		pmClose = new JMenuItem(Messages.getString("Receive.Close")); //$NON-NLS-1$
		pmClose.addActionListener(this);
		pmClear = new JMenuItem(Messages.getString("Receive.Clear")); //$NON-NLS-1$
		pmClear.addActionListener(this);
		tabPopup.add(pmClose);
		tabPopup.add(pmClear);
		//ugly workaround for right click on tab
		final MouseListener m = tabs.getMouseListeners()[0];
		tabs.removeMouseListener(m);
		tabs.addMouseListener( new MouseAdapter()
		{
			@Override
			public void mousePressed(MouseEvent e)
			{
				if ( SwingUtilities.isRightMouseButton(e) )
				{
					e= new MouseEvent(e.getComponent(), e.getID() ,e.getWhen(), e.getModifiers(), e.getX(), e.getY(), e.getXOnScreen(),e.getYOnScreen(), e.getClickCount(), e.isPopupTrigger(), MouseEvent.BUTTON1);
					tabPopup.show(tabs, e.getX(), e.getY()); //and show the menu
				}
					m.mousePressed(e);
			}
		});
		tabs.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent arg0) {
				tabs.setForegroundAt(tabs.getSelectedIndex(), null);//set default color
			}
			
		});
		prototype = control;
		console = control.getMessageControl();
		prototype.setChanPanel();
		// tab.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		tabs.addTab(
				Messages.getString("Receive.ConsoleTabName"), control.getPanel()); //$NON-NLS-1$
		contentPanel.setLayout(new BorderLayout());
		contentPanel.add(tabs);
	}

	private void createTab(String target) {
		IMessagePanelFacade tempPanel = prototype.getInstance();
		trash.put(target.toLowerCase(), tempPanel);
		tabs.addTab(target, tempPanel.getPanel());
		tabs.setSelectedIndex(tabs.getTabCount() - 1);
		tempPanel.getMessageControl().setName(target);
	}

	@Override
	public String getContext() {
		// to sending messages
		return tabs.getTitleAt(tabs.getSelectedIndex());
	}

	private IMessagePanelFacade getMessagePanel(String value) {
		String valueLower = value.toLowerCase();
		if (!trash.containsKey(valueLower)) {
			createTab(value);
		}
		return trash.get(valueLower);
	}

	private IMessageControl getMessageControl(String value) {
		String valueLower = value.toLowerCase();
		if (handlers.containsKey(valueLower)) {
			IMessageControlHandler handler = handlers.get(valueLower);
			if (!handler.isEnabledHandler()) {
				tabs.addTab(handler.getHandlerName(), handler.getPanel());
				handler.setEnabledHandler(true);
			}
			return handler;
		}
		if (!trash.containsKey(valueLower)) {
			createTab(value);
		}
		return trash.get(valueLower).getMessageControl();
	}

	@Override
	public JPanel getPanel() {
		return contentPanel;
	}

	public void onConnect() {
		sendToConsole(Messages.getString("Receive.MessageConnected")); //$NON-NLS-1$
	}

	@Override
	public void onDisconnected() {

		sendToConsole(Messages.getString("Receive.MessageDisconnected")); //$NON-NLS-1$
	}

	@Override
	public void onError(int num, String msg) {
		String message = String.format(
				Messages.getString("Receive.ErrorNum"), num, msg); //$NON-NLS-1$
		sendToConsole(message);
	}

	@Override
	public void onError(String msg) {
		String message = String
				.format(Messages.getString("Receive.Error"), msg); //$NON-NLS-1$
		sendToConsole(message);
	}

	@Override
	public void onInvite(String chan, String user, String nickPass) {
		String message = String.format(
				Messages.getString("Receive.Invite"), user, nickPass, chan); //$NON-NLS-1$
		sendToConsole(message);
		// TODO show dialog
	}

	@Override
	public void onJoin(String chan, String user) {
		if (user.equals(provider.getCurrentNick()))
			createTab(chan);
		else {
			getMessagePanel(chan).getNickList().addNick(user);
			String message = String.format(
					Messages.getString("Receive.Joins"), user, chan); //$NON-NLS-1$
			getMessagePanel(chan).getMessageControl().addNickChangeMsg(message);
		}
		// add the nickname to the nickname-table
	}

	@Override
	public void onKick(String chan, String user, String nickPass, String msg) {
		String message = String.format(
				Messages.getString("Receive.Kicks"), user, nickPass, msg); //$NON-NLS-1$
		getMessagePanel(user).getMessageControl().addMessage(message);
		sendToConsole(message);
		getMessagePanel(chan).getNickList().removeNick(nickPass);
		// remove the nickname from the nickname-table
	}

	@Override
	public void onMode(String chan, String user, IRCModeParser modeParser) {
		String message = String.format(
				Messages.getString("Receive.Mode"), user, chan); //$NON-NLS-1$
		sendToConsole(message);
		getMessagePanel(chan).getMessageControl().addMessage(message);
		// some operations with the modes
	}

	@Override
	public void onMode(String user, String passiveNick, String mode) {
		String message = String
				.format(Messages.getString("Receive.ModChange"), user, passiveNick, mode); //$NON-NLS-1$
		sendToConsole(message);
	}

	@Override
	public void onNick(String user, String nickNew) {
		String message = String.format(
				Messages.getString("Receive.NickChange"), user, //$NON-NLS-1$
				nickNew);
		for (IMessagePanelFacade item : trash.values())// sadly, we need to
														// change nick
		// in all active conversations
		{
			if (item.getNickList().replaceNick(user, nickNew))
				item.getMessageControl().addMessage(message);
		}
		// update the nickname in the nickname-table
	}

	@Override
	public void onNotice(String target, String user, String msg) {
		if (target.equals(provider.getCurrentNick()))
			getMessageControl(user).addMessage(msg);
		else
			sendToConsole(String.format(
					Messages.getString("Receive.Notice"), user, //$NON-NLS-1$
					target, msg));

	}

	@Override
	public void onPart(String chan, String user, String msg) {
		if (!user.equals(provider.getCurrentNick())) {
			String message = String.format(
					Messages.getString("Receive.parts"), user, chan, //$NON-NLS-1$
					msg);
			if (getMessagePanel(chan).getNickList().removeNick(user))
				getMessagePanel(chan).getMessageControl().addNickChangeMsg(
						message);
		}
		// remove the nickname from the nickname-table
	}

	@Override
	public void onPrivmsg(String target, String user, String msg) {
		if (!target.equals(provider.getCurrentNick()))
		{
			getMessageControl(target).addMessage(user, msg);
			if(getContext()!=target)
				tabs.setForegroundAt(tabs.indexOfTab(target), Color.RED);
		}
		else
		{
			getMessageControl(user).addMessage(user, msg);
			if(getContext()!=user)
				tabs.setForegroundAt(tabs.indexOfTab(user), Color.RED);
		}
		
	}

	@Override
	public void onQuit(String user, String msg) {
		String message = String.format(
				Messages.getString("Receive.quits"), user, msg); //$NON-NLS-1$
		for (IMessagePanelFacade item : trash.values())// sadly, we need to
														// remove nick
		// from all active conversations
		{
			if (item.getNickList().removeNick(user))
				item.getMessageControl().addMessage(message);
		}
	}

	@Override
	public void onRegistered() {
		sendToConsole(Messages.getString("Receive.MessageRegistered")); //$NON-NLS-1$

	}

	@Override
	public void OnRegisterNicks(String chan, List<String> list) {
		getMessagePanel(chan).setNickList(list);
	}

	@Override
	public void onReply(int num, String value, String msg) {
		String message = String.format(
				Messages.getString("Receive.Reply"), num, msg, value); //$NON-NLS-1$
		sendToConsole(message);
	}

	@Override
	public void onTopic(String chan, String topic) {
		String message = String.format(
				Messages.getString("Receive.Topic"), topic); //$NON-NLS-1$
		getMessagePanel(chan).getMessageControl().addTopicMsg(message);
	}

	@Override
	public void onTopicChange(String chan, String user, String topic) {
		String message = String.format(
				Messages.getString("Receive.TopicChange"), user, topic); //$NON-NLS-1$
		getMessagePanel(chan).getMessageControl().addTopicMsg(message);
	}

	private void sendToConsole(String text) {
		System.out.println(text);
		if (console != null)
			console.addMessage(text);
	}

	@Override
	public void unknown(String prefix, String command, String middle,
			String trailing) {
		String message = String
				.format(Messages.getString("Receive.Unknown"), prefix, command, middle, trailing); //$NON-NLS-1$
		sendToConsole(message);

	}

	@Override
	public void onMessage(String msg) {
		String target = getContext();
		getMessageControl(target).addMessage(msg);

	}

	@Override
	public synchronized void actionPerformed(ActionEvent e) {
		java.awt.Component selected = tabs.getSelectedComponent();
		if (e.getSource() == pmClose) {

			if (selected != prototype)// we can't close console tab
			{

				if (handlers.containsKey(getContext().toLowerCase())) {
					handlers.get(getContext().toLowerCase()).clear();
					handlers.get(getContext().toLowerCase()).setEnabledHandler(
							false);
				} else if (trash.containsKey(getContext().toLowerCase())) {
					trash.remove(getContext().toLowerCase());
					provider.sendRaw("/part " + getContext()); //$NON-NLS-1$
				}
				tabs.remove(selected);
			}
		} else if (e.getSource() == pmClear) {
			try {
				((IMessagePanelFacade) selected).getMessageControl().clear();
			} catch (ClassCastException ex) {
				((IMessageControl) selected).clear();
			}
		}
	}

}