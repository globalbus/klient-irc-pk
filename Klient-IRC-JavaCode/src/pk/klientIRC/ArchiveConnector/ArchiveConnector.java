package pk.klientIRC.ArchiveConnector;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;

import pk.klientIRC.Contracts.IMessageControl;
@Aspect
class ArchiveConnector {

	Archiwum.Archiwum.Archiwizacja worker;
	@Autowired
	public ArchiveConnector(Archiwum.Archiwum.Archiwizacja worker){
		this.worker = worker;
		System.out.println("Archive Connected"); //$NON-NLS-1$
	}
	@Pointcut("execution(* pk.klientIRC.MessageControl.MessageControl.addMessage(String, String))")
	public void getMessageWithNoArgument() {
	}

	@Before("pk.klientIRC.ArchiveConnector.ArchiveConnector.getMessageWithNoArgument()")
	public void addToArchive(JoinPoint joinPoint) {
		String[] args = new String[]{ (String) joinPoint.getArgs()[0], (String) joinPoint.getArgs()[1]};
		
		worker.dodajDoArchiwum(((IMessageControl)joinPoint.getTarget()).getName(), args[0]+": "+args[1]); //$NON-NLS-1$
	}
}
