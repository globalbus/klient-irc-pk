package pk.klientIRC.ConfigProvider;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IConfigProvider;
import pk.klientIRC.Contracts.IConfigProviderDefaults;
import pk.klientIRC.Localization.Messages;

/**
 * @author globalbus
 * 
 */
@Component
class ConfigProvider implements IConfigProvider {
	private Map<String, Object> configs=new HashMap<String,Object>();
	private List<IConfigProviderDefaults> defaults;
	@Autowired(required=false)
	private ConfigProvider(List <IConfigProviderDefaults> defaults){
		this.defaults=defaults;

		configs = loadSettings();
	}
	private ConfigProvider(Map<String, Object> configs, Map<String, Object> configs2) {
		configs.putAll(configs2);
		this.configs=configs;
	}
	/**
	 * @return ConfigProvider instance from file
	 */
	@SuppressWarnings("unchecked")
	private Map<String, Object> loadSettings() {
		Map<String, Object> temp = null;
		try (XMLDecoder reader = new XMLDecoder(new BufferedInputStream(
				new FileInputStream(
						Messages.getString("ConfigProvider.ConfigFile"))))) { //$NON-NLS-1$
			;
			temp = (Map<String, Object>) reader.readObject();
		} catch (FileNotFoundException e) {
			System.out.println(e.getLocalizedMessage());
		}
		if(temp==null)
			temp=new HashMap<String,Object>();
		return temp;
	}
	@Override
	public void loadDefaults(){
		configs=new HashMap<>();
		for(IConfigProviderDefaults item : defaults)
			this.putConfig(item.Initialize());
			
	}
	/** Saving instance to file
	 * 
	 */
	private void saveSettings() {

		try (XMLEncoder writer = new XMLEncoder(new BufferedOutputStream(
				new FileOutputStream(
						Messages.getString("ConfigProvider.ConfigFile"))))) { //$NON-NLS-1$
			writer.writeObject(configs);
		} catch (FileNotFoundException e) {
			System.out.println(e.getLocalizedMessage());
		}

	}

	/** Getting new instance for modify (in example in graphical window) without changes in this singleton
	 * @see pk.klientIRC.Contracts.IConfigProvider#getNewInstance()
	 */
	@Override
	public IConfigProvider getNewInstance() {
		return new ConfigProvider(loadSettings(), configs);
	}


	/**
	 * @param instance to set as singleton instance
	 */
	@Override
	public void setInstance(IConfigProvider instance) {
		((ConfigProvider) instance).saveSettings();
		configs = loadSettings();
	}

	/**
	 * @param object to register
	 */
	@Override
	public void putConfig(Object obj) {
		Class<?> clazz = obj.getClass();
		if (!clazz.isInterface())
			for (Class<?> item : clazz.getInterfaces())
				 configs.put(item.getName(), obj);
	}
	/** getting registered object by providing interface class
	 * @see pk.klientIRC.Contracts.IConfigProvider#getConfig(java.lang.Class)
	 * @return registered object or null if nothing found
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getConfig(Class<? extends T> clazz) {
		T result = null;
		try {
			if(clazz.isInterface())
				result = (T) configs.get(clazz.getName());

		} catch (ClassCastException e) {

		}
		return result;
	}

}
