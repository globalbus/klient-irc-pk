package pk.klientIRC.ConfigProvider.Configs;

import pk.klientIRC.Contracts.INotifyCondition;

public class NotifyCondition implements INotifyCondition{

	private String regex;
	private boolean notifiedBySound;
	private String notifySound;
	private boolean notifiedByTrayBlink;
	private boolean notifiedByPopup;
	private String popupMessage;

	@Override
	public String getRegex() {
		return regex;
	}

	@Override
	public void setRegex(String regex) {
		this.regex=regex;
	}

	@Override
	public boolean isNotifiedBySound() {
		return notifiedBySound;
	}

	@Override
	public void setNotifiedBySound(boolean notifiedBySound) {
		this.notifiedBySound=notifiedBySound;
		
	}

	@Override
	public String getNotifySound() {
		return notifySound;
	}

	@Override
	public void setNotifySound(String notifySound) {
		this.notifySound=notifySound;
		
	}

	@Override
	public boolean isNotifiedByTrayBlink() {
		return notifiedByTrayBlink;
	}

	@Override
	public void setNotifiedByTrayBlink(boolean notifiedByTrayBlink) {
		this.notifiedByTrayBlink=notifiedByTrayBlink;
		
	}

	@Override
	public boolean isNotifiedByPopup() {
		return notifiedByPopup;
	}

	@Override
	public void setNotifiedByPopup(boolean notifiedByPopup) {
		this.notifiedByPopup=notifiedByPopup;
	}

	@Override
	public String getPopupMessage() {
		return popupMessage;
	}

	@Override
	public void setPopupMessage(String popupMessage) {
		this.popupMessage=popupMessage;
		
	}
 @Override
 public String toString(){
	 return regex + " " + popupMessage; //$NON-NLS-1$
 }
	public NotifyCondition(String regex, boolean notifiedBySound,
			String notifySound, boolean notifiedByTrayBlink,
			boolean notifiedByPopup, String popupMessage) {
		this.regex = regex;
		this.notifiedBySound = notifiedBySound;
		this.notifySound = notifySound;
		this.notifiedByTrayBlink = notifiedByTrayBlink;
		this.notifiedByPopup = notifiedByPopup;
		this.popupMessage = popupMessage;
	}
	public NotifyCondition() {
		// TODO Auto-generated constructor stub
	}

}