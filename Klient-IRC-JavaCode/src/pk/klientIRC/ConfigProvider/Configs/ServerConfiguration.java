package pk.klientIRC.ConfigProvider.Configs;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IConfigProviderDefaults;
import pk.klientIRC.Contracts.IServerConfiguration;

/**Structure to keep single server connection settings
 * @author globalbus 
 * 
 */
@Component
public class ServerConfiguration implements IServerConfiguration, IConfigProviderDefaults{
	private String serverAddress;
	private int serverPort;
	private Set<String> availableNicks;
	private String userName;
	private String realName;
	private String quitMsg;
	private Iterator<String> nickIterator;
	private String encoding;
	public ServerConfiguration(){
	}
	private ServerConfiguration(String serverAddress, int serverPort,
			Set<String> availableNicks, String userName, String realName, String quitMsg, String encoding) {
		this.serverAddress = serverAddress;
		this.serverPort = serverPort;
		this.availableNicks = availableNicks;
		this.userName = userName;
		this.realName = realName;
		this.quitMsg=quitMsg;
		this.encoding=encoding;
		nickIterator = this.availableNicks.iterator();
	}

	@Override
	public String getServerAddress() {
		return serverAddress;
	}

	@Override
	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}

	@Override
	public int getServerPort() {
		return serverPort;
	}

	@Override
	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	@Override
	public Set<String> getAvailableNicks() {
		return availableNicks;
	}

	@Override
	public void setAvailableNicks(Set<String> availableNicks) {
		this.availableNicks = availableNicks;
	}
	@Override
	public String getNickNext() {
		if(nickIterator==null)
			nickIterator=availableNicks.iterator(); //this initialization must be lazy, because will make problems at serialization
		if(nickIterator.hasNext())
			return nickIterator.next();
		return String.format("nick%d", (new Random()).nextInt(Integer.MAX_VALUE)); //$NON-NLS-1$
	}


	@Override
	public String getUserName() {
		return userName;
	}

	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
	public String getEncoding() {
		return encoding;
	}

	@Override
	public void setEncoding(String encoding) {
		this.encoding= encoding;
	}
	
	@Override
	public String getRealName() {
		return realName;
	}

	@Override
	public void setRealName(String realName) {
		this.realName = realName;
	}
	@Override
	public String getQuitMsg() {
		return quitMsg;
	}
	@Override
	public void setQuitMsg(String quitMsg) {
		this.quitMsg = quitMsg;
	}
	@Override
	public Object Initialize() {
		Set<String> nicks = new HashSet<String>();
		nicks.add("nickgsdfgsdf"); //$NON-NLS-1$
		nicks.add("nickasdtert"); //$NON-NLS-1$
		IServerConfiguration serverConfiguration = new ServerConfiguration(
				"irc.eu.freenode.net", 6666, nicks, "username", "realname", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				"Quit...", "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$
		return serverConfiguration;
	}

}
