package pk.klientIRC.ConfigProvider.Configs;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IConfigProviderDefaults;
import pk.klientIRC.Contracts.INotifyCondition;
import pk.klientIRC.Contracts.INotifyConfig;
@Component
public class NotifyConfig implements INotifyConfig, IConfigProviderDefaults {

	private Set<INotifyCondition> notifyConditions;
	
	@Override
	public Set<INotifyCondition> getNotifyConditions() {
		return notifyConditions;
	}

	@Override
	public void setNotifyConditions(Set<INotifyCondition> notifyConditions) {
		this.notifyConditions = notifyConditions;

	}
	@Override
	public Object Initialize() {
		NotifyConfig sample = new NotifyConfig();
		notifyConditions= new HashSet<INotifyCondition>();
		INotifyCondition condition = fabricNotifyCondition();
		condition.setRegex(".*"); //$NON-NLS-1$
		condition.setNotifySound("beam2.wav"); //$NON-NLS-1$
		condition.setNotifiedBySound(true);
		condition.setNotifiedByPopup(false);
		condition.setPopupMessage("test message"); //$NON-NLS-1$
		condition.setNotifiedByTrayBlink(true);
		notifyConditions.add(condition);
		sample.setNotifyConditions(notifyConditions);
		return sample;
	}
	
	public NotifyConfig(){
		this.notifyConditions = new HashSet<INotifyCondition>();
	}
	
	public NotifyConfig(Set<INotifyCondition> notifyConditions){
		this.notifyConditions = notifyConditions;
	}
	@Override
	public INotifyCondition fabricNotifyCondition(){
		return new NotifyCondition();
	}
}
