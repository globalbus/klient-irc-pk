package pk.klientIRC.ConfigProvider.Configs;

import java.awt.Color;

import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IConfigProviderDefaults;
import pk.klientIRC.Contracts.IVisibilityConfig;
@Component
public class VisibilityConfig implements IVisibilityConfig,
		IConfigProviderDefaults {

	private Color nickStyleColor;
	private Color messageStyleColor;
	private Color timeStyleColor;
	private Color nickChangeStyleColor;
	private Color topicStyleColor;

	@Override
	public Object Initialize() {
		return new VisibilityConfig(Color.GREEN, Color.BLACK, Color.BLACK, Color.GREEN, Color.GRAY);
	}

	@Override
	public void setNickStyleColor(Color nickStyleColor) {
		this.nickStyleColor = nickStyleColor;
	}

	@Override
	public void setMessageStyleColor(Color messageStyleColor) {
		this.messageStyleColor = messageStyleColor;
	}

	@Override
	public void setTimeStyleColor(Color timeStyleColor) {
		this.timeStyleColor = timeStyleColor;
	}

	@Override
	public void setNickChangeStyleColor(Color nickChangeStyleColor) {
		this.nickChangeStyleColor = nickChangeStyleColor;
	}

	@Override
	public void setTopicStyleColor(Color topicStyleColor) {
		this.topicStyleColor = topicStyleColor;
	}

	public VisibilityConfig() {
	};

	private VisibilityConfig(Color nickStyleColor, Color messageStyleColor,
			Color timeStyleColor, Color nickChangeStyleColor,
			Color topicStyleColor) {
		this.nickStyleColor = nickStyleColor;
		this.messageStyleColor = messageStyleColor;
		this.timeStyleColor = timeStyleColor;
		this.nickChangeStyleColor = nickChangeStyleColor;
		this.topicStyleColor = topicStyleColor;
	}

	@Override
	public Color getNickStyleColor() {
		// TODO Auto-generated method stub
		return nickStyleColor;
	}

	@Override
	public Color getMessageStyleColor() {
		// TODO Auto-generated method stub
		return messageStyleColor;
	}

	@Override
	public Color getTimeStyleColor() {
		// TODO Auto-generated method stub
		return timeStyleColor;
	}

	@Override
	public Color getNickChangeStyleColor() {
		// TODO Auto-generated method stub
		return nickChangeStyleColor;
	}

	@Override
	public Color getTopicStyleColor() {
		// TODO Auto-generated method stub
		return topicStyleColor;
	}

}
