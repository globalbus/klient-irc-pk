package pk.klientIRC.ConfigProvider.Configs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IConfigProvider;
import pk.klientIRC.Contracts.ISettingsControlTab;
import pk.klientIRC.Contracts.IVisibilityConfig;
import pk.klientIRC.Localization.Messages;

@Component
class VisibilityConfigTab extends JPanel implements ISettingsControlTab {
	private static final long serialVersionUID = 1L;
	private IConfigProvider config;
	private JButton buttonNickStyle;
	private JButton buttonMessageStyle;
	private JButton buttonNickChangeStyle;
	private JButton buttonTimeStyle;
	private JButton buttonTopicStyle;

	@Autowired
	private VisibilityConfigTab(IConfigProvider config) {
		this.config = config;
		IVisibilityConfig visualConfig = config
				.getConfig(IVisibilityConfig.class);
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JLabel nickStyleLabel = new JLabel(Messages.getString("VisibilityConfigTab.NickColor")); //$NON-NLS-1$
		buttonNickStyle = new JButton(Messages.getString("VisibilityConfigTab.ClickToSetLabel")); //$NON-NLS-1$
		buttonNickStyle.setActionCommand(Messages.getString("VisibilityConfigTab.NickColor")); //$NON-NLS-1$
		buttonNickStyle.setBackground(visualConfig.getNickStyleColor());
		buttonNickStyle.setForeground(inverseColor(visualConfig
				.getNickStyleColor()));
		buttonNickStyle.addActionListener(new SetColor());
		this.add(nickStyleLabel);
		this.add(buttonNickStyle);
		//
		JLabel messageStyleLabel = new JLabel(Messages.getString("VisibilityConfigTab.MessageColor")); //$NON-NLS-1$
		buttonMessageStyle = new JButton(Messages.getString("VisibilityConfigTab.ClickToSetLabel")); //$NON-NLS-1$
		buttonMessageStyle.setActionCommand(Messages.getString("VisibilityConfigTab.MessageColor")); //$NON-NLS-1$
		buttonMessageStyle.setBackground(visualConfig.getMessageStyleColor());
		buttonMessageStyle.setForeground(inverseColor(visualConfig
				.getMessageStyleColor()));
		buttonMessageStyle.addActionListener(new SetColor());
		this.add(messageStyleLabel);
		this.add(buttonMessageStyle);
		//
		JLabel nickChangeStyleLabel = new JLabel(Messages.getString("VisibilityConfigTab.NickChangeColor")); //$NON-NLS-1$
		buttonNickChangeStyle = new JButton(Messages.getString("VisibilityConfigTab.ClickToSetLabel")); //$NON-NLS-1$
		buttonNickChangeStyle.setActionCommand(Messages.getString("VisibilityConfigTab.NickChangeColor")); //$NON-NLS-1$
		buttonNickChangeStyle.setBackground(visualConfig
				.getNickChangeStyleColor());
		buttonNickChangeStyle.setForeground(inverseColor(visualConfig
				.getNickChangeStyleColor()));
		buttonNickChangeStyle.addActionListener(new SetColor());
		this.add(nickChangeStyleLabel);
		this.add(buttonNickChangeStyle);
		//
		JLabel timeStyleLabel = new JLabel(Messages.getString("VisibilityConfigTab.TimeColor")); //$NON-NLS-1$
		buttonTimeStyle = new JButton(Messages.getString("VisibilityConfigTab.ClickToSetLabel")); //$NON-NLS-1$
		buttonTimeStyle.setActionCommand(Messages.getString("VisibilityConfigTab.TimeColor")); //$NON-NLS-1$
		buttonTimeStyle.setBackground(visualConfig.getTimeStyleColor());
		buttonTimeStyle.setForeground(inverseColor(visualConfig
				.getTimeStyleColor()));
		buttonTimeStyle.addActionListener(new SetColor());
		this.add(timeStyleLabel);
		this.add(buttonTimeStyle);
		//
		JLabel topicStyleLabel = new JLabel(Messages.getString("VisibilityConfigTab.TopicColor")); //$NON-NLS-1$
		buttonTopicStyle = new JButton(Messages.getString("VisibilityConfigTab.ClickToSetLabel")); //$NON-NLS-1$
		buttonTopicStyle.setActionCommand(Messages.getString("VisibilityConfigTab.TopicColor")); //$NON-NLS-1$
		buttonTopicStyle.setBackground(visualConfig.getTopicStyleColor());
		buttonTopicStyle.setForeground(inverseColor(visualConfig
				.getTopicStyleColor()));
		buttonTopicStyle.addActionListener(new SetColor());
		this.add(topicStyleLabel);
		this.add(buttonTopicStyle);
	}

	@Override
	public JPanel getPanel() {
		return this;
	}

	@Override
	public String getName() {
		return Messages.getString("VisibilityConfigTab.ColorsFontsTab"); //$NON-NLS-1$
	}

	class SetColor implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JButton source = (JButton) arg0.getSource();
			Color color = JColorChooser.showDialog(null,
					arg0.getActionCommand(), source.getBackground());
			if (color == null)
				return;
			source.setBackground(color);
			source.setForeground(inverseColor(color));
		}
	}

	public Color inverseColor(Color color) {
		float[] components = color.getComponents(null);
		for (int i = 0; i < components.length - 1; ++i)
			components[i] = 1 - components[i];
		return new Color(components[0], components[1], components[2],
				components[3]);
	}

	@Override
	public void validate() {
		config = config.getNewInstance();
		IVisibilityConfig visualConfig = config
				.getConfig(IVisibilityConfig.class);
		visualConfig.setNickStyleColor(buttonNickStyle.getBackground());
		visualConfig.setMessageStyleColor(buttonMessageStyle.getBackground());
		visualConfig.setNickChangeStyleColor(buttonNickChangeStyle
				.getBackground());
		visualConfig.setTimeStyleColor(buttonTimeStyle.getBackground());
		visualConfig.setTopicStyleColor(buttonTopicStyle.getBackground());
		this.config.setInstance(config);
	}

}
