package pk.klientIRC.ConfigProvider.Configs;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.regex.Pattern;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IConfigProvider;
import pk.klientIRC.Contracts.INotifyCondition;
import pk.klientIRC.Contracts.INotifyConfig;
import pk.klientIRC.Contracts.ISettingsControlTab;

@Component
public class NotifyConfigTab extends JPanel implements ISettingsControlTab {

	private static final long serialVersionUID = 1L;
	private IConfigProvider config;
	private JList<INotifyCondition> conditionList = new JList<INotifyCondition>();
	private DefaultListModel<INotifyCondition> model = new DefaultListModel<INotifyCondition>();
	private INotifyConfig notifyConfig;
	private JTextField regularExp;
	private JTextField message;
	private JToggleButton notifyByTrayBlink;
	private JToggleButton notifyByPopup;
	private JToggleButton notifyBySound;

	@Autowired
	private NotifyConfigTab(IConfigProvider config) {
		this.setLayout(new BorderLayout());
		this.config = config;
		notifyConfig = config.getConfig(INotifyConfig.class);
		if(notifyConfig==null)
		{
			config.loadDefaults();
			notifyConfig = config.getConfig(INotifyConfig.class);
		}
		conditionList.setModel(model);
		conditionList.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent arg0) {
				@SuppressWarnings("unchecked")
				INotifyCondition notify = ((JList<INotifyCondition>) arg0
						.getSource()).getSelectedValue();
				if (notify != null) {
					regularExp.setText(notify.getRegex());
					message.setText(notify.getPopupMessage());
					notifyByPopup.setSelected(notify.isNotifiedByPopup());
					notifyBySound.setSelected(notify.isNotifiedBySound());
					notifyByTrayBlink.setSelected(notify
							.isNotifiedByTrayBlink());
				}
			}

		});
		for (INotifyCondition item : notifyConfig.getNotifyConditions())
			model.addElement(item);
		JPanel leftPanel = new JPanel();
		leftPanel.add(conditionList);
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		this.add(leftPanel, BorderLayout.WEST);
		JPanel bottomPanel = new JPanel();
		JButton addNotify = new JButton("Add Notify");
		bottomPanel.add(addNotify);
		addNotify.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Pattern test;
				try {
					test = Pattern.compile(regularExp.getText());
					INotifyCondition notification = notifyConfig
							.fabricNotifyCondition();
					notification.setRegex(regularExp.getText());
					notification.setPopupMessage(message.getText());
					notification.setNotifySound("beam2.wav");// sadly hardcoded
					notification.setNotifiedByPopup(notifyByPopup.isSelected());
					notification.setNotifiedBySound(notifyBySound.isSelected());
					notification.setNotifiedByTrayBlink(notifyByTrayBlink
							.isSelected());
					model.addElement(notification);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Invalid Regex");
					return;
				}

			}

		});
		JButton removeNotify = new JButton("Remove Notify");
		removeNotify.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				model.removeElement(conditionList.getSelectedValue());
			}

		});
		bottomPanel.add(removeNotify);
		JButton changeSelected = new JButton("Change Selected");
		bottomPanel.add(changeSelected);
		changeSelected.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Pattern test;
				try {
					test = Pattern.compile(regularExp.getText());
					INotifyCondition notification = conditionList
							.getSelectedValue();
					notification.setRegex(regularExp.getText());
					notification.setPopupMessage(message.getText());
					notification.setNotifySound("beam2.wav");// sadly hardcoded
					notification.setNotifiedByPopup(notifyByPopup.isSelected());
					notification.setNotifiedBySound(notifyBySound.isSelected());
					notification.setNotifiedByTrayBlink(notifyByTrayBlink
							.isSelected());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Invalid Regex");
					return;
				}

			}

		});
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		rightPanel.add(new JLabel("Regular expression"));
		regularExp = new JTextField();
		rightPanel.add(regularExp);
		rightPanel.add(new JLabel("Popup Message"));
		message = new JTextField();
		rightPanel.add(message);
		// rightPanel.add(new JLabel("Sound"));
		// rightPanel.add(new JComboBox());
		notifyBySound = new JToggleButton();
		rightPanel.add(new JLabel("Notify by sound"));
		rightPanel.add(notifyBySound);
		rightPanel.add(new JLabel("Notify by tray blink"));
		notifyByTrayBlink = new JToggleButton();
		rightPanel.add(notifyByTrayBlink);
		//rightPanel.add(new JLabel("Notify by popup"));
		notifyByPopup = new JToggleButton();
		//rightPanel.add(notifyByPopup);
		this.add(rightPanel, BorderLayout.CENTER);
		this.add(bottomPanel, BorderLayout.SOUTH);

	}

	@Override
	public String getName() {
		return "Notifications";
	}

	@Override
	public JPanel getPanel() {
		return this;
	}

	// private JPanel listPane;
	// private JPanel detailsPane;
	//
	//
	// private JButton addButton;
	//
	// private JButton removeButton;
	//
	// private JButton modifyButton;
	//
	// private JLabel regexLabel;
	//
	// private JLabel isSoundLabel;
	//
	// private JLabel soundLabel;
	//
	// private JLabel isDialogLabel;
	//
	// private JLabel isTrayLabel;
	//
	// private JLabel trayMessageLabel;
	//
	// private JLabel regex;
	//
	// private JLabel isSound;
	//
	// private JLabel sound;
	//
	// private JLabel isDialog;
	//
	// private JLabel isTray;
	//
	// private JLabel trayMessage;
	//
	// private JTextField regexField;
	//
	// private JCheckBox isSoundCheck;
	//
	// private JTextField soundChooser;
	//
	// private JCheckBox isDialogCheck;
	//
	// private JCheckBox isTrayCheck;
	//
	// private JTextField trayMessageField;
	//
	// private JButton applyButton;
	//
	// private void reloadList() {
	//
	// listPane.removeAll();
	//
	// }
	//
	// private void setPlainView() {
	//
	// detailsPane.removeAll();
	//
	// }
	//
	// private void setReadOnlyView() {
	//
	// detailsPane.removeAll();
	//
	// INotifyCondition selectedCondition = conditionList.getSelectedValue();
	//
	// regex.setText(selectedCondition.getRegex());
	//
	// isSound.setText(selectedCondition.isNotifiedBySound());
	//
	// sound.setText(selectedCondition.getNotifySound());
	//
	// isDialog.setText(selectedCondition.isNotifiedByPopup());
	//
	// isTray.setText(selectedCondition.isNotifiedByTrayBlink());
	//
	// trayMessage.setText(selectedCondition.getPopupMessage());
	//
	// detailsPane.add(regexLabel);
	//
	// detailsPane.add(regex);
	//
	// detailsPane.add(isSoundLabel);
	//
	// detailsPane.add(isSound);
	//
	// detailsPane.add(soundLabel);
	//
	// detailsPane.add(sound);
	//
	// detailsPane.add(isDialogLabel);
	//
	// detailsPane.add(isDialog);
	//
	// detailsPane.add(isTrayLabel);
	//
	// detailsPane.add(isTray);
	//
	// detailsPane.add(trayMessageLabel);
	//
	// detailsPane.add(trayMessage);
	//
	// }
	//
	// private void setModifyView() {
	//
	// detailsPane.removeAll();
	//
	// INotifyCondition selectedCondition = conditionList.getSelectedValue();
	//
	// regexField.setText(selectedCondition.getRegex());
	//
	// isSoundCheck.setSelected(selectedCondition.isNotifiedBySound());
	//
	// soundChooser.setText(selectedCondition.getNotifySound());
	//
	// isDialogCheck.setSelected(selectedCondition.isNotifiedByPopup());
	//
	// isTrayCheck.setSelected(selectedCondition.isNotifiedByTrayBlink());
	//
	// trayMessageField.setText(selectedCondition.getPopupMessage());
	//
	// detailsPane.add(regexLabel);
	//
	// detailsPane.add(regex);
	//
	// detailsPane.add(isSoundLabel);
	//
	// detailsPane.add(isSound);
	//
	// detailsPane.add(soundLabel);
	//
	// detailsPane.add(sound);
	//
	// detailsPane.add(isDialogLabel);
	//
	// detailsPane.add(isDialog);
	//
	// detailsPane.add(isTrayLabel);
	//
	// detailsPane.add(isTray);
	//
	// detailsPane.add(trayMessageLabel);
	//
	// detailsPane.add(trayMessage);
	//
	// detailsPane.add(applyButton);
	//
	// }
	//
	// @Override
	// public String getName() {
	//
	//		return "Notifications"; //$NON-NLS-1$
	//
	// }

	@Override
	public void validate() {

		config = config.getNewInstance();
		INotifyConfig notifyConfig = config.getConfig(INotifyConfig.class);
		HashSet<INotifyCondition> notifyConditions = new HashSet<INotifyCondition>();
		Enumeration<INotifyCondition> enumerator = model.elements();
		for (; enumerator.hasMoreElements();)
			notifyConditions.add(enumerator.nextElement());
		notifyConfig.setNotifyConditions(notifyConditions);
		this.config.setInstance(config);

	}
}
