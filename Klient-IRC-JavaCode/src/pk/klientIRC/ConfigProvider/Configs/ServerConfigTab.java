package pk.klientIRC.ConfigProvider.Configs;

import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.ISettingsControlTab;
import pk.klientIRC.Contracts.IConfigProvider;
import pk.klientIRC.Contracts.IServerConfiguration;
import pk.klientIRC.Localization.Messages;
@Component
class ServerConfigTab extends JPanel implements ISettingsControlTab {

	private static final long serialVersionUID = 1L;
	private IConfigProvider config;
	private JTextField serverAddress;
	private JTextField serverPort;
	private JTextField nicks;
	private JTextField realname;
	private JTextField username;
	private JComboBox<String> charList;
	private JTextField quit;
	@Autowired
	private ServerConfigTab(IConfigProvider config) {
		this.config=config;
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		IServerConfiguration serverConfig = config.getConfig(IServerConfiguration.class);
		JLabel serverAddressLabel = new JLabel(Messages.getString("ServerConfigTab.ServerAddressLabel")); //$NON-NLS-1$
		serverAddress = new JTextField(serverConfig.getServerAddress());
		this.add(serverAddressLabel);
		this.add(serverAddress);
		JLabel serverPortLabel = new JLabel(Messages.getString("ServerConfigTab.ServerPortLabel")); //$NON-NLS-1$
		serverPort = new JTextField(String.valueOf(serverConfig.getServerPort()));
		this.add(serverPortLabel);
		this.add(serverPort);
		JLabel nicksLabel = new JLabel(Messages.getString("ServerConfigTab.NicksLabel")); //$NON-NLS-1$
		String nicksString = ""; //$NON-NLS-1$
		for(String item : serverConfig.getAvailableNicks())
			nicksString+=item + " "; //$NON-NLS-1$
		nicks = new JTextField(nicksString);
		this.add(nicksLabel);
		this.add(nicks);
		JLabel realnameLabel = new JLabel(Messages.getString("ServerConfigTab.RealNameLabel")); //$NON-NLS-1$
		realname = new JTextField(serverConfig.getRealName());
		this.add(realnameLabel);
		this.add(realname);
		JLabel usernameLabel = new JLabel(Messages.getString("ServerConfigTab.UserNameLabel")); //$NON-NLS-1$
		username = new JTextField(serverConfig.getUserName());
		this.add(usernameLabel);
		this.add(username);
		JLabel quitLabel = new JLabel(Messages.getString("ServerConfigTab.QuitMessageLabel")); //$NON-NLS-1$
		quit = new JTextField(serverConfig.getQuitMsg());
		this.add(quitLabel);
		this.add(quit);
		charList= new JComboBox<String>();
		for(String item :Charset.availableCharsets().keySet())
			charList.addItem(item);
		charList.setSelectedItem(serverConfig.getEncoding());
		charList.setEditable(false);
		this.add(charList);
	}

	@Override
	public JPanel getPanel() {
		return this;
	}

	@Override
	public String getName() {
		return Messages.getString("ServerConfigTab.ServerConfigTab"); //$NON-NLS-1$
	}

	@Override
	public void validate() {
		config = config.getNewInstance();
		IServerConfiguration serverConfig =config.getConfig(IServerConfiguration.class);
		serverConfig.setServerAddress(this.serverAddress.getText());
		try{
		int i = Integer.parseInt(serverPort.getText());
		serverConfig.setServerPort(i);
		}
		catch(NumberFormatException e){
			
		}
		Set<String> nickSet = new HashSet<String>();
		for(String item : nicks.getText().split(" ")) //$NON-NLS-1$
			nickSet.add(item);
		serverConfig.setAvailableNicks(nickSet);
		serverConfig.setRealName(realname.getText());
		serverConfig.setUserName(username.getText());
		serverConfig.setQuitMsg(quit.getText());
		serverConfig.setEncoding((String) charList.getSelectedItem());
		this.config.setInstance(config);
	}
}
