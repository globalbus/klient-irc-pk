package pk.klientIRC.MainFrame;

import icons.IconLoader;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IConfigProvider;
import pk.klientIRC.Contracts.IIRCProvider;
import pk.klientIRC.Contracts.IInputControl;
import pk.klientIRC.Contracts.IMainFrame;
import pk.klientIRC.Contracts.IMultithreader;
import pk.klientIRC.Contracts.IReceive;
import pk.klientIRC.Localization.Messages;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.BorderLayout;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
class MainFrame extends JFrame implements IMainFrame {
	private static final long serialVersionUID = 1L;
	private JPanel textPane;
	private JPanel panel;

	private IIRCProvider provider;
	private IReceive receive;
	private IInputControl send;
	private JDialog dialog;
	protected boolean dialogShowed;
	private IConfigProvider config;

	/**
	 * Create the application.
	 */
	@Autowired
	private MainFrame(IIRCProvider provider, IReceive receive,
			IInputControl send, IConfigProvider config,
			IMultithreader multithread) {
		this.config = config;
		this.provider = provider;
		this.receive = receive;
		this.send = send;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {

				if (provider.isConnected()) {
					provider.doQuit();
					provider.disconnect();
					System.exit(0);
				}
			}

			@Override
			public void windowOpened(WindowEvent e) {
				getConnection();
			}
		});
		dialog = new JDialog(this,
				Messages.getString("MainFrame.Loading"), true); //$NON-NLS-1$
		dialog.add(new JLabel(
				Messages.getString("MainFrame.Loading"), new ImageIcon(IconLoader.class.getResource("icon-wait.gif")), //$NON-NLS-1$ //$NON-NLS-2$
				JLabel.CENTER));

		dialog.setAlwaysOnTop(true);
		dialog.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		dialog.setBounds(100, 100, 200, 200);
		this.setTitle(Messages.getString("OkienkoTestowe.NazwaAplikacji")); //$NON-NLS-1$
		this.setBounds(100, 100, 600, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new BorderLayout(0, 0));
		// splitPane = new JSplitPane();
		// splitPane.setDividerSize(0);
		// splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		textPane = receive.getPanel();
		// splitPane.setLeftComponent(textPane);
		panel = send.getPanel();
		// splitPane.setRightComponent(panel);
		// splitPane.setDividerLocation(200);
		try {
			this.setIconImage(ImageIO.read(IconLoader.class.getResource("logo.png"))); //$NON-NLS-1$
		} catch (IOException e1) {
		}
		this.getContentPane().add(textPane, BorderLayout.CENTER);
		this.getContentPane().add(panel, BorderLayout.SOUTH);
	}

	private void getConnection() {
		ExecutorService thread = Executors.newSingleThreadExecutor();
		thread.execute(new Runnable() {

			@Override
			public void run() {
				try {
					System.out.println("Running Connection"); //$NON-NLS-1$
					dialog.setEnabled(true);
					provider.connect();
					dialog.setEnabled(false);
				} catch (IOException e) {
					dialog.setEnabled(false);
					// dialog.setVisible(false);
					// dialogShowed = true;
					int n = JOptionPane.showConfirmDialog(
							null,
							e.getLocalizedMessage()
									+ Messages
											.getString("MainFrame.FailedToConnect"), //$NON-NLS-1$
							Messages.getString("MainFrame.ConnectionErrorTitle"), JOptionPane.YES_NO_CANCEL_OPTION); //$NON-NLS-1$
					if (n == JOptionPane.CANCEL_OPTION)
						System.exit(0);
					else if (n == JOptionPane.YES_OPTION)
						config.loadDefaults();
					run();
				}
			}
		});

	}

	@Override
	public JFrame getFrame() {
		return this;
	}
}
