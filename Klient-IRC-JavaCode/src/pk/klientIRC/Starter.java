package pk.klientIRC;

import java.awt.EventQueue;

import javax.swing.JFrame;

/** Start point for application
 * @author globalbus
 *
 */
public class Starter {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					JFrame window = BeanFactory.getInstance().getMainFrame().getFrame();
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
