package pk.klientIRC.MessageControlFacade;

import icons.IconLoader;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.BeanFactory;
import pk.klientIRC.Contracts.ISettingControl;
import pk.klientIRC.Localization.Messages;

@Component
public class CommonButtons {
	private final ISettingControl settings;
	private JButton[] buttons;

	@Autowired
	CommonButtons(final ISettingControl settings) {
		this.settings = settings;
		JButton buttonSettingsOpen = new JButton(Messages.getString("CommonButtons.SettingsButton")); //$NON-NLS-1$
		buttonSettingsOpen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				settings.getDialog().setVisible(true);
			}

		});
		JButton buttonArchiveOpen = new JButton(Messages.getString("CommonButtons.Archive")); //$NON-NLS-1$
		buttonArchiveOpen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				SwingUtilities.invokeLater(new Runnable(){
					

					@Override
					public void run() {
						JFrame window = (JFrame) BeanFactory.getInstance().getArchiveFrame();
						try {
							window.setIconImage(ImageIO.read(IconLoader.class.getResource("logo.png"))); //$NON-NLS-1$
						} catch (IOException e1) {
						}
						window.setVisible(true);
					}
					
				});
				
			}

		});
		buttons = new JButton[] { buttonSettingsOpen, buttonArchiveOpen };
	}

	public JButton[] getButtons() {
		return buttons;
	}

	public CommonButtons getInstance() {
		return new CommonButtons(settings);
	}
}
