package pk.klientIRC.MessageControlFacade;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IIRCProvider;
import pk.klientIRC.Localization.Messages;

@Component
public class ChannelButtons {
	private IIRCProvider provider;
	private JButton[] buttons;

	@Autowired
	ChannelButtons(final IIRCProvider provider) {
		this.provider = provider;
		JButton buttonJoin = new JButton(
				Messages.getString("ChannelButtons.JoinButton")); //$NON-NLS-1$
		JButton buttonChangeNickname = new JButton(
				Messages.getString("ChannelButtons.ChangeNickButton"));//$NON-NLS-1$
		JButton buttonNickServ = new JButton(
				Messages.getString("ChannelButtons.NickServButton")); //$NON-NLS-1$
		JButton buttonChanServ = new JButton(Messages.getString("ChannelButtons.ChanServButton")); //$NON-NLS-1$
		buttonJoin.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String val = ""; //$NON-NLS-1$
				val = JOptionPane.showInputDialog(
						Messages.getString("ChannelButtons.ChannelDialog"), val); //$NON-NLS-1$
				if (val != null)
					provider.joinChannel(val);

			}

		});
		buttonChangeNickname.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String val = ""; //$NON-NLS-1$
				val = JOptionPane.showInputDialog(
						Messages.getString("ChannelButtons.NewNickDialog"), val); //$NON-NLS-1$
				if (val != null)
					provider.changeNick(val);

			}

		});
		buttonNickServ.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				provider.sendRaw("/msg NickServ help"); //$NON-NLS-1$
			}

		});
		buttonChanServ.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				provider.sendRaw("/msg ChanServ help"); //$NON-NLS-1$
			}

		});
		buttons = new JButton[] { buttonJoin, buttonChangeNickname,
				buttonNickServ, buttonChanServ };
	}

	public JButton[] getButtons() {
		return buttons;
	}

	public ChannelButtons getInstance() {
		return new ChannelButtons(provider);
	}
}
