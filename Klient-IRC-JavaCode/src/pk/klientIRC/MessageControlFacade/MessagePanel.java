package pk.klientIRC.MessageControlFacade;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IMessageControl;
import pk.klientIRC.Contracts.IMessageControlMain;
import pk.klientIRC.Contracts.IMessagePanelFacade;

@Component
public class MessagePanel extends JPanel implements IMessagePanelFacade {
	private static final long serialVersionUID = 1L;

	private NickList nicks;
	IMessageControlMain messageControl;
	private ChannelButtons buttons;
	private boolean isChanPanel;
	private PrivButtons buttonsDef;
	private CommonButtons buttonsCom;

	private JPanel buttonsPanel;

	@Autowired
	private MessagePanel(NickList nicks, IMessageControlMain messageControl,
			ChannelButtons buttons, PrivButtons buttonsDef,
			CommonButtons buttonsCom) {
		isChanPanel = false;
		this.buttonsDef = buttonsDef;
		this.nicks = nicks;
		this.buttonsCom = buttonsCom;
		this.setLayout(new BorderLayout());
		this.messageControl = messageControl;
		JScrollPane scrollpane = new JScrollPane(messageControl.getPanel());
		// messageControl.getPanel().setPreferredSize(scrollpane.getSize());
		scrollpane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		this.add(scrollpane, BorderLayout.CENTER);
		this.buttons = buttons;
		buttonsPanel = new JPanel(new GridLayout(0, 1));
		for (JButton item : buttonsDef.getButtons())
			buttonsPanel.add(item);
		for (JButton item : buttonsCom.getButtons())
			buttonsPanel.add(item);
		JPanel rightPanel = new JPanel(new BorderLayout());

		rightPanel.add(nicks.getVisualList(), BorderLayout.CENTER);
		rightPanel.add(buttonsPanel, BorderLayout.SOUTH);
		this.add(rightPanel, BorderLayout.EAST);
	}

	public boolean isChanPanel() {
		return isChanPanel;
	}

	@Override
	public void setChanPanel() {
		isChanPanel = true;
		for (JButton item : buttonsDef.getButtons())
			buttonsPanel.remove(item);
		for (JButton item : buttons.getButtons())
			buttonsPanel.add(item);
	}

	@Override
	public NickList getNickList() {
		return nicks;
	}

	@Override
	public IMessageControl getMessageControl() {
		return messageControl;
	}

	@Override
	public void setNickList(List<String> list) {
		if (!isChanPanel) {
			setChanPanel();
		}
		this.nicks.setNickList(list);
	}

	@Override
	public JPanel getPanel() {
		return this;
	}

	@Override
	public IMessagePanelFacade getInstance() {
		return new MessagePanel(nicks.getInstance(),
				(IMessageControlMain) messageControl.getInstance(),
				buttons.getInstance(), buttonsDef.getInstance(),
				buttonsCom.getInstance());
	}

}
