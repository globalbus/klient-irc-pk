package pk.klientIRC.MessageControlFacade;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.ISend;
import pk.klientIRC.Localization.Messages;

@Component
public class NickList implements ActionListener{
	private JList<String> visualList;
	private DefaultListModel<String> nicks = new DefaultListModel<String>();
	private ISend sender;
	private JPopupMenu nicklistPopup;
	private JMenuItem pmWho, pmPriv;
	
	@Autowired
	NickList(ISend sender) {
		this.sender=sender;
		visualList = new JList<String>();
		visualList.setModel(nicks);
		visualList.setEnabled(false);
		visualList.setLayoutOrientation(JList.VERTICAL);
		visualList.addMouseListener( new MouseAdapter()
		{
			@Override
			public void mousePressed(MouseEvent e)
			{
				if ( SwingUtilities.isRightMouseButton(e) )
				{
					visualList.setSelectedIndex(visualList.locationToIndex(e.getPoint()));
					nicklistPopup.show(visualList, e.getX(), e.getY()); //and show the menu
				}
			}
		});


		nicklistPopup=new JPopupMenu();
		pmWho=new JMenuItem(Messages.getString("NickList.WhoIs")); //$NON-NLS-1$
		pmWho.addActionListener(this);
		pmPriv=new JMenuItem(Messages.getString("NickList.PrivateMessage")); //$NON-NLS-1$
		pmPriv.addActionListener(this);
		nicklistPopup.add(pmWho);
		nicklistPopup.add(pmPriv);
		// visualList.setPreferredSize(new Dimension(100, 100));//TODO - better
		// scalling of list element
	}
	private int beginningOpers = 0;
	private int beginningVoiced = 0;
	private int beginningNormal = 0;
	@SuppressWarnings("unused")
	private int count = 0;
	
	public void addNick(String nick) {
		char c = nick.charAt(0);
		if (c == '@') {
			nicks.add(beginningOpers, nick);
			beginningVoiced++;
			beginningNormal++;
		} else if (c == '+') {
			nicks.add(beginningVoiced, nick);
			beginningNormal++;
		} else {
			nicks.add(beginningNormal, nick);
		}
		count++;
	}

	public boolean removeNick(String nick) {
		if (nicks.contains(nick)) {
			nicks.removeElement(nick);
			char c = nick.charAt(0);
			switch (c) {
			case '@':
				beginningVoiced--;
			case '+':
				beginningNormal--;
			default:
				count--;
			}
			return true;
		}
		return false;
	}

	public boolean replaceNick(String oldNick, String newNick) {
		if (nicks.contains(oldNick)) {
			if (this.removeNick(oldNick))
			{
				this.addNick(newNick);
				return true;
			}
		}
		return false;
	}

	public JList<String> getVisualList() {
		return visualList;
	}

	public void setNickList(List<String> list) {
		visualList.setEnabled(true);
		for (String item : list)
			this.addNick(item);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==pmWho){
			sender.pushMessage("/who "+visualList.getSelectedValue());	 //$NON-NLS-1$
		}else if(e.getSource()==pmPriv){
			String val="";  //$NON-NLS-1$
			val = JOptionPane.showInputDialog(Messages.getString("NickList.PrivateMessage"), val); //$NON-NLS-1$
			if(val!=null){
				sender.pushMessage("/msg "+visualList.getSelectedValue()+" "+val); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
	}

	public NickList getInstance() {
		return new NickList(sender);
	}

}
