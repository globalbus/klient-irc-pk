package pk.klientIRC.MessageControlFacade;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IIRCProvider;
import pk.klientIRC.Localization.Messages;

@Component
public class PrivButtons {
	private IIRCProvider provider;

	private JButton[] buttons;
	@Autowired
	PrivButtons(final IIRCProvider provider) {
		this.provider = provider;
		JButton buttonWhoIs = new JButton(Messages.getString("PrivButtons.WhoIsButton")); //$NON-NLS-1$
		buttonWhoIs.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
					provider.whoIs();

			}

		});
		buttons = new JButton[]{buttonWhoIs};
	}

	public JButton[] getButtons() {
		return buttons;
	}

	public PrivButtons getInstance() {
		return new PrivButtons(provider);
	}
}
