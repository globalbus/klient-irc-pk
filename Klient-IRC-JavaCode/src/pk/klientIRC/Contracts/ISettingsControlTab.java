package pk.klientIRC.Contracts;

public interface ISettingsControlTab extends IVisualControl {
	String getName();

	void validate();

}
