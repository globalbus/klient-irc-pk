package pk.klientIRC.Contracts;

import java.io.IOException;

public interface IIRCProvider {

	void addListener(IReceive receive);

	String getCurrentNick();

	void sendRaw(String text);

	void connect() throws IOException;

	boolean isConnected();

	void disconnect();

	void doQuit();

	void joinChannel(String channel);

	void changeNick(String nick);

	void whoIs();
	
	void whoIs(String nick);
}
