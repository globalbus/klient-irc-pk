package pk.klientIRC.Contracts;

import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;

public interface IMessageControl extends IVisualControl{
	void addTopicMsg(String text);
	void addNickChangeMsg(String text);
	void addMessage(String text);

	void addMessage(String currentNick, String msg);
	IMessageControl getInstance();

	JTextPane getTextPane();

	StyledDocument getStyledDocument();
	void setName(String target);
	String getName();
	void clear();
}
