package pk.klientIRC.Contracts;


public interface IConfigProvider {
	IConfigProvider getNewInstance();
	<T> T getConfig(Class<? extends T> clazz);
	void setInstance(IConfigProvider config);
	void putConfig(Object obj);
	void loadDefaults();
}
