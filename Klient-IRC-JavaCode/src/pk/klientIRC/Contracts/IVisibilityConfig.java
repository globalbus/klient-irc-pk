package pk.klientIRC.Contracts;

import java.awt.Color;

public interface IVisibilityConfig {

	Color getNickStyleColor();

	Color getMessageStyleColor();

	Color getTimeStyleColor();

	Color getNickChangeStyleColor();

	Color getTopicStyleColor();

	void setNickStyleColor(Color nickStyleColor);

	void setMessageStyleColor(Color messageStyleColor);

	void setTimeStyleColor(Color timeStyleColor);

	void setNickChangeStyleColor(Color nickChangeStyleColor);

	void setTopicStyleColor(Color topicStyleColor);
}
