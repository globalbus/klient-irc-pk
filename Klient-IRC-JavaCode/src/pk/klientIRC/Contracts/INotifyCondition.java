package pk.klientIRC.Contracts;

public interface INotifyCondition {
	
	String getRegex();
	void setRegex(String regex);
	
	boolean isNotifiedBySound();
	void setNotifiedBySound(boolean notifiedBySound);
	
	String getNotifySound();
	void setNotifySound(String notifySound);
	
	boolean isNotifiedByTrayBlink();
	void setNotifiedByTrayBlink(boolean notifiedByTrayBlink);
	
	boolean isNotifiedByPopup();
	void setNotifiedByPopup(boolean notifiedByPopup);
	
	String getPopupMessage();
	void setPopupMessage(String popupMessage);

}
