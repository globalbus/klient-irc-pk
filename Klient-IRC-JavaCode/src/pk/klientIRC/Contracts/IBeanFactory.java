package pk.klientIRC.Contracts;

import Archiwum.Archiwum.Odczyt;

public interface IBeanFactory {
	IMainFrame getMainFrame();
	IMessageControl getMessageControl();
	Odczyt getArchiveFrame();
}
