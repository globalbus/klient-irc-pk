package pk.klientIRC.Contracts;

import java.util.Set;

public interface IServerConfiguration {
	String getServerAddress();

	void setServerAddress(String serverAddress);

	int getServerPort();

	void setServerPort(int serverPort);

	Set<String> getAvailableNicks();

	void setAvailableNicks(Set<String> availableNicks);
	String getNickNext();


	String getUserName();

	void setUserName(String userName);

	String getRealName();
	void setRealName(String realName);
	String getQuitMsg();
	void setQuitMsg(String quitMsg);

	void setEncoding(String encoding);

	String getEncoding();

}
