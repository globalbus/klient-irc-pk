package pk.klientIRC.Contracts;

import javax.swing.JPanel;

public interface IVisualControl {

	JPanel getPanel();

}
