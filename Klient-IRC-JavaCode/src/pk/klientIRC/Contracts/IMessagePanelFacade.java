package pk.klientIRC.Contracts;

import java.util.List;

import pk.klientIRC.MessageControlFacade.NickList;




public interface IMessagePanelFacade extends IVisualControl {

	IMessageControl getMessageControl();

	NickList getNickList();

	IMessagePanelFacade getInstance();

	void setChanPanel();

	void setNickList(List<String> list);
	
}
