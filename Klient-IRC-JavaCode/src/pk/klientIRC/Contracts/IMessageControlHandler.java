package pk.klientIRC.Contracts;


/** Provide implementation if you want to handle messages from specified nick/service/bot
 * @author globalbus
 *
 */
public interface IMessageControlHandler extends IMessageControl{
	String getHandlerName();
	boolean isEnabledHandler();
	void setEnabledHandler(boolean b);
}
