package pk.klientIRC.Contracts;

import java.util.Set;

public interface INotifyConfig {
	Set<INotifyCondition> getNotifyConditions();
	void setNotifyConditions(Set<INotifyCondition> notifyConditions);
	INotifyCondition fabricNotifyCondition();
}
