package pk.klientIRC.Contracts;
/** Provide implementation if you want to modify components in IMessageControl
 * @author globalbus
 *
 */
public interface IMessageControlFormatting {

	void initListener(IMessageControl messageControl);

}
