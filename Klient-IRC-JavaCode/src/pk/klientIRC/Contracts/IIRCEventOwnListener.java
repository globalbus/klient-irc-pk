package pk.klientIRC.Contracts;

import java.util.List;

import org.schwering.irc.lib.IRCModeParser;

/**
 * @author globalbus
 * @content This interface can be changed
 */
public interface IIRCEventOwnListener {
	void unknown(String prefix, String command, String middle, String trailing);

	void onRegistered();

	void onDisconnected();

	void onError(String msg);

	void onError(int num, String msg);

	void onInvite(String chan, String user, String passiveNick);

	void onJoin(String chan, String user);

	void onKick(String chan, String user, String passiveNick, String msg);

	void onMode(String chan, String user, IRCModeParser modeParser);

	void onMode(String user, String passiveNick, String mode);

	void onNick(String user, String newNick);

	void onNotice(String target, String user, String msg);

	void onPart(String chan, String user, String msg);

	void onPrivmsg(String target, String user, String msg);

	void onQuit(String user, String msg);

	void onReply(int num, String value, String msg);

	void onTopic(String chan, String user);

	void OnRegisterNicks(String string, List<String> list);

	void onTopicChange(String chan, String string, String topic);

	void onMessage(String msg);

}
