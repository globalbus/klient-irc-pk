package pk.klientIRC.Multithreader;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.stereotype.Component;

import pk.klientIRC.Contracts.IMultithreader;

@Component
public class Multithreader implements IMultithreader {
	private ExecutorService executor;

	private Multithreader(){
		executor = Executors.newSingleThreadExecutor();
	}
	@Override
	public void addToPool(Runnable object){
		executor.execute(object);
	}
}
